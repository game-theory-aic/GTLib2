add_library(LPsolvers OBJECT
        AbstractLPSolver.h
        LPSolver.h
        )
target_include_directories(LPsolvers PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})

set(TEST_FILES ${TEST_FILES}
        PARENT_SCOPE)

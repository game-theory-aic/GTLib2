/*
    Copyright 2020 Faculty of Electrical Engineering at CTU in Prague

    This file is part of Game Theoretic Library.

    Game Theoretic Library is free software: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License
    as published by the Free Software Foundation, either version 3
    of the License, or (at your option) any later version.

    Game Theoretic Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Game Theoretic Library.

    If not, see <http://www.gnu.org/licenses/>.
*/


#include <base/random.h>
#include "base/base.h"
#include "algorithms/stats.h"
#include "algorithms/cfr.h"
#include "algorithms/strategy.h"
#include "algorithms/evaluation.h"
#include "domains/darkchess.h"
#include "base/algorithm.h"

#include "gtest/gtest.h"

namespace GTLib2::domains {

using namespace chess;
TEST(Darkchess, enPassant) {
  domains::DarkchessDomain d(4, BOARD::STANDARD);
  shared_ptr<State> s = d.getRootStatesDistribution()[0].outcome.state;
  auto state = dynamic_cast<domains::DarkchessState *>(s.get());
  state->clearBoard();

  //build a model enPassant situation
  //need kings on board for the game to function
  shared_ptr<DarkchessKing> whiteKing = make_shared<DarkchessKing>(KING, 0, Square(1, 1), state);
  shared_ptr<DarkchessKing> blackKing = make_shared<DarkchessKing>(KING, 1, Square(1, 8), state);

  shared_ptr<DarkchessPawn> whitePawn = make_shared<DarkchessPawn>(PAWN, 0, Square(5, 2), state, 1);
  shared_ptr<DarkchessPawn> blackPawn = make_shared<DarkchessPawn>(PAWN, 1, Square(4, 4), state, 3);
  state->insertPiece(whitePawn);
  state->insertPiece(blackPawn);
  state->insertPiece(whiteKing);
  state->insertPiece(blackKing);
  state->updateAllPieces();

  vector<shared_ptr<Action>> v;
  for (const shared_ptr<Action> &a: state->getAvailableActionsFor(0)) {
    string check = a->toString();
    if (a->toString() == "Pe4") {
      v.emplace_back(a);
      v.emplace_back(d.getNoAction());
      break;
    }
  }

  Outcome newState = state->performActions(v)[0].outcome;
  auto newBoard = dynamic_cast<domains::DarkchessState *>(newState.state.get());
  //a black pawn's valid move should now be to capture en passant
  Square sq(5, 3);
  auto newBlackPawn = newBoard->getPiecesOfColorAndKind(1, PAWN)[0];
  EXPECT_NE(std::find(newBlackPawn->getAllMoves()->begin(),
                      newBlackPawn->getAllMoves()->end(),
                      sq), newBlackPawn->getAllMoves()->end());

  //make the en passant cut and check figure has been cut
  v.clear();
  for (const shared_ptr<Action> &a: newBoard->getAvailableActionsFor(1)) {
    string check = a->toString();
    if (a->toString() == "pe3") {
      v.emplace_back(a);
      v.emplace_back(d.getNoAction());
      break;
    }
  }

  Outcome lastState = newState.state->performActions(v)[0].outcome;
  auto lastBoard = dynamic_cast<domains::DarkchessState *>(lastState.state.get());
  EXPECT_EQ(lastBoard->getPiecesOfColor(0).size() + lastBoard->getPiecesOfColor(1).size(), 3);
}

TEST(Darkchess, castling) {
  domains::DarkchessDomain d(4, BOARD::STANDARD);
  shared_ptr<State> s = d.getRootStatesDistribution()[0].outcome.state;
  auto state = dynamic_cast<domains::DarkchessState *>(s.get());
  state->clearBoard();

  //build a model castling situation
  //need kings on board for the game to function
  shared_ptr<DarkchessKing> whiteKing = make_shared<DarkchessKing>(KING, 0, Square(5, 1), state);
  shared_ptr<DarkchessKing> blackKing = make_shared<DarkchessKing>(KING, 1, Square(2, 8), state);

  shared_ptr<DarkchessRook> whiteRookLong = make_shared<DarkchessRook>(ROOK, 0, Square(1, 1), state);
  shared_ptr<DarkchessRook> whiteRookShort = make_shared<DarkchessRook>(ROOK, 0, Square(8, 1), state);

  state->insertPiece(whiteRookLong);
  state->insertPiece(whiteRookShort);
  state->insertPiece(whiteKing);
  state->insertPiece(blackKing);
  state->updateAllPieces();

  //the white king should have 5 moves + 2 castling moves
  EXPECT_EQ(whiteKing->getAllMoves()->size(), 7);
  //fetch castling move
  vector<shared_ptr<Action>> v;
  for (shared_ptr<Action> a: state->getAvailableActionsFor(0)) {
    string check = a->toString();
    if (a->toString() == "Kg1") {
      v.emplace_back(a);
      v.emplace_back(d.getNoAction());
      break;
    }
  }

  Outcome newState = state->performActions(v)[0].outcome;
  auto newBoard = dynamic_cast<GTLib2::domains::DarkchessState *>(newState.state.get());
  DarkchessPiece *newWhiteKing = newBoard->getPiecesOfColorAndKind(0, KING)[0].get();
  DarkchessPiece *newWhiteRookShort = newBoard->getPiecesOfColorAndKind(0, ROOK)[1].get();
  EXPECT_EQ(newWhiteKing->getPosition().x, 7);
  EXPECT_EQ(newWhiteKing->getPosition().y, 1);
  EXPECT_EQ(newWhiteRookShort->getPosition().x, 6);
  EXPECT_EQ(newWhiteRookShort->getPosition().y, 1);

  domains::DarkchessDomain d2(4, BOARD::STANDARD);
  shared_ptr<State> s2 = d2.getRootStatesDistribution()[0].outcome.state;
  auto ks2 = dynamic_cast<domains::DarkchessState *>(s2.get());
  ks2->clearBoard();
  whiteKing = make_shared<DarkchessKing>(KING, 0, Square(5, 1), state);
  blackKing = make_shared<DarkchessKing>(KING, 1, Square(2, 8), state);

  whiteRookLong = make_shared<DarkchessRook>(ROOK, 0, Square(1, 1), state);
  whiteRookShort = make_shared<DarkchessRook>(ROOK, 0, Square(8, 1), state);

  ks2->insertPiece(whiteRookLong);
  ks2->insertPiece(whiteRookShort);
  ks2->insertPiece(whiteKing);
  ks2->insertPiece(blackKing);
  ks2->updateAllPieces();

  //fetch castling move
  v.clear();
  for (shared_ptr<Action> a: ks2->getAvailableActionsFor(0)) {
    string check = a->toString();
    if (a->toString() == "Kc1") {
      v.emplace_back(a);
      v.emplace_back(d.getNoAction());
      break;
    }
  }

  Outcome lastState = ks2->performActions(v)[0].outcome;
  auto lastBoard = dynamic_cast<GTLib2::domains::DarkchessState *>(lastState.state.get());
  DarkchessPiece *newWhiteKing2 = lastBoard->getPiecesOfColorAndKind(0, KING)[0].get();
  DarkchessPiece *newWhiteRookLong = lastBoard->getPiecesOfColorAndKind(0, ROOK)[0].get();
  EXPECT_EQ(newWhiteKing2->getPosition().x, 3);
  EXPECT_EQ(newWhiteKing2->getPosition().y, 1);
  EXPECT_EQ(newWhiteRookLong->getPosition().x, 4);
  EXPECT_EQ(newWhiteRookLong->getPosition().y, 1);
}

TEST(Darkchess, castleDeny) {
  domains::DarkchessDomain d(4, BOARD::STANDARD);
  shared_ptr<State> s = d.getRootStatesDistribution()[0].outcome.state;
  auto state = dynamic_cast<domains::DarkchessState *>(s.get());
  state->clearBoard();

  //build a model castling situation
  //need kings on board for the game to function
  shared_ptr<DarkchessKing> whiteKing = make_shared<DarkchessKing>(KING, 0, Square(5, 1), state);
  shared_ptr<DarkchessKing> blackKing = make_shared<DarkchessKing>(KING, 1, Square(2, 8), state);

  shared_ptr<DarkchessRook> whiteRookLong = make_shared<DarkchessRook>(ROOK, 0, Square(1, 1), state);
  shared_ptr<DarkchessRook> blackRook = make_shared<DarkchessRook>(ROOK, 1, Square(7, 8), state);
  shared_ptr<DarkchessRook> whiteRookShort = make_shared<DarkchessRook>(ROOK, 0, Square(8, 1), state);

  state->insertPiece(whiteRookLong);
  state->insertPiece(whiteRookShort);
  state->insertPiece(blackRook);
  state->insertPiece(whiteKing);
  state->insertPiece(blackKing);
  state->updateAllPieces();

  //the white king should have 5 moves + 2 castling move
  EXPECT_EQ(whiteKing->getAllMoves()->size(), 7);

  //fetch castling move
  vector<shared_ptr<Action>> v;
  for (shared_ptr<Action> a: state->getAvailableActionsFor(0)) {
    string check = a->toString();
    if (a->toString() == "Kg1") {
      v.emplace_back(a);
      v.emplace_back(d.getNoAction());
      break;
    }
  }

  //short castle should be valid even through check
  Outcome newState = state->performActions(v)[0].outcome;
  //todo code observation
//        EXPECT_EQ(newState.privateObservations[0]->getId(), v[0].get()->getId());
//        EXPECT_EQ(newState.privateObservations[1]->getId(), NO_OBSERVATION);

  //reset the board
  state->clearBoard();
  whiteKing = make_shared<DarkchessKing>(KING, 0, Square(5, 1), state);
  blackKing = make_shared<DarkchessKing>(KING, 1, Square(2, 8), state);
  blackRook = make_shared<DarkchessRook>(ROOK, 1, Square(3, 8), state);
  whiteRookLong = make_shared<DarkchessRook>(ROOK, 0, Square(1, 1), state);
  whiteRookShort = make_shared<DarkchessRook>(ROOK, 0, Square(8, 1), state);

  state->insertPiece(whiteRookLong);
  state->insertPiece(whiteRookShort);
  state->insertPiece(blackRook);
  state->insertPiece(whiteKing);
  state->insertPiece(blackKing);
  state->updateAllPieces();

  //fetch castling move
  v.clear();
  for (shared_ptr<Action> a: state->getAvailableActionsFor(0)) {
    string check = a->toString();
    if (a->toString() == "Kc1") {
      v.emplace_back(a);
      v.emplace_back(d.getNoAction());
      break;
    }
  }

  auto newState2 = state->performActions(v)[0].outcome;
//        EXPECT_EQ(newState2.privateObservations[0]->getId(), v[0].get()->getId());
//        EXPECT_EQ(newState2.privateObservations[1]->getId(), NO_OBSERVATION);
}

TEST(Darkchess, PAWNPromotion) {
  domains::DarkchessDomain d(4, BOARD::STANDARD);
  shared_ptr<State> s = d.getRootStatesDistribution()[0].outcome.state;
  auto state = dynamic_cast<domains::DarkchessState *>(s.get());
  state->clearBoard();

  //build a model game drawing situation
  //need kings on board for the game to function
  shared_ptr<DarkchessKing> whiteKing = make_shared<DarkchessKing>(KING, 0, Square(4, 6), state);
  shared_ptr<DarkchessKing> blackKing = make_shared<DarkchessKing>(KING, 1, Square(1, 1), state);

  shared_ptr<DarkchessPawn> whitePawn = make_shared<DarkchessPawn>(PAWN, 0, Square(5, 7), state, 1);

  state->insertPiece(whitePawn);
  state->insertPiece(whiteKing);
  state->insertPiece(blackKing);
  state->updateAllPieces();

  //fetch game winning move
  vector<shared_ptr<Action>> v;
  for (shared_ptr<Action> a: state->getAvailableActionsFor(0)) {
    string check = a->toString();
    if (a->toString() == "Pe8") {
      v.emplace_back(a);
      v.emplace_back(d.getNoAction());
      break;
    }
  }

  Outcome newState = state->performActions(v)[0].outcome;
  auto newBoard = dynamic_cast<domains::DarkchessState *>(newState.state.get());
  //after Rh8 there should be only 3 pieces left - the black rook is cut
  EXPECT_EQ(newBoard->getPiecesOfColor(0).size(), 2);
  EXPECT_EQ(newBoard->getPiecesOfColorAndKind(0, QUEEN).size(), 1);
}

TEST(Darkchess, gameOverTest) {
  domains::DarkchessDomain d(5, BOARD::MINIMAL4x3);
  shared_ptr<State> s = d.getRootStatesDistribution()[0].outcome.state;
  auto state = dynamic_cast<domains::DarkchessState *>(s.get());

  vector<shared_ptr<Action>> v;
  for (const shared_ptr<Action> &a: state->getAvailableActionsFor(WHITE)) {
    string check = a->toString();
    if (a->toString() == "Kb1") {
      v.emplace_back(a);
      v.emplace_back(d.getNoAction());
      break;
    }
  }

  Outcome newState = state->performActions(v)[0].outcome;
  auto newBoard = dynamic_cast<domains::DarkchessState *>(newState.state.get());

  v.clear();
  for (const shared_ptr<Action> &a: newBoard->getAvailableActionsFor(BLACK)) {
    string check = a->toString();
    if (a->toString() == "kb3") {
      v.emplace_back(a);
      v.emplace_back(d.getNoAction());
      break;
    }
  }

  Outcome state1 = newBoard->performActions(v)[0].outcome;
  auto board1 = dynamic_cast<domains::DarkchessState *>(state1.state.get());

  v.clear();
  for (const shared_ptr<Action> &a: board1->getAvailableActionsFor(WHITE)) {
    string check = a->toString();
    if (a->toString() == "Rd3") {
      v.emplace_back(a);
      v.emplace_back(d.getNoAction());
      break;
    }
  }

  Outcome state2 = board1->performActions(v)[0].outcome;
  auto board2 = dynamic_cast<domains::DarkchessState *>(state2.state.get());

  // "checkmate", but game is not over
  EXPECT_EQ(state2.rewards[0], 0);
  EXPECT_EQ(state2.rewards[1], 0);
  EXPECT_EQ(state2.state->countAvailableActionsFor(BLACK), 5);

  v.clear();
  for (const shared_ptr<Action> &a: board2->getAvailableActionsFor(BLACK)) {
    string check = a->toString();
    if (a->toString() == "kb2") {
      v.emplace_back(a);
      v.emplace_back(d.getNoAction());
      break;
    }
  }
  Outcome state3 = board2->performActions(v)[0].outcome;
  auto board3 = dynamic_cast<domains::DarkchessState *>(state3.state.get());
  v.clear();
  for (const shared_ptr<Action> &a: board3->getAvailableActionsFor(WHITE)) {
    string check = a->toString();
    if (a->toString() == "Kb2") {
      v.emplace_back(a);
      v.emplace_back(d.getNoAction());
      break;
    }
  }

  Outcome lastState = board3->performActions(v)[0].outcome;
  EXPECT_EQ(lastState.rewards[0], 1);
  EXPECT_EQ(lastState.rewards[1], -1);
}

TEST(Darkchess, kingToKingMove) {
  domains::DarkchessDomain domain(4, BOARD::SILVERMAN4BY4);
  shared_ptr<State> rootState = domain.getRootStatesDistribution()[0].outcome.state;
  auto state = dynamic_cast<domains::DarkchessState *>(rootState.get());
  state->clearBoard();

  vector<shared_ptr<DarkchessPiece>> pieces{
      make_shared<DarkchessKing>(KING, WHITE, Square(3, 1), state),
      make_shared<DarkchessRook>(ROOK, WHITE, Square(1, 1), state),
      make_shared<DarkchessRook>(ROOK, WHITE, Square(4, 2), state),
      make_shared<DarkchessKing>(KING, BLACK, Square(3, 3), state),
      make_shared<DarkchessQueen>(QUEEN, BLACK, Square(2, 4), state),
      make_shared<DarkchessRook>(ROOK, BLACK, Square(4, 4), state),
      make_shared<DarkchessPawn>(PAWN, BLACK, Square(3, 2), state, 1),
  };

  for (const auto &piece : pieces) {
    state->insertPiece(piece);
  }
  state->updateAllPieces();

//    cout << endl << state->toString() << endl;
//    auto actions = state->getAvailableActionsFor(WHITE);
//    auto validActions = state->getAllValidActions(WHITE);

  EXPECT_EQ(pieces[0]->getAllMoves()->size(), 4);
}

TEST(Darkchess, FENFactory){
  string FEN = "rnbqkbnr/pp1ppppp/8/2p5/4P3/8/PPPP1PPP/RNBQKBNR w KQkq c6";
  domains::DarkchessDomain domain(6, FEN);
  shared_ptr<State> rootState = domain.getRootStatesDistribution()[0].outcome.state;
  auto state = dynamic_cast<domains::DarkchessState *>(rootState.get());
  string b = "8 rnbqkbnr\n7 pp_ppppp\n6 ________\n5 __p_____\n4 ____P___\n3 ________\n2 PPPP_PPP\n1 RNBQKBNR\n  abcdefgh\n";
  EXPECT_EQ(state->toString(), b);
}

}
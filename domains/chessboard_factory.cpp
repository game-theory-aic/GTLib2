/*
    Copyright 2019 Faculty of Electrical Engineering at CTU in Prague

    This file is part of Game Theoretic Library.

    Game Theoretic Library is free software: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License
    as published by the Free Software Foundation, either version 3
    of the License, or (at your option) any later version.

    Game Theoretic Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Game Theoretic Library.

    If not, see <http://www.gnu.org/licenses/>.
*/

#include "chessboard_factory.h"

namespace GTLib2::domains::chess {
boardInfo BoardFactory::create(BOARD b, GTLib2::domains::KriegspielState *s) {
  switch (b) {
    case STANDARD:return createStandard(s);
    case MICROCHESS:return createMicrochess(s);
    case MINIMAL4x3:return createMinimal(s);
    case DEMICHESS:return createDemichess(s);
    case SILVERMAN4BY4:return createSilverman4x4(s);
    case KRK:return createKRK(s);
    default:unreachable("unrecognized option!");
  }
}

boardInfoDarkchess BoardFactory::create(BOARD b, GTLib2::domains::DarkchessState *s) {
  switch (b) {
    case STANDARD:return createStandard(s);
    case MICROCHESS:return createMicrochess(s);
    case MINIMAL4x3:return createMinimal(s);
//        case DEMICHESS:
//            return createDemichess(s);
    case SILVERMAN4BY4:return createSilverman4x4(s);
    case MINISILVER:return createMiniSilver(s);
    case MATE2:return createMateIn2(s);
    default:unreachable("unrecognized option!");
  }
}

boardInfoDarkchess BoardFactory::createMateIn2(GTLib2::domains::DarkchessState *b) {
  boardInfoDarkchess binfo;
  binfo.pieces = make_shared<vector<shared_ptr<DarkchessPiece>>>();
  binfo.pieces->push_back(make_shared<DarkchessKing>(KING, WHITE, Square(3, 5), b));
  binfo.pieces->push_back(make_shared<DarkchessRook>(ROOK, WHITE, Square(1, 1), b));
  binfo.pieces->push_back(make_shared<DarkchessPawn>(PAWN, WHITE, Square(2, 2), b, 1));

  binfo.pieces->push_back(make_shared<DarkchessKing>(KING, BLACK, Square(1, 5), b));
  binfo.pieces->push_back(make_shared<DarkchessRook>(ROOK, BLACK, Square(5, 4), b));
  binfo.pieces->push_back(make_shared<DarkchessPawn>(PAWN, BLACK, Square(1, 3), b, 1));

  binfo.castle = false;
  binfo.x = 5;
  binfo.y = 5;
  return binfo;
}

boardInfo BoardFactory::createKRK(GTLib2::domains::KriegspielState *b) {
  boardInfo binfo;
  binfo.pieces = make_shared<vector<shared_ptr<AbstractPiece>>>();
  unreachable("Not implemented");
//            return binfo
}

boardInfoDarkchess BoardFactory::createMiniSilver(GTLib2::domains::DarkchessState *b) {
  unreachable("Not implemented");
}

boardInfoDarkchess BoardFactory::createMicrochess(GTLib2::domains::DarkchessState *b) {
  boardInfoDarkchess binfo;
  binfo.pieces = make_shared<vector<shared_ptr<DarkchessPiece>>>();
  for (int j = WHITE; j <= BLACK; j++) {
    int y = j == WHITE ? 2 : 4;
    Square pos(j == WHITE ? 4 : 1, y);
    binfo.pieces->push_back(make_shared<DarkchessPawn>(PAWN, j, pos, b, 0));

    y = j == WHITE ? 1 : 5;
    for (int i = 1; i < 5; i++) {
      Square posi(i, y);
      switch (i) {
        case 1:
          if (j == WHITE) {
            binfo.pieces->push_back(make_shared<DarkchessRook>(ROOK, j, posi, b));
          } else {
            binfo.pieces->push_back(make_shared<DarkchessKing>(KING, j, posi, b));
          }
          break;
        case 2:
          if (j == WHITE) {
            binfo.pieces->push_back(make_shared<DarkchessBishop>(BISHOP, j, posi, b));
          } else {
            binfo.pieces->push_back(make_shared<DarkchessKnight>(KNIGHT, j, posi, b));
          }
          break;
        case 3:
          if (j == WHITE) {
            binfo.pieces->push_back(make_shared<DarkchessKnight>(KNIGHT, j, posi, b));
          } else {
            binfo.pieces->push_back(make_shared<DarkchessBishop>(BISHOP, j, posi, b));
          }
          break;
        case 4:
          if (j == WHITE) {
            binfo.pieces->push_back(make_shared<DarkchessKing>(KING, j, posi, b));
          } else {
            binfo.pieces->push_back(make_shared<DarkchessRook>(ROOK, j, posi, b));
          }
          break;
      }
    }
  }
  binfo.castle = false;
  binfo.x = 4;
  binfo.y = 5;
  return binfo;
}

boardInfo BoardFactory::createStandard(GTLib2::domains::KriegspielState *b) {
  boardInfo binfo;
  binfo.pieces = make_shared<vector<shared_ptr<AbstractPiece>>>();
  for (int j = WHITE; j <= BLACK; j++) {
    int currid = 0;
    int y = j == WHITE ? 2 : 7;
    for (int i = 1; i < 9; i++) {
      Square pos(i, y);
      binfo.pieces->push_back(make_shared<Pawn>(PAWN, j, pos, b, currid));
      currid++;
    }

    y = j == WHITE ? 1 : 8;
    for (int i = 1; i < 9; i++) {
      Square pos(i, y);
      switch (i) {
        case 1:
        case 8:binfo.pieces->push_back(make_shared<Rook>(ROOK, j, pos, b));
          break;
        case 2:
        case 7:binfo.pieces->push_back(make_shared<Knight>(KNIGHT, j, pos, b));
          break;
        case 3:
        case 6:binfo.pieces->push_back(make_shared<Bishop>(BISHOP, j, pos, b));
          break;
        case 4:binfo.pieces->push_back(make_shared<Queen>(QUEEN, j, pos, b));
          break;
        case 5:binfo.pieces->push_back(make_shared<King>(KING, j, pos, b));
          break;
        default:break;
      }
    }
  }
  binfo.castle = true;
  binfo.x = 8;
  binfo.y = 8;
  return binfo;
}

boardInfoDarkchess BoardFactory::createStandard(GTLib2::domains::DarkchessState *b) {
  boardInfoDarkchess binfo;
  binfo.pieces = make_shared<vector<shared_ptr<DarkchessPiece>>>();
  for (int j = WHITE; j <= BLACK; j++) {
    int currid = 0;
    int y = j == WHITE ? 2 : 7;
    for (int i = 1; i < 9; i++) {
      Square pos(i, y);
      binfo.pieces->push_back(make_shared<DarkchessPawn>(PAWN, j, pos, b, currid));
      currid++;
    }

    y = j == WHITE ? 1 : 8;
    for (int i = 1; i < 9; i++) {
      Square pos(i, y);
      switch (i) {
        case 1:
        case 8:binfo.pieces->push_back(make_shared<DarkchessRook>(ROOK, j, pos, b));
          break;
        case 2:
        case 7:binfo.pieces->push_back(make_shared<DarkchessKnight>(KNIGHT, j, pos, b));
          break;
        case 3:
        case 6:binfo.pieces->push_back(make_shared<DarkchessBishop>(BISHOP, j, pos, b));
          break;
        case 4:binfo.pieces->push_back(make_shared<DarkchessQueen>(QUEEN, j, pos, b));
          break;
        case 5:binfo.pieces->push_back(make_shared<DarkchessKing>(KING, j, pos, b));
          break;
        default:break;
      }
    }
  }
  binfo.castle = true;
  binfo.x = 8;
  binfo.y = 8;
  return binfo;
}

boardInfo BoardFactory::createSilverman4x4(
    GTLib2::domains::KriegspielState *b) {
  boardInfo binfo;
  binfo.pieces = make_shared<vector<shared_ptr<AbstractPiece>>>();
  for (int j = WHITE; j <= BLACK; j++) {
    int currid = 0;
    int y = j == WHITE ? 2 : 3;
    for (int i = 1; i < 5; i++) {
      Square pos(i, y);
      binfo.pieces->push_back(make_shared<Pawn>(PAWN, j, pos, b, currid));
      currid++;
    }

    y = j == WHITE ? 1 : 4;
    for (int i = 1; i < 5; i++) {
      Square pos(i, y);
      switch (i) {
        case 2:binfo.pieces->push_back(make_shared<Queen>(QUEEN, j, pos, b));
          break;
        case 3:binfo.pieces->push_back(make_shared<King>(KING, j, pos, b));
          break;
        default:binfo.pieces->push_back(make_shared<Rook>(ROOK, j, pos, b));
          break;
      }
    }
  }
  binfo.castle = false;
  binfo.x = 4;
  binfo.y = 4;
  return binfo;
}

boardInfoDarkchess BoardFactory::createSilverman4x4(GTLib2::domains::DarkchessState *b) {
  boardInfoDarkchess binfo;
  binfo.pieces = make_shared<vector<shared_ptr<DarkchessPiece>>>();
  for (int j = WHITE; j <= BLACK; j++) {
    int currid = 0;
    int y = j == WHITE ? 2 : 3;
    for (int i = 1; i < 5; i++) {
      Square pos(i, y);
      binfo.pieces->push_back(make_shared<DarkchessPawn>(PAWN, j, pos, b, currid));
      currid++;
    }

    y = j == WHITE ? 1 : 4;
    for (int i = 1; i < 5; i++) {
      Square pos(i, y);
      switch (i) {
        case 2:binfo.pieces->push_back(make_shared<DarkchessQueen>(QUEEN, j, pos, b));
          break;
        case 3:binfo.pieces->push_back(make_shared<DarkchessKing>(KING, j, pos, b));
          break;
        default:binfo.pieces->push_back(make_shared<DarkchessRook>(ROOK, j, pos, b));
          break;
      }
    }
  }
  binfo.castle = false;
  binfo.x = 4;
  binfo.y = 4;
  return binfo;
}

boardInfo BoardFactory::createMinimal(
    GTLib2::domains::KriegspielState *b) {
  boardInfo binfo;
  binfo.pieces = make_shared<vector<shared_ptr<AbstractPiece>>>();
  binfo.pieces->push_back(make_shared<King>(KING, WHITE, Square(3, 1), b));
  binfo.pieces->push_back(make_shared<King>(KING, BLACK, Square(1, 3), b));
  binfo.pieces->push_back(make_shared<Rook>(ROOK, WHITE, Square(4, 2), b));
  binfo.castle = false;
  binfo.x = 4;
  binfo.y = 3;
  return binfo;
}

boardInfoDarkchess BoardFactory::createMinimal(GTLib2::domains::DarkchessState *b) {
  boardInfoDarkchess binfo;
  binfo.pieces = make_shared<vector<shared_ptr<DarkchessPiece>>>();
  binfo.pieces->push_back(make_shared<DarkchessKing>(KING, WHITE, Square(3, 1), b));
  binfo.pieces->push_back(make_shared<DarkchessKing>(KING, BLACK, Square(1, 3), b));
  binfo.pieces->push_back(make_shared<DarkchessRook>(ROOK, WHITE, Square(4, 2), b));
  binfo.castle = false;
  binfo.x = 4;
  binfo.y = 3;
  return binfo;
}

boardInfo BoardFactory::createMicrochess(
    GTLib2::domains::KriegspielState *b) {
  boardInfo binfo;
  binfo.pieces = make_shared<vector<shared_ptr<AbstractPiece>>>();
  for (int j = WHITE; j <= BLACK; j++) {
    int y = j == WHITE ? 2 : 4;
    Square pos(j == WHITE ? 4 : 1, y);
    binfo.pieces->push_back(make_shared<Pawn>(PAWN, j, pos, b, 0));

    y = j == WHITE ? 1 : 5;
    for (int i = 1; i < 5; i++) {
      Square posi(i, y);
      switch (i) {
        case 1:
          if (j == WHITE) {
            binfo.pieces->push_back(make_shared<Rook>(ROOK, j, posi, b));
          } else {
            binfo.pieces->push_back(make_shared<King>(KING, j, posi, b));
          }
          break;
        case 2:
          if (j == WHITE) {
            binfo.pieces->push_back(make_shared<Bishop>(BISHOP, j, posi, b));
          } else {
            binfo.pieces->push_back(make_shared<Knight>(KNIGHT, j, posi, b));
          }
          break;
        case 3:
          if (j == WHITE) {
            binfo.pieces->push_back(make_shared<Knight>(KNIGHT, j, posi, b));
          } else {
            binfo.pieces->push_back(make_shared<Bishop>(BISHOP, j, posi, b));
          }
          break;
        case 4:
          if (j == WHITE) {
            binfo.pieces->push_back(make_shared<King>(KING, j, posi, b));
          } else {
            binfo.pieces->push_back(make_shared<Rook>(ROOK, j, posi, b));
          }
          break;
      }
    }
  }
  binfo.castle = true;
  binfo.x = 4;
  binfo.y = 5;
  return binfo;
}

boardInfo BoardFactory::createDemichess(
    GTLib2::domains::KriegspielState *b) {
  boardInfo binfo;
  binfo.pieces = make_shared<vector<shared_ptr<AbstractPiece>>>();
  for (int j = WHITE; j <= BLACK; j++) {
    int currid = 0;
    int y = j == WHITE ? 2 : 7;
    for (int i = 1; i < 5; i++) {
      Square pos(i, y);
      binfo.pieces->push_back(make_shared<Pawn>(PAWN, j, pos, b, currid));
      currid++;
    }

    y = j == WHITE ? 1 : 8;
    for (int i = 1; i < 5; i++) {
      Square pos(i, y);
      switch (i) {
        case 1:binfo.pieces->push_back(make_shared<King>(KING, j, pos, b));
          break;
        case 2:binfo.pieces->push_back(make_shared<Bishop>(BISHOP, j, pos, b));
          break;
        case 3:binfo.pieces->push_back(make_shared<Knight>(KNIGHT, j, pos, b));
          break;
        default:binfo.pieces->push_back(make_shared<Rook>(ROOK, j, pos, b));
          break;
      }
    }
  }
  binfo.castle = true;
  binfo.x = 4;
  binfo.y = 8;
  return binfo;
}



std::vector<std::string> split(std::string stringToBeSplitted, std::string delimeter) {
    std::vector<std::string> splittedString;
    int startIndex = 0;
    int  endIndex = 0;
    while( (endIndex = stringToBeSplitted.find(delimeter, startIndex)) < stringToBeSplitted.size() )
    {
        std::string val = stringToBeSplitted.substr(startIndex, endIndex - startIndex);
        splittedString.push_back(val);
        startIndex = endIndex + delimeter.size();
    }
    if(startIndex < stringToBeSplitted.size())
    {
        std::string val = stringToBeSplitted.substr(startIndex);
        splittedString.push_back(val);
    }
    return splittedString;
}

boardInfo FenBoardFactory::create(string s, GTLib2::domains::KriegspielState *b) {
    boardInfo binfo;
    binfo.pieces = make_shared<vector<shared_ptr<AbstractPiece>>>();
    vector<string> strs = split(s, " ");

    string pieceString = strs[0];
    vector<string> rows = split(pieceString, "/");

    int ySize = rows.size();
    int xSize = 0;
    for (char &c: rows[0]) {
        xSize += isdigit(c) ? c - 48 : 1;
    }

    binfo.x = xSize;
    binfo.y = ySize;

    int y = ySize;
    int x = 1;

    vector<shared_ptr<AbstractPiece>> rooks;
    shared_ptr<AbstractPiece> blackKing = nullptr;
    shared_ptr<AbstractPiece> whiteKing = nullptr;

    for (string row: rows) {
        for (char &c: row) {
            if (!isdigit(c)) {
                shared_ptr<AbstractPiece> piece = createPiece(c, x, y, b);
                ++x;
                blackKing = c == 'k' ? piece : blackKing;
                whiteKing = c == 'K' ? piece : whiteKing;
                if (piece->getKind() == ROOK) {
                    rooks.emplace_back(piece);
                }
                binfo.pieces->push_back(move(piece));
            } else {
                x += c - 48;
            }
        }
        y--;
        x = 1;
    }

    if (strs[1] == "b") {
        b->setPlayerOnMove(chess::BLACK);
    } else {
        b->setPlayerOnMove(chess::WHITE);
    }

    if (strs[2] == "----" || strs[2] == "--") {
        binfo.castle = false;
    } else {
        binfo.castle = true;
        for (shared_ptr<AbstractPiece> r: rooks) {
            shared_ptr<AbstractPiece> king = r->getColor() == WHITE ? whiteKing : blackKing;
            if (r->getPosition().x != 1 || r->getPosition().x != binfo.x || r->getPosition().y != 1
                || r->getPosition().y != binfo.y) {
                r->setHasMoved(true);
            } else if (r->getPosition().x > king->getPosition().x) {
                if (strs[2].find(r->getColor() == WHITE ? toupper('k') : 'k') != string::npos) {
                    r->setHasMoved(true);
                }
            } else if (r->getPosition().x < king->getPosition().x) {
                if (strs[2].find(r->getColor() == WHITE ? toupper('q') : 'q') != string::npos) {
                    r->setHasMoved(true);
                }
            }
        }
    }

    if (strs[3] != "-") {
        Square s = chess::stringToCoord(strs[3]);
        binfo.xEnpass = s.x;
        binfo.yEnpass = s.y;
    } else {
      binfo.xEnpass = 0;
      binfo.yEnpass = 0;
    }
    return binfo;
}

shared_ptr<AbstractPiece>
FenBoardFactory::createPiece(char &c, int x, int y, GTLib2::domains::KriegspielState *b) {
  char pieceChar = tolower(c);
  Square s(x, y);
  switch (pieceChar) {
    case 'r':
      return make_shared<Rook>(static_cast<pieceName>(pieceChar),
                               isupper(c) ? chess::WHITE : chess::BLACK,
                               s,
                               b);
    case 'q':
      return make_shared<Queen>(static_cast<pieceName>(pieceChar),
                                isupper(c) ? chess::WHITE : chess::BLACK,
                                s,
                                b);
    case 'k':
      return make_shared<King>(static_cast<pieceName>(pieceChar),
                               isupper(c) ? chess::WHITE : chess::BLACK,
                               s,
                               b);
    case 'n':
      return make_shared<Knight>(static_cast<pieceName>(pieceChar),
                                 isupper(c) ? chess::WHITE : chess::BLACK,
                                 s,
                                 b);
    case 'b':
      return make_shared<Bishop>(static_cast<pieceName>(pieceChar),
                                 isupper(c) ? chess::WHITE : chess::BLACK,
                                 s,
                                 b);
    case 'p': {
      auto p = make_shared<Pawn>(static_cast<pieceName>(pieceChar),
                                 isupper(c) ? chess::WHITE : chess::BLACK,
                                 s,
                                 b,
                                 isupper(c) ? pawnidW_ : pawnidB_);
      if (isupper(c)){
        pawnidW_++;
      } else {
        pawnidB_++;
      }
      return p;
    }
    default:unreachable("unrecognized option!");
  }
}

string FenBoardFactory::createFEN(const GTLib2::domains::KriegspielState *b) {
    string FEN;
    for (int i = b->getYSize(); i > 0; i--) {
        int freespaces = 0;
        for (int j = 1; j <= b->getXSize(); j++) {
            Square pos(j, i);
            shared_ptr<AbstractPiece> piece = b->getPieceOnCoords(pos);
            if (piece == nullptr) {
                freespaces++;
            } else {
                if (freespaces > 0) {
                    FEN += to_string(freespaces);
                }
                FEN += piece->toString();
                freespaces = 0;
            }
        }
        if (freespaces > 0) {
            FEN += to_string(freespaces);
        }

        if (i > 1) {
            FEN += "/";
        } else {
            FEN += " ";
        }
    }

    FEN += b->getPlayers()[0] == chess::WHITE ? "w " : "b ";

    if (b->canCastle()) {
        for (int i = chess::WHITE; i <= chess::BLACK; i++) {
            shared_ptr<AbstractPiece> king = b->getPiecesOfColorAndKind(i, KING)[0];
            vector<shared_ptr<AbstractPiece>> rooks = b->getPiecesOfColorAndKind(i, ROOK);

            if (rooks.size() == 2) {
                AbstractPiece *shortRook = nullptr;
                AbstractPiece *longRook = nullptr;

                for (shared_ptr<AbstractPiece> &r: rooks) {
                    shortRook = !r->hasMoved() && r->getPosition().x == 8 ? r.get() : shortRook;
                    longRook = !r->hasMoved() && r->getPosition().x == 1 ? r.get() : longRook;
                }

                if (!king->hasMoved()) {
                    if (shortRook != nullptr) {
                        FEN += i == chess::WHITE ? "K" : "k";
                    } else {
                        FEN += "-";
                    }

                    if (longRook != nullptr) {
                        FEN += i == chess::WHITE ? "Q" : "q";
                    } else {
                        FEN += "-";
                    }
                } else {
                    FEN += "--";
                }
            } else {
                if (!king->hasMoved() && !rooks[0]->hasMoved()) {
                    FEN += king->toString();
                } else {
                    FEN += "--";
                }
            }
        }
        FEN += " ";
    } else {
        FEN += "---- ";
    }

    if (!(b->getEnPassantSquare() == Square(-1, -1))) {
        FEN += chess::coordToString(b->getEnPassantSquare()) + " ";
    } else {
        FEN += "- ";
    }

    return FEN;
}
boardInfoDarkchess FenBoardFactory::create(string s, GTLib2::domains::DarkchessState *b) {
  boardInfoDarkchess binfo = boardInfoDarkchess();
  vector<string> strs = split(s, " ");

  string pieceString = strs[0];
  vector<string> rows = split(pieceString, "/");

  int ySize = rows.size();
  int xSize = 0;
  for (char &c: rows[0]) {
    xSize += isdigit(c) ? c - 48 : 1;
  }

  binfo.x = xSize;
  binfo.y = ySize;

  int y = ySize;
  int x = 1;

  vector<shared_ptr<DarkchessPiece>> rooks;
  shared_ptr<DarkchessPiece> blackKing = nullptr;
  shared_ptr<DarkchessPiece> whiteKing = nullptr;

  for (string row: rows) {
    for (char &c: row) {
      if (!isdigit(c)) {
        shared_ptr<DarkchessPiece> piece = createPiece(c, x, y, b);
        ++x;
        blackKing = c == 'k' ? piece : blackKing;
        whiteKing = c == 'K' ? piece : whiteKing;
        if (piece->getKind() == ROOK) {
          rooks.emplace_back(piece);
        }
        binfo.pieces->push_back(move(piece));
      } else {
        x += c - 48;
      }
    }
    y--;
    x = 1;
  }

  if (strs[1] == "b") {
    b->setPlayerOnMove(chess::BLACK);
  } else {
    b->setPlayerOnMove(chess::WHITE);
  }

  if (strs[2] == "----" || strs[2] == "--") {
    binfo.castle = false;
  } else {
    binfo.castle = true;
    for (shared_ptr<DarkchessPiece> r: rooks) {
      shared_ptr<DarkchessPiece> king = r->getColor() == WHITE ? whiteKing : blackKing;
      if (r->getPosition().x != 1 || r->getPosition().x != binfo.x || r->getPosition().y != 1
          || r->getPosition().y != binfo.y) {
        r->setHasMoved(true);
      } else if (r->getPosition().x > king->getPosition().x) {
        if (strs[2].find(r->getColor() == WHITE ? toupper('k') : 'k') != string::npos) {
          r->setHasMoved(true);
        }
      } else if (r->getPosition().x < king->getPosition().x) {
        if (strs[2].find(r->getColor() == WHITE ? toupper('q') : 'q') != string::npos) {
          r->setHasMoved(true);
        }
      }
    }
  }

  if (strs[3] != "-") {
    Square s = chess::stringToCoord(strs[3]);
    binfo.xEnpass = s.x;
    binfo.yEnpass = s.y;
  }
  return binfo;
}

shared_ptr<DarkchessPiece> FenBoardFactory::createPiece(char &c, int x, int y, GTLib2::domains::DarkchessState *b) {
  char pieceChar = tolower(c);
  Square s(x, y);
  switch (pieceChar) {
    case 'r':
      return make_shared<DarkchessRook>(static_cast<pieceName>(pieceChar),
                               isupper(c) ? chess::WHITE : chess::BLACK,
                               s,
                               b);
    case 'q':
      return make_shared<DarkchessQueen>(static_cast<pieceName>(pieceChar),
                                isupper(c) ? chess::WHITE : chess::BLACK,
                                s,
                                b);
    case 'k':
      return make_shared<DarkchessKing>(static_cast<pieceName>(pieceChar),
                               isupper(c) ? chess::WHITE : chess::BLACK,
                               s,
                               b);
    case 'n':
      return make_shared<DarkchessKnight>(static_cast<pieceName>(pieceChar),
                                 isupper(c) ? chess::WHITE : chess::BLACK,
                                 s,
                                 b);
    case 'b':
      return make_shared<DarkchessBishop>(static_cast<pieceName>(pieceChar),
                                 isupper(c) ? chess::WHITE : chess::BLACK,
                                 s,
                                 b);
    case 'p': {
      auto p = make_shared<DarkchessPawn>(static_cast<pieceName>(pieceChar),
                                 isupper(c) ? chess::WHITE : chess::BLACK,
                                 s,
                                 b,
                                 isupper(c) ? pawnidW_ : pawnidB_);
      if (isupper(c)){
        pawnidW_++;
      } else {
        pawnidB_++;
      }
      return p;
    }
    default:unreachable("unrecognized option!");
  }
}
}

/*
    Copyright 2019 Faculty of Electrical Engineering at CTU in Prague

    This file is part of Game Theoretic Library.

    Game Theoretic Library is free software: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License
    as published by the Free Software Foundation, either version 3
    of the License, or (at your option) any later version.

    Game Theoretic Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Game Theoretic Library.

    If not, see <http://www.gnu.org/licenses/>.
*/


#include <base/algorithm.h>
#include "domains/phantomTTT.h"

#include "gtest/gtest.h"

namespace GTLib2::domains {
TEST(PhantomTTT, CheckDraw) {
    const auto domain = make_shared<PhantomTTTDomain>(5, 3, NonObservable);
    vector<CellState> board = {Cross, Nought, Cross, Nought, Cross,
                               Nought, Cross, Nought, Cross, Nought,
                               Nought, Cross, Nought, Cross, Nought,
                               Cross, Nought, Cross, Nought, Cross,
                               Cross, Nought, Cross, Nought, Empty};
    vector<int> last = {};
    auto state = make_shared<PhantomTTTState>(&(*domain), board, last, 0, true);
    auto action = make_shared<PhantomTTTAction>(0, 24);
    auto res = state->performActions({action, action});
    EXPECT_TRUE(res[0].outcome.state->isTerminal() && res[0].outcome.rewards[0] == 0.0);
}

TEST(PhantomTTT, FullRandomGame) {
    const auto domain = make_shared<PhantomTTTDomain>(10, 4, NonObservable);
    vector<PreparedAlgorithm> algs = {
        createInitializer<RandomPlayer>(),
        createInitializer<RandomPlayer>()
    };
    int seed = 0;
    auto actualOutcome = playMatch(*domain, algs, {1, 1}, {1, 1}, BudgetIterations, seed);
}

TEST(PhantomTTT, FullGame) {
    const auto domain = make_shared<PhantomTTTDomain>(3, 3, FullyObservable);
    int action = 0;
    vector<PreparedAlgorithm> algs = {
        createInitializer<FixedActionPlayer>(action),
        createInitializer<FixedActionPlayer>(action)
    };
    int seed = 0;
    auto actualOutcome = playMatch(*domain, algs, {1, 1}, {1, 1}, BudgetIterations, seed);
    EXPECT_TRUE(actualOutcome[0] == 1.0);
}

TEST(PhantomTTT, CheckFailure) {
    const auto domain = make_shared<PhantomTTTDomain>(5, 3, NonObservable);
    vector<CellState> board = {Cross, Cross, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty};
    vector<int> last = {};
    auto state = make_shared<PhantomTTTState>(&(*domain), board, last, 0, true);
    auto action = make_shared<PhantomTTTAction>(0, 0);
    auto res = state->performActions({action, action});
    const auto obs = dynamic_cast<const PhantomTTTObservation &>(*res[0].outcome.privateObservations.back());
    EXPECT_TRUE(obs.result_ == Failure);
}

TEST(PhantomTTT, CheckForWinH1) {
    const auto domain = make_shared<PhantomTTTDomain>(5, 3, NonObservable);
    vector<CellState> board = {Cross, Cross, Cross, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty};
    vector<int> last = {};
    auto state = make_shared<PhantomTTTState>(&(*domain), board, last, 0, true);
    auto action = make_shared<PhantomTTTAction>(0, 0);
    auto res = state->performActions({action, action});
    EXPECT_TRUE(res[0].outcome.rewards[0] == 1.0);
}

TEST(PhantomTTT, CheckForWinH2) {
    const auto domain = make_shared<PhantomTTTDomain>(5, 3, NonObservable);
    vector<CellState> board = {Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Nought, Nought, Nought,
                               Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty};
    vector<int> last = {};
    auto state = make_shared<PhantomTTTState>(&(*domain), board, last, 0, true);
    auto action = make_shared<PhantomTTTAction>(0, 0);
    auto res = state->performActions({action, action});
    EXPECT_TRUE(res[0].outcome.rewards[0] == -1.0);
}

TEST(PhantomTTT, CheckForWinH3) {
    const auto domain = make_shared<PhantomTTTDomain>(5, 3, NonObservable);
    vector<CellState> board = {Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty,
                               Empty, Nought, Nought, Nought, Empty,
                               Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty};
    vector<int> last = {};
    auto state = make_shared<PhantomTTTState>(&(*domain), board, last, 0, true);
    auto action = make_shared<PhantomTTTAction>(0, 0);
    auto res = state->performActions({action, action});
    EXPECT_TRUE(res[0].outcome.rewards[0] == -1.0);
}

TEST(PhantomTTT, CheckForWinV1) {
    const auto domain = make_shared<PhantomTTTDomain>(5, 3, NonObservable);
    vector<CellState> board = {Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Cross, Empty,
                               Empty, Empty, Empty, Cross, Empty,
                               Empty, Empty, Empty, Cross, Empty};
    vector<int> last = {};
    auto state = make_shared<PhantomTTTState>(&(*domain), board, last, 0, true);
    auto action = make_shared<PhantomTTTAction>(0, 0);
    auto res = state->performActions({action, action});
    EXPECT_TRUE(res[0].outcome.rewards[0] == 1.0);
}

TEST(PhantomTTT, CheckForWinV2) {
    const auto domain = make_shared<PhantomTTTDomain>(5, 3, NonObservable);
    vector<CellState> board = {Nought, Empty, Empty, Empty, Empty,
                               Nought, Empty, Empty, Empty, Empty,
                               Nought, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty};
    vector<int> last = {};
    auto state = make_shared<PhantomTTTState>(&(*domain), board, last, 0, true);
    auto action = make_shared<PhantomTTTAction>(0, 0);
    auto res = state->performActions({action, action});
    EXPECT_TRUE(res[0].outcome.rewards[0] == -1.0);
}

TEST(PhantomTTT, CheckForWinV3) {
    const auto domain = make_shared<PhantomTTTDomain>(5, 3, NonObservable);
    vector<CellState> board = {Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Nought, Empty, Empty,
                               Empty, Empty, Nought, Empty, Empty,
                               Empty, Empty, Nought, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty};
    vector<int> last = {};
    auto state = make_shared<PhantomTTTState>(&(*domain), board, last, 0, true);
    auto action = make_shared<PhantomTTTAction>(0, 0);
    auto res = state->performActions({action, action});
    EXPECT_TRUE(res[0].outcome.rewards[0] == -1.0);
}

TEST(PhantomTTT, CheckForWinD1) {
    const auto domain = make_shared<PhantomTTTDomain>(5, 3, NonObservable);
    vector<CellState> board = {Cross, Empty, Empty, Empty, Empty,
                               Empty, Cross, Empty, Empty, Empty,
                               Empty, Empty, Cross, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty};
    vector<int> last = {};
    auto state = make_shared<PhantomTTTState>(&(*domain), board, last, 0, true);
    auto action = make_shared<PhantomTTTAction>(0, 0);
    auto res = state->performActions({action, action});
    EXPECT_TRUE(res[0].outcome.rewards[0] == 1.0);
}

TEST(PhantomTTT, CheckForWinD2) {
    const auto domain = make_shared<PhantomTTTDomain>(5, 3, NonObservable);
    vector<CellState> board = {Empty, Cross, Empty, Empty, Empty,
                               Empty, Empty, Cross, Empty, Empty,
                               Empty, Empty, Empty, Cross, Empty,
                               Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty};
    vector<int> last = {};
    auto state = make_shared<PhantomTTTState>(&(*domain), board, last, 0, true);
    auto action = make_shared<PhantomTTTAction>(0, 0);
    auto res = state->performActions({action, action});
    EXPECT_TRUE(res[0].outcome.rewards[0] == 1.0);
}

TEST(PhantomTTT, CheckForWinD3) {
    const auto domain = make_shared<PhantomTTTDomain>(5, 3, NonObservable);
    vector<CellState> board = {Empty, Empty, Cross, Empty, Empty,
                               Empty, Empty, Empty, Cross, Empty,
                               Empty, Empty, Empty, Empty, Cross,
                               Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty};
    vector<int> last = {};
    auto state = make_shared<PhantomTTTState>(&(*domain), board, last, 0, true);
    auto action = make_shared<PhantomTTTAction>(0, 0);
    auto res = state->performActions({action, action});
    EXPECT_TRUE(res[0].outcome.rewards[0] == 1.0);
}

TEST(PhantomTTT, CheckForWinD4) {
    const auto domain = make_shared<PhantomTTTDomain>(5, 3, NonObservable);
    vector<CellState> board = {Empty, Empty, Empty, Empty, Cross,
                               Empty, Empty, Empty, Cross, Empty,
                               Empty, Empty, Cross, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty};
    vector<int> last = {};
    auto state = make_shared<PhantomTTTState>(&(*domain), board, last, 0, true);
    auto action = make_shared<PhantomTTTAction>(0, 0);
    auto res = state->performActions({action, action});
    EXPECT_TRUE(res[0].outcome.rewards[0] == 1.0);
}

TEST(PhantomTTT, CheckForWinD5) {
    const auto domain = make_shared<PhantomTTTDomain>(5, 3, NonObservable);
    vector<CellState> board = {Empty, Empty, Empty, Cross, Empty,
                               Empty, Empty, Cross, Empty, Empty,
                               Empty, Cross, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty};
    vector<int> last = {};
    auto state = make_shared<PhantomTTTState>(&(*domain), board, last, 0, true);
    auto action = make_shared<PhantomTTTAction>(0, 0);
    auto res = state->performActions({action, action});
    EXPECT_TRUE(res[0].outcome.rewards[0] == 1.0);
}

TEST(PhantomTTT, CheckForWinD6) {
    const auto domain = make_shared<PhantomTTTDomain>(5, 3, NonObservable);
    vector<CellState> board = {Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty,
                               Cross, Empty, Empty, Empty, Empty,
                               Empty, Cross, Empty, Empty, Empty,
                               Empty, Empty, Cross, Empty, Empty};
    vector<int> last = {};
    auto state = make_shared<PhantomTTTState>(&(*domain), board, last, 0, true);
    auto action = make_shared<PhantomTTTAction>(0, 0);
    auto res = state->performActions({action, action});
    EXPECT_TRUE(res[0].outcome.rewards[0] == 1.0);
}

TEST(PhantomTTT, CheckForWinD7) {
    const auto domain = make_shared<PhantomTTTDomain>(5, 3, NonObservable);
    vector<CellState> board = {Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Cross,
                               Empty, Empty, Empty, Cross, Empty,
                               Empty, Empty, Cross, Empty, Empty};
    vector<int> last = {};
    auto state = make_shared<PhantomTTTState>(&(*domain), board, last, 0, true);
    auto action = make_shared<PhantomTTTAction>(0, 0);
    auto res = state->performActions({action, action});
    EXPECT_TRUE(res[0].outcome.rewards[0] == 1.0);
}

TEST(PhantomTTT, CheckForWinD8) {
    const auto domain = make_shared<PhantomTTTDomain>(5, 3, NonObservable);
    vector<CellState> board = {Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Cross, Empty,
                               Empty, Empty, Cross, Empty, Empty,
                               Empty, Cross, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty};
    vector<int> last = {};
    auto state = make_shared<PhantomTTTState>(&(*domain), board, last, 0, true);
    auto action = make_shared<PhantomTTTAction>(0, 0);
    auto res = state->performActions({action, action});
    EXPECT_TRUE(res[0].outcome.rewards[0] == 1.0);
}

TEST(PhantomTTT, CheckForWinD9) {
    const auto domain = make_shared<PhantomTTTDomain>(5, 3, NonObservable);
    vector<CellState> board = {Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Cross, Empty, Empty,
                               Empty, Empty, Empty, Cross, Empty,
                               Empty, Empty, Empty, Empty, Cross,
                               Empty, Empty, Empty, Empty, Empty};
    vector<int> last = {};
    auto state = make_shared<PhantomTTTState>(&(*domain), board, last, 0, true);
    auto action = make_shared<PhantomTTTAction>(0, 0);
    auto res = state->performActions({action, action});
    EXPECT_TRUE(res[0].outcome.rewards[0] == 1.0);
}

TEST(PhantomTTT, CheckForWinD10) {
    const auto domain = make_shared<PhantomTTTDomain>(5, 3, NonObservable);
    vector<CellState> board = {Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Cross, Empty,
                               Empty, Empty, Cross, Empty, Empty,
                               Empty, Cross, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty};
    vector<int> last = {};
    auto state = make_shared<PhantomTTTState>(&(*domain), board, last, 0, true);
    auto action = make_shared<PhantomTTTAction>(0, 0);
    auto res = state->performActions({action, action});
    EXPECT_TRUE(res[0].outcome.rewards[0] == 1.0);
}

TEST(PhantomTTT, CheckForWinD11) {
    const auto domain = make_shared<PhantomTTTDomain>(5, 3, NonObservable);
    vector<CellState> board = {Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Empty,
                               Empty, Empty, Empty, Empty, Cross,
                               Empty, Empty, Empty, Cross, Empty,
                               Empty, Empty, Cross, Empty, Empty};
    vector<int> last = {};
    auto state = make_shared<PhantomTTTState>(&(*domain), board, last, 0, true);
    auto action = make_shared<PhantomTTTAction>(0, 0);
    auto res = state->performActions({action, action});
    EXPECT_TRUE(res[0].outcome.rewards[0] == 1.0);
}
}
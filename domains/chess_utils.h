//
// Created by shuo on 08.01.20.
//

#ifndef GTLIB2_CHESS_UTILS_H
#define GTLIB2_CHESS_UTILS_H

#include "base/base.h"

namespace GTLib2::domains::chess {
    enum observationType {
        NOPAWNTAKES, WITHPAWNTAKES
    };
    enum player {
        WHITE = 0, BLACK = 1
    };
    enum pieceName {
        PAWN = 'p', KNIGHT = 'n', BISHOP = 'b', ROOK = 'r', QUEEN = 'q', KING = 'k'
    };
    enum checkType {
        VERTICAL, HORIZONTAL, LONG_DIAGONAL, SHORT_DIAGONAL, KNIGHT_CHECK
    };
    enum moveValidity {
        VALID, INVALID
    };
    enum BOARD : int {
        STANDARD, SILVERMAN4BY4, MINIMAL4x3, MICROCHESS, DEMICHESS, KRK, MINISILVER, MATE2,
    };
// See https://en.wikipedia.org/wiki/Minichess for variants description


    struct Square {
        Square(const Square &square) {
            x = square.x;
            y = square.y;
        }

        int x;
        int y;

        Square(int p1, int p2) : x(p1), y(p2) {}

        inline bool operator==(const Square &that) const {
            return this->x == that.x && this->y == that.y;
        }

        inline HashType getHash() const { return hashCombine(935453154564551, x, y); }
    };

/**
 * Converts Square object to a string eg. Square(3, 4) -> "c4"
 *
 * @param Square
 * @returns std::string
 */
    inline const string coordToString(Square c) {
        const char coords[] = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h'};
        return coords[c.x - 1] + to_string(c.y);
    }

/**
 * Converts a string into a Square object eg. "c4" -> Square(3, 4)
 *
 * @param std::string
 * @returns Square
 */
    inline Square stringToCoord(string s) {
        int x = s[0] - 96;
        int y = s[1] - 48;
        return Square(x, y);
    }

/**
 * Inverts player color (returns chess::BLACK for chess::WHITE and vice versa.
 *
 * @param int
 * @returns int
 */
    inline int invertColor(int c) {
        return c == WHITE ? BLACK : WHITE;
    }

}


#endif //GTLIB2_CHESS_UTILS_H
/*
    Copyright 2019 Faculty of Electrical Engineering at CTU in Prague

    This file is part of Game Theoretic Library.

    Game Theoretic Library is free software: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License
    as published by the Free Software Foundation, either version 3
    of the License, or (at your option) any later version.

    Game Theoretic Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Game Theoretic Library.

    If not, see <http://www.gnu.org/licenses/>.
*/

#include "domains/phantomTTT.h"

namespace GTLib2::domains {

unique_ptr<PhantomTTTDomain> PhantomTTTDomain::classicTTT3_3() {
    return make_unique<PhantomTTTDomain>(3,3, FullyObservable);
}

unique_ptr<PhantomTTTDomain> PhantomTTTDomain::classicTTT4_3() {
    return make_unique<PhantomTTTDomain>(4,3, FullyObservable);
}

unique_ptr<PhantomTTTDomain> PhantomTTTDomain::phantomTTT3_3() {
    return make_unique<PhantomTTTDomain>(3,3, NonObservable);
}

unique_ptr<PhantomTTTDomain> PhantomTTTDomain::phantomTTT4_3() {
    return make_unique<PhantomTTTDomain>(4,3, NonObservable);
}

unique_ptr<PhantomTTTDomain> PhantomTTTDomain::phantomTTT5_4() {
    return make_unique<PhantomTTTDomain>(5,4, NonObservable);
}

bool PhantomTTTAction::operator==(const Action &that) const {
    if (typeid(*this) == typeid(that)) {
        const auto rhsAction = dynamic_cast<const PhantomTTTAction *>(&that);
        return move_ == rhsAction->move_;
    }
    return false;
}

unsigned long PhantomTTTState::countAvailableActionsFor(const Player player) const {
    int count = 0;
    const CellState playerMark = player == Player(0) ? Cross : Nought;
    for (int i = 0; i < board_.size(); ++i) {
        if (domain_->observability_ == FullyObservable && board_[i] != Empty)
            continue;
        if (std::find(playedPos_.begin(), playedPos_.end(), i) != playedPos_.end())
            continue;
        if (board_[i] == playerMark)
            continue;
        ++count;
    }
    return count;
}

vector<shared_ptr<Action>> PhantomTTTState::getAvailableActionsFor(const Player player) const {
    auto list = vector<shared_ptr<Action>>();
    int count = 0;
    const CellState playerMark = player == Player(0) ? Cross : Nought;
    for (int i = 0; i < board_.size(); ++i) {
        if (domain_->observability_ == FullyObservable && board_[i] != Empty)
            continue;
        if (std::find(playedPos_.begin(), playedPos_.end(), i) != playedPos_.end())
            continue;
        if (board_[i] == playerMark)
            continue;
        list.push_back(make_shared<PhantomTTTAction>(count, i));
        ++count;
    }
    return list;
}

shared_ptr<Action> PhantomTTTState::getActionByID(const Player player, const ActionId action) const {
    int count = 0;
    const CellState playerMark = player == Player(0) ? Cross : Nought;
    for (int i = 0; i < board_.size(); ++i) {
        if (domain_->observability_ == FullyObservable && board_[i] != Empty)
            continue;
        if (std::find(playedPos_.begin(), playedPos_.end(), i) != playedPos_.end())
            continue;
        if (board_[i] == playerMark)
            continue;
        if (count == action)
            return make_shared<PhantomTTTAction>(count, i);
        ++count;
    }

    unreachable("action id out of range!");
}

CellState PhantomTTTState::checkHorizontal(const vector<CellState>& board) const {
    int currentSeq = 0;
    CellState currentMark = Empty;
    for (Position i = 0; i < domain_->boardSize_; ++i) {
        for (Position j = 0; j < domain_->boardSize_; ++j) {
            if (board[i*domain_->boardSize_ + j] == currentMark && currentMark != Empty) {
                ++currentSeq;
                if (currentSeq == domain_->victorySequenceLength_)
                    return currentMark;
                continue;
            }
            currentMark = board[i*domain_->boardSize_ + j];
            currentSeq = currentMark == Empty ? 0 : 1;
        }
        currentSeq = 0;
        currentMark = Empty;
    }
    return Empty;
}

CellState PhantomTTTState::checkVertical(const vector<CellState>& board) const {
    int currentSeq = 0;
    CellState currentMark = Empty;
    for (Position i = 0; i < domain_->boardSize_; ++i) {
        for (Position j = 0; j < domain_->boardSize_; ++j) {
            if (board[j*domain_->boardSize_ + i] == currentMark && currentMark != Empty) {
                ++currentSeq;
                if (currentSeq == domain_->victorySequenceLength_)
                    return currentMark;
                continue;
            }
            currentMark = board[j*domain_->boardSize_ + i];
            currentSeq = currentMark == Empty ? 0 : 1;
        }
        currentSeq = 0;
        currentMark = Empty;
    }
    return Empty;
}

CellState PhantomTTTState::checkDiagonal(const vector<CellState>& board) const {
    int currentSeq;
    CellState currentMark;
    for (Position i = 0; i < domain_->boardSize_; ++i) {
        currentSeq = 0;
        currentMark = Empty;
        int k = i;
        for (Position j = 0; (j < domain_->boardSize_) && (k < domain_->boardSize_); ++j) {
            if (board[j * domain_->boardSize_ + k] == currentMark && currentMark != Empty) {
                ++currentSeq;
                if (currentSeq == domain_->victorySequenceLength_)
                    return currentMark;
                ++k;
                continue;
            }
            currentMark = board[j * domain_->boardSize_ + k];
            currentSeq = currentMark == Empty ? 0 : 1;
            ++k;
        }
        currentSeq = 0;
        currentMark = Empty;
        k = i;
        for (Position j = 0; (j < domain_->boardSize_) && (k >= 0); ++j) {
            if (board[j*domain_->boardSize_ + k] == currentMark && currentMark != Empty) {
                ++currentSeq;
                if (currentSeq == domain_->victorySequenceLength_)
                    return currentMark;
                --k;
                continue;
            }
            currentMark = board[j*domain_->boardSize_ + k];
            currentSeq = currentMark == Empty ? 0 : 1;
            --k;
        }
        currentSeq = 0;
        currentMark = Empty;
        k = i;
        for (Position j = domain_->boardSize_ - 1; (j >= 0) && (k < domain_->boardSize_); --j) {
            if (board[j*domain_->boardSize_ + k] == currentMark && currentMark != Empty) {
                ++currentSeq;
                if (currentSeq == domain_->victorySequenceLength_)
                    return currentMark;
                ++k;
                continue;
            }
            currentMark = board[j*domain_->boardSize_ + k];
            currentSeq = currentMark == Empty ? 0 : 1;
            ++k;
        }
        currentSeq = 0;
        currentMark = Empty;
        k = i;
        for (Position j = domain_->boardSize_ - 1; (j >= 0) && (k >= 0); --j) {
            if (board[j*domain_->boardSize_ + k] == currentMark && currentMark != Empty) {
                ++currentSeq;
                if (currentSeq == domain_->victorySequenceLength_)
                    return currentMark;
                --k;
                continue;
            }
            currentMark = board[j*domain_->boardSize_ + k];
            currentSeq = currentMark == Empty ? 0 : 1;
            --k;
        }
    }
    return Empty;
}

bool PhantomTTTState::checkForDraw(const vector<CellState>& board) {
    return !any_of(board.begin(), board.end(), [](CellState c) { return c == Empty; });
}

OutcomeDistribution PhantomTTTState::performActions (const vector <shared_ptr<Action>> &actions) const {
    const auto action = dynamic_cast<PhantomTTTAction *>(actions[currentPlayer_].get());
    assert(action->move_ >= 0 && action->move_ < board_.size());
    bool success = false;
    auto playedPos = playedPos_;
    auto newBoard = board_;
    shared_ptr<PhantomTTTObservation> fullObs, partObs;
    if (newBoard[action->move_] == Empty) {
        newBoard[action->move_] = currentPlayer_ == Player(0) ? Cross : Nought;
        success = true;
        playedPos = {};
    }
    else {
        playedPos.push_back(action->move_);
    }
    fullObs = make_shared<PhantomTTTObservation>(success ? Success : Failure, action->move_);
    if (domain_->observability_ == NonObservable)
        partObs = make_shared<PhantomTTTObservation>(success ? Success : Failure);
    else if (domain_->observability_ == PartialObservable) {
        if (success)
            partObs = make_shared<PhantomTTTObservation>(Success);
        else
            partObs = make_shared<PhantomTTTObservation>(Failure, action->move_);
    }
    else
        partObs = make_shared<PhantomTTTObservation>(success ? Success : Failure, action->move_);
    CellState winner = checkHorizontal(newBoard);
    winner = winner != Empty ? winner : checkVertical(newBoard);
    winner = winner != Empty ? winner : checkDiagonal(newBoard);
    const shared_ptr<PhantomTTTState> newState =
        make_shared<PhantomTTTState>(domain_, newBoard, playedPos, success ? opponent(currentPlayer_) : currentPlayer_,
                                     (winner != Empty) || checkForDraw(newBoard));
    vector<double> rewards = {0.0, 0.0};
    if (winner == Cross) rewards = {1.0, -1.0};
    else if (winner == Nought) rewards = {-1.0, 1.0};
    const Outcome newOutcome(newState,
{currentPlayer_ == 0 ? fullObs : partObs, currentPlayer_ == 0 ? partObs : fullObs}, partObs, rewards);
    return OutcomeDistribution{OutcomeEntry(newOutcome)};
}

bool PhantomTTTState::operator==(const State &rhs) const {
    auto rhsState = dynamic_cast<const PhantomTTTState &>(rhs);
    return hash_ == rhsState.hash_
        && board_ == rhsState.board_
        && currentPlayer_ == rhsState.currentPlayer_
        && playedPos_ == rhsState.playedPos_
        && isTerminal_ == rhsState.isTerminal_;
}

string PhantomTTTState::toString() const {
    string s;
    s += ("Playing player: " + to_string(currentPlayer_) + "\n");
    s += "Board:\n";
    for (int i = 0; i < board_.size(); ++i) {
        s += board_[i] == Empty ? "_ " : board_[i] == Cross ? "X " : "O ";
        if ((i+1) % domain_->boardSize_ == 0)
            s += "\n";
    }
    return s;
}

PhantomTTTDomain::PhantomTTTDomain(const int boardSize, const int victorySequenceLength, const Observability observability) :
    boardSize_(boardSize), victorySequenceLength_(victorySequenceLength), observability_(observability),
    Domain(calculateMaxDepth(boardSize),
           2, true, make_shared<Action>(), make_shared<Observation>()) {
    assert(victorySequenceLength_ <= boardSize_);
    assert(boardSize_ > 1);
    assert(victorySequenceLength_ > 1);
    vector<CellState> board(boardSize*boardSize);
    std::fill(board.begin(),board.end(),Empty);
    vector<Position> playedPos = {};
    rootStatesDistribution_.push_back(OutcomeEntry(
        Outcome(make_shared<PhantomTTTState>(this, board, playedPos, Player(0), false),
            {noObservation_, noObservation_}, noObservation_, {0.0, 0.0})));
}

string PhantomTTTDomain::getInfo() const {
    return "************ Phantom Tic Tac Toe *************\nBoard size: " + to_string(boardSize_) + "*" +
    to_string(boardSize_) + ", Victory sequence length " + to_string(victorySequenceLength_) + "\n";
}

int PhantomTTTDomain::calculateMaxDepth(const int size) {
    int played = 0;
    int res = 0;
    for (int i = 0; i < size*size; ++i) {
        ++res;
        res += played;
        if (i % 2 == 0) ++played;
    }
    return res;
}

vector<int> PhantomTTTState::cellStatesToInt(const vector<CellState>& board) {
    vector<int> res;
    res.reserve(board.size());
    for (auto b : board) {
        res.push_back(b == Empty ? 0 : b == Cross ? 1 : 2);
    }
    return res;
}
}  // namespace GTLib2

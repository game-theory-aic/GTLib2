/**
Copyright 2020 Faculty of Electrical Engineering at CTU in Prague

This file is part of Game Theoretic Library.

Game Theoretic Library is free software: you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License
as published by the Free Software Foundation, either version 3
of the License, or (at your option) any later version.

Game Theoretic Library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with Game Theoretic Library.

If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef DOMAINS_DARKCHESS_H
#define DOMAINS_DARKCHESS_H

#include "base/base.h"
#include "chessboard_factory.h"
#include "chess_utils.h"

namespace GTLib2::domains {
class DarkchessState;

namespace chess {
class BoardFactory;

/**
* DarkchessPiece class for Darkchess, represents an abstract figure on the board
*/
class DarkchessPiece {
 public:

  /**
   * Constructor for an abstract piece
   * @param pieceName enum, the kind of figure (eg. chess::PAWN)
   * @param int color
   * @param Square position
   * @param const GTLib2::domains::DarkchessState* the board the piece is on
   */
  DarkchessPiece(pieceName, int, Square, const GTLib2::domains::DarkchessState *s);

  inline ~DarkchessPiece() {};

  /**
   * Returns all (valid and non-valid) moves of a figure
   */
  vector<Square> *getAllMoves() const;

  /**
   * Updates the figure's list of moves (non-valid)
   */
  virtual void updateMoves() = 0;

  /**
   * Returns a string used for figure notation eg. K for king and N for knight
   */
  string toString() const;

  bool operator==(const DarkchessPiece &that) const;

  bool equal_to(const DarkchessPiece &) const;

  int getColor() const;

  /**
   * Performs an update of moves depending on the circumstances (pinned/not pinned, in check/not in check)
   */
  void update();

  inline pieceName getKind() const {
    return this->kind;
  }

  inline void setBoard(const GTLib2::domains::DarkchessState *s) {
    this->board = s;
  }

  inline void setHasMoved(bool moved) {
    this->moved = moved;
  }

  /**
   * Updates the figure's position
   */
  void move(Square);

  /**
   * Returns if piece has moved in some previous move
   * @return bool
   */
  bool hasMoved() const;

  Square getPosition() const;

  /**
   * Clears the figure's moves and pinned by/protected by variables
   */
  void reset();

  virtual shared_ptr<DarkchessPiece> clone() const = 0;

 protected:
  int color;
  shared_ptr<vector<Square>> moves = make_shared<vector<Square>>();
  shared_ptr<vector<Square>> observedPieces = make_shared<vector<Square>>();
 public:
  const shared_ptr<vector<Square>> &getObservedPieces() const {
    return observedPieces;
  }

 protected:
  pieceName kind;
  Square position;
  const GTLib2::domains::DarkchessState *board;
  bool moved = false;
};

/**
* Pawn class for Darkchess, represents a Pawn on the board
* For documentation of functions, see superior class
*/
class DarkchessPawn : public DarkchessPiece {
 public:
  /**
   * Constructor for the Pawn class
   * @param pieceName enum, the kind of figure (eg. chess::PAWN)
   * @param int color
   * @param Square position
   * @param const GTLib2::domains::DarkchessState* the board the piece is on
   * @param int id of the pawn, used for calculation of pawn's take moves for observations
   */
  DarkchessPawn(pieceName, int, Square, const GTLib2::domains::DarkchessState *, int);

  void updateMoves() override;

  shared_ptr<DarkchessPiece> clone() const override;

  int getId() const;

 protected:
  int id;
};

/**
* Bishop class for Darkchess, represents a Bishop on the board
* For documentation of functions, see superior class
*/
class DarkchessBishop : public DarkchessPiece {
 public:
  DarkchessBishop(pieceName, int, Square, const GTLib2::domains::DarkchessState *);

  void updateMoves() override;

  shared_ptr<DarkchessPiece> clone() const override;
};

/**
* Rook class for Darkchess, represents a Rook on the board
* For documentation of functions, see superior class
*/
class DarkchessRook : public DarkchessPiece {
 public:
  DarkchessRook(pieceName, int, Square, const GTLib2::domains::DarkchessState *);

  DarkchessRook(pieceName, int, Square, const GTLib2::domains::DarkchessState *, bool);

  void updateMoves() override;

  shared_ptr<DarkchessPiece> clone() const override;
};

/**
* Knight class for Darkchess, represents a Knight on the board
* For documentation of functions, see superior class
*/
class DarkchessKnight : public DarkchessPiece {
 public:
  DarkchessKnight(pieceName, int, Square, const GTLib2::domains::DarkchessState *);

  void updateMoves() override;

  shared_ptr<DarkchessPiece> clone() const override;
};

/**
* Queen class for Darkchess, represents a Queen on the board
* For documentation of functions, see superior class
*/
class DarkchessQueen : public DarkchessPiece {
 public:
  DarkchessQueen(pieceName, int, Square, const GTLib2::domains::DarkchessState *);

  void updateMoves() override;

  shared_ptr<DarkchessPiece> clone() const override;
};

/**
* King class for Darkchess, represents a King on the board
* For documentation of functions, see superior class
*/
class DarkchessKing : public DarkchessPiece {
 public:
  DarkchessKing(pieceName, int, Square, const GTLib2::domains::DarkchessState *);

  DarkchessKing(pieceName, int, Square, const GTLib2::domains::DarkchessState *, bool);

  void updateMoves() override;

  shared_ptr<DarkchessPiece> clone() const override;
};
}

using chess::DarkchessPiece;
using chess::coordToString;

/**
* DarkchessAction class represents a single action in the game of Darkchess
*/
class DarkchessAction : public Action {
 public:
  /**
   * DarkchessAction constructor
   * @param int id
   * @param pair<shared_ptr<DarkchessPiece>, chess::Square>, pair where the first member is the piece being moved and second is
   *                                                        the position to which it is moving
   * @param chess::Square, the square where the figure is moving from
   */
  DarkchessAction(ActionId id, pair<shared_ptr<DarkchessPiece>, chess::Square>, const chess::Square &);

  explicit DarkchessAction(ActionId id);

  inline DarkchessAction() : DarkchessAction(NO_ACTION) {}

  inline string toString() const final {
    if (id_ == NO_ACTION)
      return "No action";
    return move_.first->toString()
        + coordToString(move_.second);
  }

  bool operator==(const Action &that) const override;

  pair<shared_ptr<DarkchessPiece>, chess::Square> getMove() const;

  chess::Square movingFrom() const;

  HashType getHash() const override;

  shared_ptr<DarkchessAction> clone() const;

 private:
  const pair<shared_ptr<DarkchessPiece>, chess::Square> move_;
  chess::Square moveFrom_;
};

/**
* DarkchessDomain class represents the domain of the game of Darkchess
*/
class DarkchessDomain : public Domain {
 public:
  // constructor
  /**
   * DarkchessDomain constructor
   * @param int maxHalfMoves, maximum depth when counting only legal half-moves
   * @param chess::BOARD enum, type of chess board for initialization
   * @param chess::observationType , determines if in observations are shown possible pawn takes
   */
  explicit DarkchessDomain(unsigned int maxHalfMoves, chess::BOARD board);

  DarkchessDomain(unsigned int maxHalfMoves, string &FEN);

  // destructor
  ~DarkchessDomain() override = default;

  // GetInfo returns string containing domain information.
  inline string getInfo() const final {
    return "************ Darkchess *************\n" +
        this->rootStatesDistribution_[0].outcome.state->toString() + "\n";
  }

};

/**
* DarkchessObservation class represents the observation for the game of Darkchess.
*
* bits 31-27    ->  types of checks
* bits 24-8     ->  can pawn take (bit 10 - can pawn1 take left, bit 11 - can pawn1 take right, etc)
* bits 7-0      ->  position of taken piece
*/
class DarkchessObservation : public Observation {
 public:
  inline DarkchessObservation() : Observation() {}

  inline explicit DarkchessObservation(ObservationId id) : Observation(id) {};

  inline explicit DarkchessObservation(ObservationId id, string boardFEN) : Observation(id),
                                                                            boardFEN_(boardFEN) {};

  inline explicit DarkchessObservation(ObservationId id, string boardFEN, int x, int y) : Observation(id),
                                                                                          boardFEN_(boardFEN),
                                                                                          x(x), y(y) {};

  const string &getBoardFen() const {
    return boardFEN_;
  }

  int getX() const {
    return x;
  }

  int getY() const {
    return y;
  }

 private:
  string boardFEN_;
  int x;
  int y;

};

/**
* DarkchessState class represents a board state in the game of Darkchess
*/
class DarkchessState : public State {
 public:

  // Constructor
  /**
   * @param Domain the Darkchess domain
   * @param unsigned int maxHalfMoves, the depth of game only when counting legal half-moves
   * @param chess::BOARD the board type of the game
   */
  DarkchessState(const Domain *domain,
                 unsigned int maxHalfMoves,
                 chess::BOARD board);

  DarkchessState(const Domain *domain,
                 unsigned int maxHalfMoves,
                 string &FEN);

  /**
   * @param Domain the Darkchess domain
   * @param int legalMaxDepth, the depth of game only when counting legal half-moves
   * @param int x-size of the board
   * @param int y-size of the board
   * @param shared_ptr<vector<shared_ptr<DarkchessPiece>>> a list containing all of the pieces on the board
   * @param Square enPassantSquare, the square where an en passant capture is possible (if any, otherwise null)
   * @param shared_ptr<vector<shared_ptr<DarkchessAction>>>, the current move history of the board
   * @param int p, the player that is on the move (chess::WHITE or chess::BLACK)
   * @param bool castle, whether castling is possible on the current board
   * @param bool onlyLegalMoves, allow players to play only legal moves
   * @param chess::observationType , determines if in observations are shown possible pawn takes
   * @param shared_ptr<vector<shared_ptr<DarkchessAction>>> attemptedMoves, a history of attempted moves (non-legal moves)
   */
  DarkchessState(const Domain *domain,
                 unsigned int legalMaxDepth,
                 unsigned int halfMoveNumber,
                 int x,
                 int y,
                 shared_ptr<vector<shared_ptr<DarkchessPiece>>> pieces,
                 chess::Square enPassantSquare,
                 int p,
                 bool castle);

  // Destructor
  ~DarkchessState() override = default;

  unsigned long countAvailableActionsFor(Player player) const override;

  // GetActions returns possible actions for a player in the state.
  vector<shared_ptr<Action>> getAvailableActionsFor(Player player) const override;

  /**
   * Performs an action on the current board state
   * @returns OutcomeDistribution containing the Outcome(a new state (should be a completely new object), observations for the players, rewards for the players)
   *                              and the NaturalProbability of the Outcome
   */
  OutcomeDistribution performActions(const vector<shared_ptr<Action>> &actions) const override;

  /**
   * Gets the player(s) moving in the current game state
   * @returns vector<Player> a list of the players
   */
  inline vector<Player> getPlayers() const final {
    vector<Player> v;
    if (!this->gameHasEnded_ || this->movesPlayed_ == domain_->getMaxStateDepth())
      v.emplace_back(playerOnMove_);
    return v;
  }

  inline bool isTerminal() const override { return gameHasEnded_; };

  bool operator==(const State &rhs) const override;

  void updateAllPieces() const;

  /**
   * Updates all pieces of a color
   * @param int color of the pieces to be updated
   */
  void updatePiecesOfColor(int color) const;

  /**
   * Fetches all of the pieces of a certain color
   * @param int color of the pieces to be fetched
   * @returns vector<shared_ptr<DarkchessPiece>> a list of the pieces
   */
  vector<shared_ptr<DarkchessPiece>> getPiecesOfColor(int) const;

  // for testing
  void clearBoard();

  void insertPiece(const shared_ptr<DarkchessPiece> &);

  inline void setPlayerOnMove(int p) {
    playerOnMove_ = p;
  };

  inline Player getPlayerOnMove() const {
    return playerOnMove_;
  };
  string toString() const override;

  /**
   * Function for calculating the rewards for the current game state
   * @returns vector<double> a list of rewards for all of the players
   */
  int checkGameOver() const;

  /**
   * Checks whether an action produces an available enPassantSquare
   * @returns chess::Square the square where the en passant is possible (if any) or null
   */
  chess::Square checkEnPassant(DarkchessAction *) const;

  /**
   * Gets all of the pieces of a certain color and kind
   * @param int color
   * @param chess::pieceName enum the kind of piece
   * @returns vector<shared_ptr<DarkchessPiece>> a list of the colors that fit the parameters
   */
  vector<shared_ptr<DarkchessPiece>> getPiecesOfColorAndKind(int, chess::pieceName) const;

  /**
   * Fetches a piece on certain coordinates
   * @param chess::Square the square for which to fetch the figure
   * @returns shared_ptr<DarkchessPiece> the piece on the square in question (if any) or null
   */
  shared_ptr<DarkchessPiece> getPieceOnCoords(chess::Square) const;

  /**
   * Returns a list of squares between two figures
   * @param DarkchessPiece* the first figure
   * @param DarkchessPiece* the second figure
   * @returns vector<chess::Square> a list of squares between the two figures
   */
  vector<chess::Square> getSquaresBetween(DarkchessPiece *, DarkchessPiece *) const;

  /**
   * Performs a castling action
   * @param DarkchessAction* an action that has previously been deemed as a castle
   */
  void castle(DarkchessAction *) const;

  /**
   * Checks whether the current Square is beyond this board
   * @param chess::Square the square to check for
   * @returns bool whether or not the Square is outside of the bounds of this board
   */
  bool coordOutOfBounds(chess::Square) const;

  /**
   * Resets all of the pieces (by calling piece->reset(), see doc of reset in DarkchessPiece::reset())
   */
  void resetAllPieces();

  /**
   * Attempts to perform a move in the game
   * @param DarkchessAction* the move to perform
   * @returns
   */
  void makeMove(DarkchessAction *a);

  /**
   * Updates the state for a player
   * @param int the color of player for who to perform the update
   */
  void updateState(int);

  /**
   * @returns bool whether or not castling is possible on this board
   */
  inline bool canCastle() const {
    return this->canPlayerCastle_;
  }

  /**
   * Promotes a pawn if it reached the end of the board and puts a queen at his coordinates
   * @param shared_ptr<DarkchessPiece> the pawn object (for deletion)
   * @param chess::Square the position where the queen should be put
   */
  void promote(const shared_ptr<DarkchessPiece> &, chess::Square) const;

  int getYSize() const;

  int getXSize() const;

  /**
   * Calculates public observation for both players, first 5 bits of number refer to different
   * checks (vertical, horizontal, long diagonal, short diagonal, knight). Last 8 bits are for
   * indicating captured pawn (value of 1 - 64) or captured piece (value 65 - 128). 0 in this
   * last bits indicate legal move made without capture.
   *
   * @return shared_ptr<DarkchessObservation> shared pointer of the observation
   */
  shared_ptr<Observation> getPublicObservation(const vector<string> &board) const;

  shared_ptr<Observation> getPrivateObservation(Player player, vector<string> &board);

  void setGameHasEnded(bool gameHasEnded);

  inline chess::Square getEnPassantSquare() const {
    return this->enPassantSquare_;
  }

  void setEnPassant(chess::Square);

 protected:
  shared_ptr<vector<shared_ptr<DarkchessPiece>>> pieces_;  // players' board
  unsigned int movesPlayed_;
  Player playerOnMove_;
  optional<shared_ptr<DarkchessPiece>> capturedPiece_ = nullopt;
  chess::Square enPassantSquare_;
  const unsigned int legalMaxDepth_;
  bool gameHasEnded_ = false;

 private:
  shared_ptr<vector<shared_ptr<DarkchessPiece>>> copyPieces() const;

  void initBoard(chess::BOARD);
  void initBoard(string);

  int xSize_{};
  int ySize_{};
  bool canPlayerCastle_{};
};

inline vector<double> encodeDarkchessObsHistory(unordered_map<ObservationId, shared_ptr<Observation>> &observations,
                                                const vector<ObservationId> &obsHistory) {
  vector<double> public_hist_int;
  for (auto &item : obsHistory) {
    if (item >= 0xFFFFFFF0) { // info about player
      continue;
    }
    if (observations.find(item) == observations.end()) {
      assert(false);
    }
    shared_ptr<Observation> obs = observations.at(item);
    const string board = dynamic_cast<DarkchessObservation *>(obs.get())->getBoardFen();
    double code = 0.;
    for (const char &c : board) {
      switch (c) {
        case 'P':code = 1.;
          break;
        case 'N':code = 2.;
          break;
        case 'B':code = 3.;
          break;
        case 'R':code = 4.;
          break;
        case 'Q':code = 5.;
          break;
        case 'K':code = 6.;
          break;
        case 'p':code = -1.;
          break;
        case 'n':code = -2.;
          break;
        case 'b':code = -3.;
          break;
        case 'r':code = -4.;
          break;
        case 'q':code = -5.;
          break;
        case 'k':code = -6.;
          break;
        default:code = 0.;
      }
      public_hist_int.emplace_back(code);
    }
  }
  return public_hist_int;
}

inline void printDarkchessObsHistory(unordered_map<ObservationId, shared_ptr<Observation>> &observations,
                                     const vector<ObservationId> &obsHistory) {
  for (auto &item : obsHistory) {
    if (item >= 0xFFFFFFF0) { // info about player
      continue;
    }
    if (observations.find(item) == observations.end()) {
      assert(false);
    }
    shared_ptr<Observation> obs = observations.at(item);
    auto pubObs = dynamic_cast<DarkchessObservation *>(obs.get());
    const string board = pubObs->getBoardFen();
    const int x = 4;
    const int y = pubObs->getY();
    for (int i = y - 1; i >= 0; --i) {
      cout << board.substr(i * x, x) << endl;
    }
    cout << endl;
  }
}

}
#endif //DOMAINS_DARKCHESS_H
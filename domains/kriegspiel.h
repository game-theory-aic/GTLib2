/**
    Copyright 2019 Faculty of Electrical Engineering at CTU in Prague

    This file is part of Game Theoretic Library.

    Game Theoretic Library is free software: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License
    as published by the Free Software Foundation, either version 3
    of the License, or (at your option) any later version.

    Game Theoretic Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Game Theoretic Library.

    If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef DOMAINS_KRIEGSPIEL_H_
#define DOMAINS_KRIEGSPIEL_H_

#include <bitset>
#include "base/base.h"
#include "chessboard_factory.h"
#include "chess_utils.h"

namespace GTLib2::domains {
class KriegspielState;

    namespace chess {
        class BoardFactory;

        struct boardInfo;

//enum observationType { NOPAWNTAKES, WITHPAWNTAKES };
//enum player { WHITE = 0, BLACK = 1 };
//enum pieceName { PAWN = 'p', KNIGHT = 'n', BISHOP = 'b', ROOK = 'r', QUEEN = 'q', KING = 'k' };
//enum checkType { VERTICAL, HORIZONTAL, LONG_DIAGONAL, SHORT_DIAGONAL, KNIGHT_CHECK };
//enum moveValidity { VALID, INVALID };
//enum BOARD : int { STANDARD, SILVERMAN4BY4, MINIMAL4x3, MICROCHESS, DEMICHESS };
//// See https://en.wikipedia.org/wiki/Minichess for variants description
//
//
//struct Square {
//  int x;
//  int y;
//  Square(int p1, int p2) : x(p1), y(p2) {}
//  inline bool operator==(const Square &that) const {
//      return this->x == that.x && this->y == that.y;
//  }
//  inline HashType getHash() const { return hashCombine(935453154564551, x, y); }
//};
//
///**
// * Converts Square object to a string eg. Square(3, 4) -> "c4"
// *
// * @param Square
// * @returns std::string
// */
//const string coordToString(Square c);
//
///**
// * Converts a string into a Square object eg. "c4" -> Square(3, 4)
// *
// * @param std::string
// * @returns Square
// */
//Square stringToCoord(string s);
//
///**
// * Inverts player color (returns chess::BLACK for chess::WHITE and vice versa.
// *
// * @param int
// * @returns int
// */
//int invertColor(int);

/**
 * AbstractPiece class for Kriegspiel, represents an abstract figure on the board
 */
        class AbstractPiece {
        public:

            /**
             * Constructor for an abstract piece
             * @param pieceName enum, the kind of figure (eg. chess::PAWN)
             * @param int color
             * @param Square position
             * @param const GTLib2::domains::KriegspielState* the board the piece is on
             */
  AbstractPiece(pieceName, int, Square, const GTLib2::domains::KriegspielState *s);
  inline ~AbstractPiece() {

  };
  /**
   * Returns all (valid and non-valid) moves of a figure
   */
  vector<Square> *getAllMoves() const;

  /**
   * Returns all valid moves of a figure
   */
  vector<Square> *getAllValidMoves() const;

  /**
   * Returns list of Squares this figure is currently attacking (serves for limiting king's movement
   * so that when a square is attacked by another figure, the king cannot move there
   */
  virtual vector<Square> getSquaresAttacked() const = 0;

  /**
   * Updates the figure's list of moves (non-valid)
   */
  virtual void updateMoves() = 0;

  /**
   * Updates the figure's list of moves (valid)
   * Also updates which figures this figure is protecting and which enemy figures it is pinning (if any)
   *
   * @param bool updates only pins and protections (not moves, used for when the figure itself is pinned but still can be
   * protecting and pinning other figures)
   */
  virtual void updateValidMovesPinsProtects(bool) = 0;

  /**
   * Updates valid moves while the figure is pinned (the line towards the pinner and the pinner itself)
   */
  virtual void updateValidMovesWhilePinned() = 0;

  /**
   * Updates valid moves of the figure while the player is in check (the checker's position and blocking moves)
   */
  virtual void updateValidMovesWhileInCheck();

  /**
   * Returns a string used for figure notation eg. K for king and N for knight
   */
  string toString() const;
  bool operator==(const AbstractPiece &that) const;
  bool equal_to(const AbstractPiece &) const;
  int getColor() const;

  /**
   * Performs an update of moves depending on the circumstances (pinned/not pinned, in check/not in check)
   */
  void update();
  inline pieceName getKind() const {
      return this->kind;
  }
  inline void setBoard(const GTLib2::domains::KriegspielState *s) {
      this->board = s;
  }

  /**
   * Updates the figure's position
   */
  void move(Square);
  /**
   * Returns if piece has moved in some previous move
   * @return bool
   */
  bool hasMoved() const;

  AbstractPiece *getProtectedBy() const;
  AbstractPiece *getPinnedBy() const;
  void setProtectedBy(AbstractPiece *);
  void setPinnedBy(AbstractPiece *);
  Square getPosition() const;

  /**
   * Clears the figure's moves and pinned by/protected by variables
   */
  void reset();
  void setHasMoved(bool);
  inline const GTLib2::domains::KriegspielState *getBoard() const {
      return this->board;
  }
  virtual shared_ptr<AbstractPiece> clone() const = 0;
 protected:
  int color;
  shared_ptr<vector<Square>> moves = make_shared<vector<Square>>();
  shared_ptr<vector<Square>> validMoves = make_shared<vector<Square>>();
  pieceName kind;
  Square position;
  const GTLib2::domains::KriegspielState *board;
  bool moved = false;
  AbstractPiece *protectedBy = nullptr;
  AbstractPiece *pinnedBy = nullptr;
};

/**
 * Pawn class for Kriegspiel, represents a Pawn on the board
 * For documentation of functions, see superior class
 */
class Pawn : public AbstractPiece {
 public:
  /**
   * Constructor for the Pawn class
   * @param pieceName enum, the kind of figure (eg. chess::PAWN)
   * @param int color
   * @param Square position
   * @param const GTLib2::domains::KriegspielState* the board the piece is on
   * @param int id of the pawn, used for calculation of pawn's take moves for observations
   */
  Pawn(pieceName, int, Square, const GTLib2::domains::KriegspielState *, int);
  void updateMoves() override;
  void updateValidMovesWhilePinned() override;
  void updateValidMovesWhileInCheck() override;
  vector<Square> getSquaresAttacked() const override;
  void updateValidMovesPinsProtects(bool) override;
  shared_ptr<AbstractPiece> clone() const override;
  int getId() const;
 protected:
  int id;
};

/**
 * Bishop class for Kriegspiel, represents a Bishop on the board
 * For documentation of functions, see superior class
 */
class Bishop : public AbstractPiece {
 public:
  Bishop(pieceName, int, Square, const GTLib2::domains::KriegspielState *);
  void updateMoves() override;
  void updateValidMovesWhilePinned() override;
  vector<Square> getSquaresAttacked() const override;
  void updateValidMovesPinsProtects(bool) override;
  shared_ptr<AbstractPiece> clone() const override;
};

/**
 * Rook class for Kriegspiel, represents a Rook on the board
 * For documentation of functions, see superior class
 */
class Rook : public AbstractPiece {
 public:
  Rook(pieceName, int, Square, const GTLib2::domains::KriegspielState *);
  Rook(pieceName, int, Square, const GTLib2::domains::KriegspielState *, bool);
  void updateMoves() override;
  void updateValidMovesWhilePinned() override;
  vector<Square> getSquaresAttacked() const override;
  void updateValidMovesPinsProtects(bool) override;
  shared_ptr<AbstractPiece> clone() const override;
};

/**
 * Knight class for Kriegspiel, represents a Knight on the board
 * For documentation of functions, see superior class
 */
class Knight : public AbstractPiece {
 public:
  Knight(pieceName, int, Square, const GTLib2::domains::KriegspielState *);
  void updateMoves() override;
  void updateValidMovesWhilePinned() override;
  vector<Square> getSquaresAttacked() const override;
  void updateValidMovesPinsProtects(bool) override;
  shared_ptr<AbstractPiece> clone() const override;
};

/**
 * Queen class for Kriegspiel, represents a Queen on the board
 * For documentation of functions, see superior class
 */
class Queen : public AbstractPiece {
 public:
  Queen(pieceName, int, Square, const GTLib2::domains::KriegspielState *);
  void updateMoves() override;
  void updateValidMovesWhilePinned() override;
  vector<Square> getSquaresAttacked() const override;
  void updateValidMovesPinsProtects(bool) override;
  shared_ptr<AbstractPiece> clone() const override;
};

/**
 * King class for Kriegspiel, represents a King on the board
 * For documentation of functions, see superior class
 */
class King : public AbstractPiece {
 public:
  King(pieceName, int, Square, const GTLib2::domains::KriegspielState *);
  King(pieceName, int, Square, const GTLib2::domains::KriegspielState *, bool);
  void updateMoves() override;
  void updateValidMovesWhilePinned() override;
  vector<Square> getSquaresAttacked() const override;
  void updateValidMovesPinsProtects(bool) override;
  shared_ptr<AbstractPiece> clone() const override;
};
}

using chess::AbstractPiece;
using chess::coordToString;

/**
 * KriegspielAction class represents a single action in the game of Kriegspiel
 */
class KriegspielAction : public Action {
 public:
  /**
   * KriegspielAction constructor
   * @param int id
   * @param pair<shared_ptr<AbstractPiece>, chess::Square>, pair where the first member is the piece being moved and second is
   *                                                        the position to which it is moving
   * @param chess::Square, the square where the figure is moving from
   */
  KriegspielAction(ActionId id, pair<shared_ptr<AbstractPiece>, chess::Square>, chess::Square);
  explicit KriegspielAction(ActionId id);
  inline KriegspielAction() : KriegspielAction(NO_ACTION) {}
  inline string toString() const final {
      if (id_ == NO_ACTION)
          return "No action";
      return move_.first->toString()
          + coordToString(move_.second);
  }
  bool operator==(const Action &that) const override;
  pair<shared_ptr<AbstractPiece>, chess::Square> getMove() const;
  chess::Square movingFrom() const;
  HashType getHash() const override;
  shared_ptr<KriegspielAction> clone() const;
 private:
  const pair<shared_ptr<AbstractPiece>, chess::Square> move_;
  chess::Square moveFrom_;
};

/**
 * KriegspielDomain class represents the domain of the game of Kriegspiel
 */
class KriegspielDomain : public Domain {
 public:
  // constructor
  /**
   * KriegspielDomain constructor
   * @param int maxDepth, maximum depth when counting both legal and illegal half-moves
   * @param int legalMaxDepth, maximum depth when counting only legal half-moves
   * @param chess::BOARD enum, type of chess board for initialization
   * @param bool onlyLegalMoves, allow players to play only legal moves
   * @param chess::observationType , determines if in observations are shown possible pawn takes
   */
  explicit KriegspielDomain(unsigned int maxDepth,
                            unsigned int maxLegalHalfMoves,
                            chess::BOARD,
                            chess::observationType = chess::WITHPAWNTAKES);

  /**
   * KriegspielDomain constructor
   * @param int maxDepth, maximum depth when counting both legal and illegal half-moves
   * @param int legalMaxDepth, maximum depth when counting only legal half-moves
   * @param string, FEN string from which to create the board
   */
  KriegspielDomain(unsigned int maxDepth, unsigned int legalMaxDepth, const string &, chess::observationType);

  // destructor
  ~KriegspielDomain() override = default;

  // GetInfo returns string containing domain information.
  inline string getInfo() const final {
      return "************ Kriegspiel *************\n" +
          this->rootStatesDistribution_[0].outcome.state->toString() + "\n";
  }
};

/**
 * KriegspielObservation class represents the observation for the game of Kriegspiel.
 *
 * bits 31-27    ->  types of checks
 * bits 24-8     ->  can pawn take (bit 10 - can pawn1 take left, bit 11 - can pawn1 take right, etc)
 * bits 7-0      ->  position of taken piece
 */
class KriegspielObservation : public Observation {
 public:
  inline KriegspielObservation() : Observation() {}
  inline explicit KriegspielObservation(ObservationId id) : Observation(id) {};
  inline explicit KriegspielObservation(ObservationId id,
                                        vector<chess::checkType> check,
                                        string capturedPiece,
                                        string takeMoves = "")
      : Observation(id),
        checks_(move(check)),
        capturedPiece_(move(capturedPiece)),
        moveValidity_(chess::VALID),
        pawnTakeMoves_(takeMoves) {};

  chess::moveValidity getMoveValidity() const {
      return moveValidity_;
  }
  const vector<chess::checkType> &getChecks() const {
      return checks_;
  }
  const string &getCapturedPiece() const {
      return capturedPiece_;
  }
 private:
  chess::moveValidity moveValidity_ = chess::INVALID;
  vector<chess::checkType> checks_ = {};
  string capturedPiece_ = "";
  string pawnTakeMoves_ = "";

  inline string toString() const final {
      if (moveValidity_ == chess::INVALID) {
          return this->getId() == NO_OBSERVATION ? "NoOb" : "Invalid action: "
              + to_string(this->getId());
      }
      if (this->getId() == 0) {
          return "Valid move played, nothing happened.";
      }
      string s = "";
      if (capturedPiece_ != "") {
          s += "Captured " + capturedPiece_ + ", ";
      }
      if (pawnTakeMoves_ != "") {
          s += "Possible pawn takes: " + pawnTakeMoves_ + ", ";
      }
      if (!checks_.empty()) {
          string
              checks[5] = {"vertical", "horizontal", "long diagonal", "short diagonal", "knight"};
          s += "checks: [";
          for (auto check : checks_) {
              s += checks[check] + ", ";
          }
          s += "]";
      }
      return s;
  };
};

/**
 * KriegspielState class represents a board state in the game of Kriegspiel
 */
class KriegspielState : public State {
 public:

  // Constructor
  /**
   * @param Domain the Kriegspiel domain
   * @param int legalMaxDepth, the depth of game only when counting legal half-moves
   * @param chess::BOARD the board type of the game
   * @param bool onlyLegalMoves, allow players to play only legal moves
   * @param chess::observationType , determines if in observations are shown possible pawn takes
   */
  KriegspielState(const Domain *domain,
                  int,
                  chess::BOARD,
                  chess::observationType = chess::NOPAWNTAKES);

  /**
   * @param Domain the Kriegspiel domain
   * @param int legalMaxDepth, the depth of game only when counting legal half-moves
   * @param stringthe string from which the board is constructed in FEN notation
   */
  KriegspielState(Domain *domain, int legalMaxDepth, string FEN, chess::observationType type = chess::NOPAWNTAKES);

  /**
   * @param Domain the Kriegspiel domain
   * @param int legalMaxDepth, the depth of game only when counting legal half-moves
   * @param int x-size of the board
   * @param int y-size of the board
   * @param shared_ptr<vector<shared_ptr<AbstractPiece>>> a list containing all of the pieces on the board
   * @param Square enPassantSquare, the square where an en passant capture is possible (if any, otherwise null)
   * @param shared_ptr<vector<shared_ptr<KriegspielAction>>>, the current move history of the board
   * @param int p, the player that is on the move (chess::WHITE or chess::BLACK)
   * @param bool castle, whether castling is possible on the current board
   * @param bool onlyLegalMoves, allow players to play only legal moves
   * @param chess::observationType , determines if in observations are shown possible pawn takes
   * @param shared_ptr<vector<shared_ptr<KriegspielAction>>> attemptedMoves, a history of attempted moves (non-legal moves)
   */
  KriegspielState(const Domain *domain,
                  int legalMaxDepth,
                  int x,
                  int y,
                  shared_ptr<vector<shared_ptr<AbstractPiece>>> pieces,
                  const chess::Square& enPassantSquare,
                  shared_ptr<vector<shared_ptr<KriegspielAction>>> moves,
                  int p,
                  bool castle,
                  chess::observationType,
                  shared_ptr<vector<shared_ptr<KriegspielAction>>>);

// Destructor
  ~KriegspielState() override = default;

  unsigned long countAvailableActionsFor(Player player) const override;

  // GetActions returns possible actions for a player in the state.
  vector<shared_ptr<Action>> getAvailableActionsFor(Player player) const override;

  /**
   * Performs an action on the current board state
   * @returns OutcomeDistribution containing the Outcome(a new state (should be a completely new object), observations for the players, rewards for the players)
   *                              and the NaturalProbability of the Outcome
   */
  OutcomeDistribution performActions(const vector<shared_ptr<Action>> &actions) const override;

  /**
   * Gets the player(s) moving in the current game state
   * @returns vector<Player> a list of the players
   */
  inline vector<Player> getPlayers() const final {
      vector<Player> v;
      if (!this->gameHasEnded_ || this->moveHistory_->size() == domain_->getMaxStateDepth())
          v.emplace_back(playerOnMove_);
      return v;
  }

  inline bool isTerminal() const override { return gameHasEnded_; };

  bool operator==(const State &rhs) const override;

  void updateAllPieces() const;

  /**
   * Updates all pieces of a color
   * @param int color of the pieces to be updated
   */
  void updatePiecesOfColor(int color) const;

  /**
   * Fetches all of the pieces of a certain color
   * @param int color of the pieces to be fetched
   * @returns vector<shared_ptr<AbstractPiece>> a list of the pieces
   */
  vector<shared_ptr<AbstractPiece>> getPiecesOfColor(int) const;

  // for testing
  void clearBoard();
  void insertPiece(const shared_ptr<AbstractPiece> &);
  void setPlayerOnMove(int);
  vector<shared_ptr<Action>> getAllValidActions(Player);

  string toString() const override;

  /**
   * Function for calculating the rewards for the current game state
   * @returns vector<double> a list of rewards for all of the players
   */
  int checkGameOver() const;

  /**
   * Checks whether an action produces an available enPassantSquare
   * @returns chess::Square the square where the en passant is possible (if any) or null
   */
  chess::Square checkEnPassant(KriegspielAction *) const;

  /**
   * Checks whether a piece is pinned by another piece
   * @param AbstractPiece* the pinner
   * @param AbstractPiece* the pinned
   * @returns bool whether the pinner is pinning the pinned
   */
  bool checkPinnedPiece(AbstractPiece *, AbstractPiece *) const;

  /**
   * Gets all of the pieces of a certain color and kind
   * @param int color
   * @param chess::pieceName enum the kind of piece
   * @returns vector<shared_ptr<AbstractPiece>> a list of the colors that fit the parameters
   */
  vector<shared_ptr<AbstractPiece>> getPiecesOfColorAndKind(int, chess::pieceName) const;

  /**
   * Fetches a piece on certain coordinates
   * @param chess::Square the square for which to fetch the figure
   * @returns shared_ptr<AbstractPiece> the piece on the square in question (if any) or null
   */
  shared_ptr<AbstractPiece> getPieceOnCoords(chess::Square) const;

  /**
   * Returns a list of squares beyond a certain figure that is being pierced by another figure
   * @param AbstractPiece* the piercing figure
   * @param AbstractPiece* the pierced figure
   * @returns vector<chess::Square> a list of the squares beyond the pierced figure
   */
  vector<chess::Square> getSquaresPierced(AbstractPiece *, AbstractPiece *) const;

  /**
   * Returns a list of squares between two figures
   * @param AbstractPiece* the first figure
   * @param AbstractPiece* the second figure
   * @returns vector<chess::Square> a list of squares between the two figures
   */
  vector<chess::Square> getSquaresBetween(AbstractPiece *, AbstractPiece *) const;

  /**
   * Returns a list of figures that are checking the king of the player currently on the move (if any)
   * @returns vector<shared<ptr<AbstractPiece>> the checking figures
   */
  vector<shared_ptr<AbstractPiece>> getCheckingFigures() const;

  chess::Square getEnPassantSquare() const;

  /**
   * Performs a castling action
   * @param KriegspielAction* an action that has previously been deemed as a castle
   */
  void castle(KriegspielAction *) const;

  /**
   * Checks whether the current Square is beyond this board
   * @param chess::Square the square to check for
   * @returns bool whether or not the Square is outside of the bounds of this board
   */
  bool coordOutOfBounds(chess::Square) const;

  /**
   * Returns this->playerInCheck
   */
  int getPlayerInCheck() const;

  /**
   * Checks whether the player currently on the move is in check
   * The result is stored in this->playerInCheck (either chess::WHITE/chess::BLACK if a player is in check, NO_PLAYER otherwise)
   */
  void checkPlayerInCheck();

  /**
   * Same principle as KriegspielState::checkPlayerInCheck() but usable where the function required has to be marked as const
   * @param int color of the player who we are checking for
   * @returns the color of the player currently on the move if he is in check, -1 otherwise
   */
  int checkGameOverCheck(int) const;

  /**
   * Resets all of the pieces (by calling piece->reset(), see doc of reset in AbstractPiece::reset())
   */
  void resetAllPieces();

  /**
   * Attempts to perform a move in the game
   * @param KriegspielAction* the move to perform
   * @returns bool true if the move to be performed is valid, false otherwise
   */
  bool makeMove(KriegspielAction *a);

  /**
   * Updates the state for a player
   * @param int the color of player for who to perform the update
   */
  void updateState(int);

  /**
   * @returns bool whether or not castling is possible on this board
   */
  bool canCastle() const;

  /**
   * Checks whether a square is under attack by any piece from a certain player
   * @param int the color of the player
   * @param chess::Square the square to check for
   * @returns bool
   */
  bool isSquareUnderAttack(int, const chess::Square&) const;

  /**
   * Promotes a pawn if it reached the end of the board and puts a queen at his coordinates
   * @param shared_ptr<AbstractPiece> the pawn object (for deletion)
   * @param chess::Square the position where the queen should be put
   */
  void promote(const shared_ptr<AbstractPiece> &, const chess::Square&) const;
  int getYSize() const;
  int getXSize() const;

  /**
   * Calculates public observation for both players, first 5 bits of number refer to different
   * checks (vertical, horizontal, long diagonal, short diagonal, knight). Last 8 bits are for
   * indicating captured pawn (value of 1 - 64) or captured piece (value 65 - 128). 0 in this
   * last bits indicate legal move made without capture.
   *
   * @return shared_ptr<KriegspielObservation> shared pointer of the observation
   */
  shared_ptr<Observation> getPublicObservation() const;
  shared_ptr<Observation> getPrivateObservation(shared_ptr<Observation> publicObs,
                                                Player player);
  void setGameHasEnded(bool gameHasEnded);
  void addToHistory(const shared_ptr<KriegspielAction> &a);
  void addToAttemptedMoves(const shared_ptr<KriegspielAction> &a);
  void setEnPassant(const chess::Square&);

  chess::observationType getObservationType() const {
      return observationType_;
  }
  shared_ptr<vector<shared_ptr<KriegspielAction>>> getAttemptedMoveHistory() const {
      return attemptedMoveHistory_;
  }
  Player getPlayerOnMove() const {
      return playerOnMove_;
  }

 protected:
  shared_ptr<vector<shared_ptr<AbstractPiece>>> pieces_;  // players' board
  vector<shared_ptr<AbstractPiece>> checkingFigures_;
  const shared_ptr<vector<shared_ptr<KriegspielAction>>> moveHistory_;
  const shared_ptr<vector<shared_ptr<KriegspielAction>>> attemptedMoveHistory_;
  Player playerOnMove_;
  optional<shared_ptr<AbstractPiece>> capturedPiece_ = nullopt;
  chess::Square enPassantSquare_;
  Player playerInCheck_ = NO_PLAYER;
  const int legalMaxDepth_;
  bool gameHasEnded_ = false;

 private:
  shared_ptr<vector<shared_ptr<AbstractPiece>>> copyPieces() const;
  shared_ptr<vector<shared_ptr<KriegspielAction>>> copyMoveHistory() const;
  shared_ptr<vector<shared_ptr<KriegspielAction>>> copyAttemptedMoves() const;

  void initBoard(chess::BOARD);
  void initBoard(string);

  int xSize_{};
  int ySize_{};
  bool canPlayerCastle_{};
  chess::observationType observationType_;
};

// TODO print obsID in human form
inline void printKriegSpielObsHistory(const vector<ActionId> &obsHistory) {
    for (const auto &id : obsHistory) {
        if (id >= 0xFFFFFFF0) {
            cout << "Player " << to_string(id - 0xFFFFFFF0) << " to move" << endl;
        } else {
            std::stringstream ss;
            ss << "Checks = [";
            array<string, 5>
                checks = {"vertical", "horizontal", "long diagonal", "short diagonal", "knight"};
            for (int i = 0; i < 5; ++i) {
                if ((id & (1 << (31 - i))) >> (31 - i)) {
                    ss << checks[i] << ", ";
                }
            }
            ss << "], captured: ";
            // capture
            uint32_t taken = (id & 0xFF);
            if (taken == 0) { // no piece taken
                ss << "None";
            } else {
                if (taken > 64) { // piece taken
                    ss << "Piece at position ";
                    taken -= 65;
                } else { // pawn taken
                    ss << "Pawn at position ";
                    taken -= 1;
                }
                assert(taken >= 0 && taken < 64);
                ss << to_string(taken);
            }
            cout << ss.str() << endl;
        }

    }
}

    inline vector<double> encodeKriegSpielObsHistory(const vector<ActionId> &obsHistory) {
        vector<double> encodedObservations;
        for (const auto &id : obsHistory) {
            if (id >= 0xFFFFFFF0) {
//                cout << "Player " << to_string(id - 0xFFFFFFF0) << " moved" << endl;
            } else {
                // get all checks
                for (int i = 0; i < 5; ++i) {
                    encodedObservations.push_back((id & (1 << (31 - i))) >> (31 - i));
                }
                // capture
                uint32_t taken = (id & 0xFF);
                if (taken == 0) { // no piece taken
                    encodedObservations.insert(encodedObservations.end(), 8, 0.0);
                } else {

                    if (taken > 64) { // piece taken
                        encodedObservations.insert(encodedObservations.end(), {0.0, 1.0});
                        taken -= 65;
                    } else { // pawn taken
                        encodedObservations.insert(encodedObservations.end(), {1.0, 0.0});
                        taken -= 1;
                    }
                    assert(taken >= 0 && taken < 64);
                    // encode position, independent on board size
                    std::bitset<6> bits(taken);
                    for (int i = 0; i < 6; ++i) {
                        encodedObservations.push_back(bits[5 - i]);
                    }
                }
            }
        }
        return encodedObservations;
    }

}

#endif  // DOMAINS_KRIEGSPIEL_H_

#pragma clang diagnostic pop

/*
    Copyright 2019 Faculty of Electrical Engineering at CTU in Prague

    This file is part of Game Theoretic Library.

    Game Theoretic Library is free software: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License
    as published by the Free Software Foundation, either version 3
    of the License, or (at your option) any later version.

    Game Theoretic Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Game Theoretic Library.

    If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef GTLIB2_PHANTOMTTT_H_
#define GTLIB2_PHANTOMTTT_H_

#include <utility>

#include "base/base.h"

namespace GTLib2::domains {

typedef int Position;

/**
 * Player 0 - Cross, Player 1 - Nought
*/
enum CellState {Empty, Cross, Nought};
enum ActionResult {Success, Failure};

/**
 * Observability changes the information, provided in opponent observation
 *
 * FullyObservable - opponent always obtains move position (non-phantom Tic-Tac-toe)
 * also, when FullyObservable, actions that would lead to Failure are not present in the actions list
 *
 * PartialObservable - opponent obtains move position if this move was a failure
 * this information can be used by some algorithms to predict opponent strategy
 *
 * NonObservable - opponent never obtains move position (phantom Tic-Tac-toe)
*/
enum Observability {FullyObservable, PartialObservable, NonObservable};

class PhantomTTTAction: public Action {
 public:
    PhantomTTTAction(ActionId id, Position move) : Action(id), move_(move) {};
    inline string toString() const final {
        return "Trying to put a mark at position" + to_string(move_);
    }

    bool operator==(const Action &that) const override;
    HashType getHash() const override { return id_; };
    const Position move_;
};

/**
 * Observation contains following information:
 *
 * position_ - position at the board, where the mark was put
 *
 * result_ - Success if that position was not occupied by the opponent mark, otherwise Failure
 *
 * Depending on the observability_ domain parameter, different information is revealed, see above
 */
class PhantomTTTObservation: public Observation {
 public:
    inline explicit PhantomTTTObservation(ActionResult result, Position position) : Observation(),
                                            result_(result), position_(position) {
        id_ = (unsigned int)(position+1) + ((result_ == Success ? 1u : 0u) << 30u);
    }
    inline explicit PhantomTTTObservation() : PhantomTTTObservation(Failure,-1) {};
    inline explicit PhantomTTTObservation(ActionResult result) : PhantomTTTObservation(result,-1) {}
    const Position position_;
    const ActionResult result_;
};

/**
 * Phantom Tic-Tac-Toe Domain with variable board size and win conditions.
 *
 * victorySequenceLength_ - number of same marks in a sequence required to win (row, column, diagonal)
 * must be less or equal to boardSize_
 *
 * boardSize_ - board size (both height and width)
 *
 * observability_ - changes how much information is revealed to the opponent in their observation (see above)
 */
class PhantomTTTDomain: public Domain {
 public:
    explicit PhantomTTTDomain(int boardSize, int victorySequenceLength, Observability observability);
    ~PhantomTTTDomain() override = default;
    string getInfo() const final;
    const int boardSize_;
    const int victorySequenceLength_;
    const Observability observability_;
    static unique_ptr<PhantomTTTDomain> classicTTT3_3();
    static unique_ptr<PhantomTTTDomain> classicTTT4_3();
    static unique_ptr<PhantomTTTDomain> phantomTTT3_3();
    static unique_ptr<PhantomTTTDomain> phantomTTT4_3();
    static unique_ptr<PhantomTTTDomain> phantomTTT5_4();
 private:
    static int calculateMaxDepth(int size) ;
};

/**
 * playedPos_ contains positions, which current player tried to occupy and failed. This positions are not available as
 * legal moves to avoid looping. Cleared after the successful move.
 */
class PhantomTTTState: public State {
 public:
    inline PhantomTTTState(const Domain *domain, vector<CellState> board, vector<Position> &playedPos,
                           Player currentPlayer, bool isTerminal)
        : State(domain, hashCombine(324816846515, cellStatesToInt(board), currentPlayer, isTerminal)),
          board_(move(board)),
          currentPlayer_(currentPlayer),
          isTerminal_(isTerminal),
          playedPos_(std::move(playedPos)),
          domain_(dynamic_cast<const PhantomTTTDomain *>(domain)) {};

    unsigned long countAvailableActionsFor(Player player) const override;
    vector <shared_ptr<Action>> getAvailableActionsFor(Player player) const override;
    shared_ptr<Action> getActionByID(Player player, ActionId action) const override;
    OutcomeDistribution performActions(const vector <shared_ptr<Action>> &actions) const override;
    inline vector <Player> getPlayers() const final { if (!isTerminal_) return {currentPlayer_}; else return {};}
    inline bool isTerminal() const override { return isTerminal_; };
    bool operator==(const State &rhs) const override;
    inline string toString() const override;

 protected:
    CellState checkHorizontal(const vector<CellState>& board) const;
    CellState checkVertical(const vector<CellState>& board) const;
    CellState checkDiagonal(const vector<CellState>& board) const;
    static bool checkForDraw(const vector<CellState> &board) ;
    static vector<int> cellStatesToInt(const vector<CellState>& board) ;
    const PhantomTTTDomain* domain_;
    const vector<CellState> board_;
    const vector<Position> playedPos_;
    const Player currentPlayer_;
    const bool isTerminal_;
};
}
#endif  // GTLIB2_PHANTOMTTT_H_


/**
    Copyright 2020 Faculty of Electrical Engineering at CTU in Prague

    This file is part of Game Theoretic Library.

    Game Theoretic Library is free software: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License
    as published by the Free Software Foundation, either version 3
    of the License, or (at your option) any later version.

    Game Theoretic Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with Game Theoretic Library.

    If not, see <http://www.gnu.org/licenses/>.
*/

#include "darkchess.h"
#include <utility>

namespace GTLib2::domains::chess {
const int bishopMoves[4][2] = {{-1, 1},
                               {-1, -1},
                               {1, -1},
                               {1, 1}};
const int rookMoves[4][2] = {{1, 0},
                             {0, 1},
                             {-1, 0},
                             {0, -1}};
const int queenKingMoves[8][2] = {{1, 0},
                                  {0, 1},
                                  {-1, 0},
                                  {0, -1},
                                  {-1, 1},
                                  {-1, -1},
                                  {1, -1},
                                  {1, 1}};
const int knightMoves[8][2] = {{1, 2},
                               {-1, 2},
                               {-1, -2},
                               {1, -2},
                               {2, 1},
                               {-2, 1},
                               {-2, -1},
                               {2, -1}};

DarkchessPiece::DarkchessPiece(chess::pieceName k,
                               int c,
                               Square pos,
                               const DarkchessState *s)
    : color(c), kind(k), position(pos), board(s) {}

//                      kind   color            position                 board
DarkchessPawn::DarkchessPawn(chess::pieceName k, int c, chess::Square p, const DarkchessState *b, int id)
    : DarkchessPiece(k, c, p, b), id(id) {}

DarkchessRook::DarkchessRook(chess::pieceName k, int c, chess::Square p, const DarkchessState *b)
    : DarkchessPiece(k, c, p, b) {}

DarkchessQueen::DarkchessQueen(chess::pieceName k, int c, chess::Square p, const DarkchessState *b)
    : DarkchessPiece(k, c, p, b) {}

DarkchessKnight::DarkchessKnight(chess::pieceName k, int c, chess::Square p, const DarkchessState *b)
    : DarkchessPiece(k, c, p, b) {}

DarkchessBishop::DarkchessBishop(chess::pieceName k, int c, chess::Square p, const DarkchessState *b)
    : DarkchessPiece(k, c, p, b) {}

DarkchessKing::DarkchessKing(chess::pieceName k, int c, chess::Square p, const DarkchessState *b) :
    DarkchessPiece(k, c, p, b) {}

DarkchessKing::DarkchessKing(chess::pieceName k, int c, chess::Square p, const DarkchessState *b, bool mo)
    : DarkchessPiece(k, c, p, b) {
  this->moved = mo;
}

DarkchessRook::DarkchessRook(chess::pieceName k, int c, chess::Square p, const DarkchessState *b, bool mo)
    : DarkchessPiece(k, c, p, b) {
  this->moved = mo;
}

shared_ptr<DarkchessPiece> DarkchessQueen::clone() const {
  return make_shared<DarkchessQueen>(this->kind, this->color, this->position, this->board);
}

shared_ptr<DarkchessPiece> DarkchessPawn::clone() const {
  return make_shared<DarkchessPawn>(this->kind, this->color, this->position, this->board, this->id);
}

shared_ptr<DarkchessPiece> DarkchessKnight::clone() const {
  return make_shared<DarkchessKnight>(this->kind, this->color, this->position, this->board);
}

shared_ptr<DarkchessPiece> DarkchessKing::clone() const {
  return make_shared<DarkchessKing>(this->kind, this->color, this->position, this->board, this->moved);
}

shared_ptr<DarkchessPiece> DarkchessRook::clone() const {
  return make_shared<DarkchessRook>(this->kind, this->color, this->position, this->board, this->moved);
}

shared_ptr<DarkchessPiece> DarkchessBishop::clone() const {
  return make_shared<DarkchessBishop>(this->kind, this->color, this->position, this->board);
}

void DarkchessPiece::move(Square pos) {
  this->position = pos;
  this->moved = true;
}

bool DarkchessPiece::hasMoved() const {
  if (this->kind != PAWN) {
    return this->moved;
  } else {
    if (this->getColor() == WHITE) {
      return this->position.y != 2;
    } else {
      return this->position.y != this->board->getYSize() - 1;
    }
  }
}

vector<Square> *DarkchessPiece::getAllMoves() const {
  return this->moves.get();
}

void DarkchessPiece::update() {
  this->updateMoves();
}

void DarkchessPiece::reset() {
  this->moves->clear();
  this->observedPieces->clear();
}

string DarkchessPiece::toString() const {
  return std::string(1, this->color == chess::WHITE ? toupper(this->kind) : tolower(this->kind));
}

bool DarkchessPiece::operator==(const chess::DarkchessPiece &that) const {
  return this->getPosition() == that.getPosition() && this->getKind() == that.getKind()
      && this->getColor() == that.getColor();
}

int DarkchessPiece::getColor() const {
  return this->color;
}

Square DarkchessPiece::getPosition() const {
  return this->position;
}

bool DarkchessPiece::equal_to(const DarkchessPiece &that) const {
  return this->getColor() == that.getColor() && this->getPosition() == that.getPosition()
      && this->getKind() == that.getKind();
}

void DarkchessPawn::updateMoves() {
  //cut moves
  int dy = this->color == WHITE ? 1 : -1;
  Square takeLeft(this->position.x - 1, this->position.y + dy);
  Square takeRight(this->position.x + 1, this->position.y + dy);
  Square moveForward(this->position.x, this->position.y + dy);
  Square moveByTwo = this->hasMoved() ? Square(-1, -1) :
                     Square(this->position.x, this->position.y + 2 * dy);

  for (const Square &move : {takeLeft, takeRight}) {
    if (!this->board->coordOutOfBounds(move)) {
      auto piece = this->board->getPieceOnCoords(move);
      if (piece != nullptr && piece->getColor() != this->getColor()) {
        moves->push_back(move);
        observedPieces->push_back(move);
      } else if (move == this->board->getEnPassantSquare() && this->board->getPlayerOnMove() == this->color) { // taking enpassant
        moves->push_back(move);
        Square enpassant(this->board->getEnPassantSquare().x, this->position.y);
        observedPieces->push_back(enpassant);

        assert(this->board->getPieceOnCoords(enpassant) != nullptr);
      }
    }
  }

  if (!this->board->coordOutOfBounds(moveForward)) {
    auto piece = this->board->getPieceOnCoords(moveForward);
    if (piece == nullptr) {
      moves->push_back(moveForward);
      piece = this->board->getPieceOnCoords(moveByTwo);
      if (!this->board->coordOutOfBounds(moveByTwo) && !hasMoved()) {
        piece = this->board->getPieceOnCoords(moveByTwo);
        if (piece == nullptr) {
          moves->push_back(moveByTwo);
        } else if (piece->getColor() != this->getColor()) {
          observedPieces->push_back(moveByTwo);
        }
      }
    } else if (piece->getColor() != this->getColor()) {
      observedPieces->push_back(moveForward);
    }
  }
}

int DarkchessPawn::getId() const {
  return this->id;
}

void DarkchessRook::updateMoves() {
  for (auto move: rookMoves) {
    int dx = this->position.x;
    int dy = this->position.y;
    bool breakInner = false;
    dx += move[0];
    dy += move[1];
    //for cycle runs until coordinates are out of bounds
    for (; !this->board->coordOutOfBounds(Square(dx, dy)); dx += move[0], dy += move[1]) {
      if (breakInner) break;
      Square newPos(dx, dy);
      shared_ptr<DarkchessPiece> p = this->board->getPieceOnCoords(newPos);
      if (p != nullptr) {
        breakInner = true;
        if (p->getColor() != this->getColor()) {
          observedPieces->push_back(newPos);
        } else {
          continue;
        }
      }
      this->moves->push_back(newPos);
    }
  }
}

void DarkchessKnight::updateMoves() {
  for (auto move: knightMoves) {
    Square newPos(this->position.x + move[0], this->position.y + move[1]);
    if (!this->board->coordOutOfBounds(newPos)) {
      shared_ptr<DarkchessPiece> p = this->board->getPieceOnCoords(newPos);
      if (p == nullptr) {
        moves->push_back(newPos);
      } else if (p != nullptr && p->getColor() != this->getColor()) {
        moves->push_back(newPos);
        observedPieces->push_back(newPos);
      }
    }
  }
}

void DarkchessBishop::updateMoves() {
  for (auto move : bishopMoves) {
    int dx = this->position.x;
    int dy = this->position.y;
    bool breakInner = false;
    dx += move[0];
    dy += move[1];
    //for cycle runs until coordinates are out of bounds
    for (; !this->board->coordOutOfBounds(Square(dx, dy)); dx += move[0], dy += move[1]) {
      if (breakInner) break;
      Square newPos(dx, dy);
      shared_ptr<DarkchessPiece> p = this->board->getPieceOnCoords(newPos);
      if (p != nullptr) {
        breakInner = true;
        if (p->getColor() != this->getColor()) {
          observedPieces->push_back(newPos);
        } else {
          continue;
        }
      }
      this->moves->push_back(newPos);
    }
  }
}

void DarkchessQueen::updateMoves() {
  for (auto move: queenKingMoves) {
    int dx = this->position.x;
    int dy = this->position.y;
    bool breakInner = false;
    dx += move[0];
    dy += move[1];
    //for cycle runs until coordinates are out of bounds
    for (; !this->board->coordOutOfBounds(Square(dx, dy)); dx += move[0], dy += move[1]) {
      if (breakInner) continue;
      Square newPos(dx, dy);
      shared_ptr<DarkchessPiece> p = this->board->getPieceOnCoords(newPos);
      if (p != nullptr) {
        breakInner = true;
        if (p->getColor() != this->getColor()) {
          observedPieces->push_back(newPos);
        } else {
          continue;
        }
      }
      this->moves->push_back(newPos);
    }
  }
}

void DarkchessKing::updateMoves() {
  for (auto move: queenKingMoves) {
    Square newPos(this->position.x + move[0], this->position.y + move[1]);
    if (!this->board->coordOutOfBounds(newPos)) {
      shared_ptr<DarkchessPiece> p = this->board->getPieceOnCoords(newPos);
      if (p != nullptr) {
        if (p->getColor() != this->getColor()) {
          observedPieces->push_back(newPos);
        } else {
          continue;
        }
      }
      this->moves->push_back(newPos);
    }
  }
  // castle
  if (!this->hasMoved() && this->board->canCastle()) {
    auto rooks = this->board->getPiecesOfColorAndKind(this->color, ROOK);
    for (const auto &rook: rooks) {
      if (!rook->hasMoved()) {
        vector<Square> squaresBetween = this->board->getSquaresBetween(rook.get(), this);
        if (squaresBetween.size() == 2) {
          //short castle
          bool pieceBetween = false;
          //check if there are pieces in the way
          for (Square square: squaresBetween) {
            if (this->board->getPieceOnCoords(square) != nullptr) {
              pieceBetween = true;
              break;
            }
          }
          if (pieceBetween) continue;

          int dx = rook->getPosition().x > this->position.x ? rook->getPosition().x - 1 :
                   rook->getPosition().x + 1;
          Square newPos(dx, this->position.y);
          this->moves->push_back(newPos);
        } else {
          //long castle
          bool pieceBetween = false;
          //check if there are pieces in the way
          for (Square square: squaresBetween) {
            if (this->board->getPieceOnCoords(square) != nullptr) {
              pieceBetween = true;
              break;
            }
          }
          if (pieceBetween) continue;

          int dx = rook->getPosition().x > this->position.x ? rook->getPosition().x - 2 :
                   rook->getPosition().x + 2;
          Square newPos(dx, this->position.y);
          this->moves->push_back(newPos);
        }
      }
    }
  }
}

} // namespace GTLib2::domain::chess

namespace GTLib2::domains {
using chess::Square;

DarkchessAction::DarkchessAction(ActionId id, pair<shared_ptr<DarkchessPiece>, Square> move,
                                 const Square &originalPos) :
    Action(id), move_(std::move(move)), moveFrom_(originalPos) {}

DarkchessAction::DarkchessAction(ActionId id) :
    Action(id), moveFrom_(-1, -1),
    move_(pair<shared_ptr<DarkchessPiece>, Square>(nullptr, Square(-1, -1))) {}

bool DarkchessAction::operator==(const GTLib2::Action &that) const {
  const auto rhsAction = dynamic_cast<const DarkchessAction *>(&that);
  return this->move_.first->equal_to(*rhsAction->move_.first)
      && this->move_.second == rhsAction->move_.second
      && this->movingFrom() == rhsAction->movingFrom();
}

HashType DarkchessAction::getHash() const {
  return id_;
}

shared_ptr<DarkchessAction> DarkchessAction::clone() const {
  Square s = Square(this->getMove().second.x, this->getMove().second.y);
  shared_ptr<DarkchessPiece> p = this->getMove().first->clone();
  Square f = Square(this->movingFrom().x, this->movingFrom().y);
  return make_shared<DarkchessAction>(id_, pair<shared_ptr<DarkchessPiece>, chess::Square>(p, s), f);
}

pair<shared_ptr<DarkchessPiece>, Square> DarkchessAction::getMove() const {
  return this->move_;
}

Square DarkchessAction::movingFrom() const {
  return this->moveFrom_;
}

DarkchessDomain::DarkchessDomain(unsigned int maxHalfMoves, chess::BOARD board)
    : Domain(maxHalfMoves + 1, 2, true, // + 1 for root state
             make_shared<DarkchessAction>(),
             make_shared<DarkchessObservation>()) {
  vector<double> rewards(2, 0.0);
  Outcome rootState
      (make_shared<DarkchessState>(this, maxHalfMoves, board),
       {make_shared<Observation>(NO_OBSERVATION),
        make_shared<Observation>(NO_OBSERVATION)},
       make_shared<Observation>(NO_OBSERVATION),
       rewards);
  rootStatesDistribution_.push_back(OutcomeEntry(rootState));
  maxUtility_ = 1;
}
DarkchessDomain::DarkchessDomain(unsigned int maxHalfMoves, string &FEN) : Domain(maxHalfMoves + 1, 2, true, // + 1 for root state
                                            make_shared<DarkchessAction>(),
                                            make_shared<DarkchessObservation>()) {
  vector<double> rewards(2, 0.0);
  Outcome rootState
      (make_shared<DarkchessState>(this, maxHalfMoves, FEN),
       {make_shared<Observation>(NO_OBSERVATION),
        make_shared<Observation>(NO_OBSERVATION)},
       make_shared<Observation>(NO_OBSERVATION),
       rewards);
  rootStatesDistribution_.push_back(OutcomeEntry(rootState));
  maxUtility_ = 1;

}

DarkchessState::DarkchessState(const Domain *domain,
                               unsigned int maxHalfMoves,
                               chess::BOARD board) :
    State(domain, hashCombine(56456654521424531, maxHalfMoves, int(board))), enPassantSquare_(-1, -1),
    playerOnMove_(chess::WHITE),
    movesPlayed_(0), // root state, no moves were made
    legalMaxDepth_(maxHalfMoves) {
  this->initBoard(board);
  this->updateState(chess::WHITE);
}

DarkchessState::DarkchessState(const Domain *domain, unsigned int maxHalfMoves, string &FEN) :
    State(domain, hashCombine(56456654521424531, maxHalfMoves, FEN)), enPassantSquare_(-1, -1),
    playerOnMove_(chess::WHITE),
    movesPlayed_(0), // root state, no moves were made
    legalMaxDepth_(maxHalfMoves) {
  this->initBoard(FEN);
  this->updateState(chess::WHITE);

}

DarkchessState::DarkchessState(const Domain *domain,
                               unsigned int legalMaxDepth,
                               unsigned int halfMoveNumber,
                               int x,
                               int y,
                               shared_ptr<vector<shared_ptr<chess::DarkchessPiece>>> pieces,
                               Square enPassantSquare,
                               int p,
                               bool castle)
// todo: hash is not computed from shared_ptr<vec<shared_ptr>> as there is not support for that atm
    : State(domain,
            hashCombine(3156841351231,
                        halfMoveNumber,
                        enPassantSquare,
                        x,
                        y,
                        legalMaxDepth,
                        p,
                        castle)),
      enPassantSquare_(enPassantSquare),
      xSize_(x),
      ySize_(y),
      pieces_(std::move(pieces)),
      legalMaxDepth_(legalMaxDepth),
      movesPlayed_(halfMoveNumber),
      canPlayerCastle_(castle),
      playerOnMove_(p) {

  //rebind to new board state
  for (shared_ptr<DarkchessPiece> &piece: *this->pieces_) {
    piece->setBoard(this);
  }
}

// todo: this is only hotfix
unsigned long DarkchessState::countAvailableActionsFor(Player player) const {
  return getAvailableActionsFor(player).size();
}

vector<shared_ptr<Action>> DarkchessState::getAvailableActionsFor(Player player) const {
  vector<shared_ptr<Action>> actions;
  if (player != this->playerOnMove_
      || this->gameHasEnded_
      || this->movesPlayed_ == this->legalMaxDepth_) {
    return actions;
  }
  // first move of white -> only give valid moves
  int count = 0;
  for (const auto &piece: this->getPiecesOfColor(player)) {
    for (Square move: *piece->getAllMoves()) {
      auto newAction = make_shared<DarkchessAction>(
          count, pair<shared_ptr<DarkchessPiece>, Square>(piece, move),
          piece->getPosition());
      actions.push_back(newAction);
      ++count;
    }
  }
  return actions;
}

void DarkchessState::promote(const shared_ptr<DarkchessPiece> &p, Square pos) const {
  auto queen = make_shared<chess::DarkchessQueen>(chess::QUEEN, p->getColor(), pos, this);
  for (auto iterator = this->pieces_->begin(); iterator != this->pieces_->end();) {
    if (*(*iterator) == *p) {
      this->pieces_->erase(iterator);
      this->pieces_->push_back(queen);
      break;
    } else {
      ++iterator;
    }
  }
}

vector<shared_ptr<DarkchessPiece>> DarkchessState::getPiecesOfColor(int color) const {
  vector<shared_ptr<DarkchessPiece>> pieces;
  for (auto &p: *this->pieces_) {
    if (p->getColor() == color) {
      pieces.push_back(p);
    }
  }
  return pieces;
}

void DarkchessState::castle(DarkchessAction *a) const {
  pair<shared_ptr<DarkchessPiece>, Square> move = a->getMove();
  shared_ptr<DarkchessPiece> king = move.first;
  Square pos = move.second;
  int dx = king->getPosition().x;
  int dy = king->getPosition().y;
  //get rook in the direction of the move so we don't have to make special cases for other boards and black/white board asymmetry
  int incrementX = pos.x > dx ? 1 : -1;
  shared_ptr<DarkchessPiece> rook;
  dx += incrementX;
  for (; ((rook = this->getPieceOnCoords(Square(dx, dy))) == nullptr); dx += incrementX);
  int newRookX = pos.x > rook->getPosition().x ? pos.x + 1 : pos.x - 1;
  Square newRookPos(newRookX, rook->getPosition().y);
  rook->move(newRookPos);
}

void DarkchessState::makeMove(DarkchessAction *a) {
  auto move = a->getMove();
  auto movePiece = this->getPieceOnCoords(move.first->getPosition());
  Square pos = move.second;
  //move valid
  shared_ptr<DarkchessPiece> checkCapture = this->getPieceOnCoords(pos);
  if (checkCapture != nullptr) {
    //capturedPawn
    this->capturedPiece_ = checkCapture;
    for (auto It = this->pieces_->begin(); It != this->pieces_->end();) {
      if (*(*It) == *checkCapture) {
        this->pieces_->erase(It);
        break;
      } else {
        ++It;
      }
    }
  }
  //en passant
  if (movePiece->getKind() == chess::PAWN && pos.x == this->enPassantSquare_.x
      && pos.y == this->enPassantSquare_.y) {
    int pawnColor = chess::invertColor(movePiece->getColor());
    int y = pawnColor == chess::WHITE ? pos.y + 1 : pos.y - 1;
    Square enPassPos(pos.x, y);
    shared_ptr<DarkchessPiece> enPassPawn = this->getPieceOnCoords(enPassPos);
    if (enPassPawn != nullptr) {
      this->capturedPiece_ = enPassPawn;
      for (auto It = this->pieces_->begin(); It != this->pieces_->end();) {
        if (*(*It) == *enPassPawn) {
          this->pieces_->erase(It);
          break;
        } else {
          ++It;
        }
      }
    }
  }
  // castle
  if (movePiece->getKind() == chess::KING && abs(a->movingFrom().x - pos.x) > 1) {
    this->castle(a);
  }
  movePiece->move(pos);
  // promote
  if (movePiece->getKind() == chess::PAWN
      && ((pos.y == this->getYSize() && movePiece->getColor() == chess::WHITE)
          || (pos.y == 1 && movePiece->getColor() == chess::BLACK))) {
    this->promote(movePiece, pos);
  }
}

int DarkchessState::checkGameOver() const {
  int result = 0; // 0 = play continue, 1 = player on the move lost, 2 = draw
  // king was captured
  if (this->capturedPiece_ != nullopt) {
    DarkchessPiece *piece = this->capturedPiece_->get();
    if (piece->getKind() == chess::KING) {
      result = 1;
    }
  } else if (this->movesPlayed_ == this->legalMaxDepth_) {
    // reached predefined depth
    result = 2;
  }
  return result;
}

OutcomeDistribution DarkchessState::performActions(const vector<shared_ptr<Action>> &actions) const {
  auto a1 = dynamic_cast<DarkchessAction *>(actions[0].get());
  auto a2 = dynamic_cast<DarkchessAction *>(actions[1].get());
  vector<shared_ptr<Observation>> observations(2);
  shared_ptr<Observation> publicObservation;
  vector<double> rewards(2);
  shared_ptr<DarkchessState> s;
  int playerOnMove = this->playerOnMove_;
  Square enPassSquare(-1, -1);
  DarkchessAction *a = a1->getMove().first != nullptr ? a1 : a2;

  shared_ptr<vector<shared_ptr<DarkchessPiece>>> pieces = this->copyPieces();

  s = make_shared<DarkchessState>(domain_,
                                  this->legalMaxDepth_,
                                  this->movesPlayed_ + 1,
                                  this->xSize_,
                                  this->ySize_,
                                  pieces,
                                  this->enPassantSquare_,
                                  playerOnMove,
                                  this->canPlayerCastle_);
  s->updateState(playerOnMove);

  shared_ptr<DarkchessAction> ac = a->clone();
  s->makeMove(a); // valid move made
  enPassSquare = this->checkEnPassant(a);

  playerOnMove = chess::invertColor(playerOnMove);
  Player opponent = chess::invertColor(playerOnMove);
  assert(playerOnMove != opponent);

  s->setPlayerOnMove(playerOnMove);
  s->setEnPassant(enPassSquare);
  s->updateState(playerOnMove);

  vector<string> board(2);
  observations[playerOnMove] = s->getPrivateObservation(playerOnMove, board);
  observations[opponent] = s->getPrivateObservation(opponent, board);
  publicObservation = s->getPublicObservation(board);

  switch (s->checkGameOver()) {
    case 0:rewards[0] = 0.0;
      rewards[1] = 0.0;
      break;
    case 1:rewards[s->playerOnMove_] = -1.0;
      rewards[chess::invertColor(s->playerOnMove_)] = 1.0;
      s->setGameHasEnded(true);
      break;
    case 2:rewards[0] = 0.;
      rewards[1] = 0.;
      s->setGameHasEnded(true);
      break;
    default:unreachable("wrong return value of checkGameOver()!");
  }

  Outcome o(s, observations, publicObservation, rewards);
  OutcomeDistribution prob;
  prob.push_back(OutcomeEntry(o));
  return prob;
}

Square DarkchessState::checkEnPassant(DarkchessAction *a) const {
  shared_ptr<DarkchessPiece> abstractPiece = a->getMove().first;
  Square pos(-1, -1);
  if (abstractPiece->getKind() != chess::PAWN) {
    return pos;
  } else {
    if ((a->getMove().second.y - a->movingFrom().y) == 2) {
      pos.x = a->movingFrom().x;
      pos.y = a->movingFrom().y + 1;
      return pos;
    } else if ((a->getMove().second.y - a->movingFrom().y) == -2) {
      pos.x = a->movingFrom().x;
      pos.y = a->movingFrom().y - 1;
      return pos;
    } else {
      return pos;
    }
  }
}

bool DarkchessState::operator==(const GTLib2::State &rhs) const {
  const auto rhsState = dynamic_cast<const DarkchessState *>(&rhs);
  if (this->xSize_ != rhsState->xSize_ || this->ySize_ != rhsState->ySize_) {
    return false;
  }
  for (int i = 1; i <= this->xSize_; i++) {
    for (int j = 1; j <= this->ySize_; j++) {
      shared_ptr<DarkchessPiece> p1 = this->getPieceOnCoords(Square(i, j));
      shared_ptr<DarkchessPiece> p2 = this->getPieceOnCoords(Square(i, j));
      if (p1 == nullptr && p2 == nullptr) continue;
      if ((p1 != nullptr && p2 == nullptr) || (p1 == nullptr && p2 != nullptr)) return false;
      if (!(*p1 == *p2)) return false;
    }
  }
  return true;
}

string DarkchessState::toString() const {
  //DEPRECATED
  vector<vector<string>> board(ySize_, vector<string>(xSize_, "_"));
  for (const shared_ptr<DarkchessPiece> &p: *this->pieces_) {
    Square pos = p->getPosition();
    pos.y -= 1;
    pos.x -= 1;
    board.at(pos.y).at(pos.x) = p->toString();
  }

  string s;
  for (int i = this->ySize_ - 1; i >= 0; i--) {
    s += to_string(i + 1) + " ";
    vector<string> row = board.at(i);
    for (const string &str: row) {
      s += str;
    }
    s += "\n";
  }

  s += "  ";
  for (int i = 0; i < this->xSize_; i++) s += 'a' + i;
  s += "\n";
  return s;
}

shared_ptr<DarkchessPiece> DarkchessState::getPieceOnCoords(Square pos) const {
  for (const shared_ptr<DarkchessPiece> &p: *this->pieces_) {
    if (p->getPosition() == pos) {
      return p;
    }
  }
  return nullptr;
}

bool DarkchessState::coordOutOfBounds(Square pos) const {
  return pos.x > this->xSize_ || pos.y > this->ySize_ || pos.x < 1 || pos.y < 1;
}

void DarkchessState::updateAllPieces() const {
  for (shared_ptr<DarkchessPiece> &p: *this->pieces_) {
    p->update();
  }
}

void DarkchessState::resetAllPieces() {
  for (shared_ptr<DarkchessPiece> &p: *this->pieces_) {
    p->reset();
  }
}

void DarkchessState::updatePiecesOfColor(int color) const {
  for (shared_ptr<DarkchessPiece> &piece: this->getPiecesOfColor(color)) {
    piece->update();
  }
}

vector<shared_ptr<DarkchessPiece>> DarkchessState::getPiecesOfColorAndKind(int c, chess::pieceName n) const {
  vector<shared_ptr<DarkchessPiece>> v;
  for (const shared_ptr<DarkchessPiece> &p: *this->pieces_) {
    if (p->getColor() == c && p->getKind() == n) {
      v.push_back(p);
    }
  }
  return v;
}

void DarkchessState::clearBoard() {
  this->pieces_->clear();
}

void DarkchessState::insertPiece(const shared_ptr<DarkchessPiece> &p) {
  this->pieces_->push_back(p);
}

vector<Square> DarkchessState::getSquaresBetween(DarkchessPiece *p1, DarkchessPiece *p2) const {

  Square pos1 = p1->getPosition();
  Square pos2 = p2->getPosition();
  vector<Square> squares;

  int dx = pos1.x;
  int dy = pos1.y;

  int incrementX = dx == pos2.x ? 0 : dx < pos2.x ? 1 : -1;
  int incrementY = dy == pos2.y ? 0 : dy < pos2.y ? 1 : -1;

  dx += incrementX;
  dy += incrementY;

  for (; !(dx == pos2.x && dy == pos2.y); dx += incrementX, dy += incrementY) {
    Square newPos(dx, dy);
    if (this->coordOutOfBounds(newPos)) {
      string s1 = "Position 1: " + to_string(p1->getPosition().x) + ", "
          + to_string(p1->getPosition().y) + "\r\n"
                                             "Figure 1: " + p1->toString() + "\r\n";
      string s2 = "Position 2: " + to_string(p2->getPosition().x) + ", "
          + to_string(p2->getPosition().y) + "\r\n"
                                             "Figure 2: " + p2->toString() + "\r\n";
      cout << s1;
      cout << s2;
      throw std::invalid_argument("wrong figures");
    }
    squares.push_back(newPos);
  }

  return squares;
}

void DarkchessState::initBoard(chess::BOARD b) {
  chess::BoardFactory bf;
  chess::boardInfoDarkchess binfo = bf.create(b, this);
  this->pieces_ = binfo.pieces;
  this->xSize_ = binfo.x;
  this->ySize_ = binfo.y;
  this->canPlayerCastle_ = binfo.castle;
}

void DarkchessState::initBoard(string FEN) {
  chess::FenBoardFactory bf;
  chess::boardInfoDarkchess binfo = bf.create(std::move(FEN), this);
  this->pieces_ = binfo.pieces;
  this->xSize_ = binfo.x;
  this->ySize_ = binfo.y;
  this->canPlayerCastle_ = binfo.castle;
}

int DarkchessState::getXSize() const {
  return this->xSize_;
}

int DarkchessState::getYSize() const {
  return this->ySize_;
}

void DarkchessState::setGameHasEnded(bool gameHasEnded) {
  this->gameHasEnded_ = gameHasEnded;
}

shared_ptr<vector<shared_ptr<DarkchessPiece>>> DarkchessState::copyPieces() const {
  shared_ptr<vector<shared_ptr<DarkchessPiece>>>
      v = make_shared<vector<shared_ptr<DarkchessPiece>>>();
  for (const shared_ptr<DarkchessPiece> &p: *pieces_) {
    auto asdf = p->clone();
    v->push_back(asdf);
  }
  return v;
}

void DarkchessState::setEnPassant(chess::Square s) {
  this->enPassantSquare_ = s;
  //refresh moves on pawns after setting enpassant square
  vector<shared_ptr<DarkchessPiece>>
      pawns = this->getPiecesOfColorAndKind(this->playerOnMove_, chess::PAWN);
  for (shared_ptr<DarkchessPiece> &p: pawns) {
    p->update();
  }
}

void DarkchessState::updateState(int p) {
  this->playerOnMove_ = p;
  this->resetAllPieces();
  this->updatePiecesOfColor(chess::invertColor(this->playerOnMove_));
  this->updatePiecesOfColor(this->playerOnMove_);
}

shared_ptr<Observation> DarkchessState::getPublicObservation(const vector<string> &board) const {
  ObservationId id;
  vector<char> b(this->xSize_ * this->ySize_, '-');
  auto pieces = this->getPiecesOfColor(0);
  int c = this->xSize_;
  for (auto &piece : pieces) {
    Square pos = piece->getPosition();
    for (auto &m: *piece->getObservedPieces()) {
      auto observed = this->getPieceOnCoords(m);
      for (auto &l: *observed->getObservedPieces()) {
        if (l == pos) {
          b[(pos.y - 1) * c + pos.x - 1] = piece->getKind() - 32;
          b[(m.y - 1) * c + m.x - 1] = observed->getKind();
          break;
        }
      }
    }
  }
  string FEN(b.begin(), b.end());
  id = static_cast<ObservationId>(hashCombine(4654871649, FEN));
  return make_shared<DarkchessObservation>(id, FEN, this->xSize_, this->ySize_);
}

shared_ptr<Observation> DarkchessState::getPrivateObservation(Player player, vector<string> &board) {

  auto correctCase = [](Player player, char kind) {
    return player == 0 ? (kind - 32) : kind;
  };

  ObservationId id;
  vector<char> b(this->xSize_ * this->ySize_, '-');
  auto pieces = this->getPiecesOfColor(player);
  int c = this->xSize_;
  for (auto &piece : pieces) {
    Square pos = piece->getPosition();
    b[(pos.y - 1) * c + pos.x - 1] = correctCase(player, piece->getKind());
    for (auto &m : *piece->getAllMoves()) {
      b[(m.y - 1) * c + m.x - 1] = 'E';
    }
    for (auto &m: *piece->getObservedPieces()) {
      b[(m.y - 1) * c + m.x - 1] = correctCase(chess::invertColor(player),
                                               this->getPieceOnCoords(m)->getKind());
    }
  }
  string FEN(b.begin(), b.end());
  board[player] = FEN;
  id = static_cast<ObservationId>(hashCombine(4487414567, FEN));

  return make_shared<DarkchessObservation>(id, FEN);
}

}
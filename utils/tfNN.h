/*
    Copyright 2019 Faculty of Electrical Engineering at CTU in Prague

    This file is part of Game Theoretic Library.

    Game Theoretic Library is free software: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License
    as published by the Free Software Foundation, either version 3
    of the License, or (at your option) any later version.

    Game Theoretic Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Game Theoretic Library.

    If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TFNN_H
#define TFNN_H

#include <iostream>
#include <initializer_list>
#include <tensorflow/core/platform/env.h>
#include <tensorflow/core/public/session.h>
#include <tensorflow/core/protobuf/meta_graph.pb.h>
#include "tensorflow/cc/framework/ops.h"

using namespace std;
using namespace tensorflow;

class TFNeuralNetwork {
private:
    Session *session;
    string inputLayer;
    string outputLayer;

public:
    TFNeuralNetwork(const string& path_to_graph, const string& checkpoint_path, string  inputLayer, string  outputLayer);
    ~TFNeuralNetwork();
    vector<double> predict(vector<float> input_vector);
};


#endif //TFNN_H

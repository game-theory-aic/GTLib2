/*
    Copyright 2019 Faculty of Electrical Engineering at CTU in Prague

    This file is part of Game Theoretic Library.

    Game Theoretic Library is free software: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License
    as published by the Free Software Foundation, either version 3
    of the License, or (at your option) any later version.

    Game Theoretic Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Game Theoretic Library.

    If not, see <http://www.gnu.org/licenses/>.
*/

#include "tfNN.h"

#include <utility>
#include "utils.h"

TFNeuralNetwork::TFNeuralNetwork(const string &path_to_graph,
                                 const string &checkpoint_path,
                                 string _inputLayer,
                                 string _outputLayer)
    : inputLayer(std::move(_inputLayer)), outputLayer(std::move(_outputLayer)) {
  // taken from https://medium.com/jim-fleming/loading-a-tensorflow-graph-with-the-c-api-4caaff88463f

  // Create a new session
  Status status = NewSession(SessionOptions(), &this->session);
  if (!status.ok()) {
    throw runtime_error("Error creating new session: " + status.ToString());
  }

  // Read in the protobuf graph we exported
  MetaGraphDef graph_def;
  status = ReadBinaryProto(Env::Default(), path_to_graph, &graph_def);
  if (!status.ok()) {
    throw runtime_error("Error reading graph definition: " + status.ToString());
  }

  // Add the graph to the session
  status = session->Create(graph_def.graph_def());
  if (!status.ok()) {
    throw runtime_error("Error creating graph: " + status.ToString());
  }

  // Read weights from the saved checkpoint
  Tensor checkpoint_path_tensor(DT_STRING, TensorShape());
  checkpoint_path_tensor.scalar<std::string>()() = checkpoint_path;
  status = session->Run(
      {{graph_def.saver_def().filename_tensor_name(), checkpoint_path_tensor},},
      {},
      {graph_def.saver_def().restore_op_name()},
      nullptr);
  if (!status.ok()) {
    throw runtime_error("Error loading checkpoint from " + checkpoint_path + ": " + status.ToString());
  }
}

TFNeuralNetwork::~TFNeuralNetwork() {
  this->session->Close();
}
vector<double> TFNeuralNetwork::predict(vector<float> input_vector) {
//    std::cout << input_vector.size() << std::endl;
  vector<Tensor> outputs;
  vector<double> returning_vector;

  // preprocessing of the `input_vector` to the input Tensorflow tensor
  auto mapped_X_ = Eigen::TensorMap<Eigen::Tensor<float, 2, Eigen::RowMajor>>(&input_vector[0], 1, input_vector.size());

  auto eigen_X_ = Eigen::Tensor<float, 2, Eigen::RowMajor>(mapped_X_);

  Tensor input_tensor = Tensor(DT_FLOAT, TensorShape({1, (long long) input_vector.size()}));
  input_tensor.tensor<float, 2>() = eigen_X_;

  tensorflow::Input::Initializer input(input_tensor);

  this->session->Run({{inputLayer, input.tensor}}, {outputLayer}, {}, &outputs);

  TensorShape shape = outputs[0].shape(); // get the first (and only) output tensor
  int shape_dim_2 = shape.dim_size(1); // get the second dimension of the tensor

  auto output_tensor = outputs[0].matrix<float>(); // transform Tensorflow tensor to Eigen tensor

  for (int i = 0; i < shape_dim_2; i++) {
    returning_vector.emplace_back(output_tensor(i));
  }
  return returning_vector;
}
/*
    Copyright 2019 Faculty of Electrical Engineering at CTU in Prague

    This file is part of Game Theoretic Library.

    Game Theoretic Library is free software: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License
    as published by the Free Software Foundation, either version 3
    of the License, or (at your option) any later version.

    Game Theoretic Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Game Theoretic Library.

    If not, see <http://www.gnu.org/licenses/>.
*/

#include "algorithms/cfr_br.h"

using namespace std;
using namespace GTLib2::algorithms;

void CFRBR::InfoSetUpdateRegretsPlus(DLCFRData &cache_, Player updatingPlayer) {

  if (cfrsettings_.cfrUpdating == InfosetsUpdating) {

    for (auto&[aoh, data]:cache_.infosetData) {

      if (aoh->getPlayer() == updatingPlayer) {

        if (cfrsettings_.accumulatorWeighting == LinearAccWeighting && !data.fixAvgStrategy) {
          ++data.numUpdates;
        }

        if (!data.fixRMStrategy) {

          for (int i = 0; i < data.regrets.size(); ++i) {

            cfrsettings_.regretMatching == RegretMatchingPlus ?

                data.regrets[i] += max(data.regretUpdates[i], 0.0) :
                data.regrets[i] += data.regretUpdates[i];

            data.regretUpdates[i] = 0.0;

          }
        }
      }
    }
  }
}

double CFRBR::runCFRBRIteration(DLCFRData &cache_, const shared_ptr<EFGNode> &node,
                                const array<double, 3> reachProbs,
                                const Player updatingPl, const Player FixedPlayer) {
  assert(cache_.isCompletelyBuilt());

  if (reachProbs[0] == 0 && reachProbs[1] == 0) {
    return 0.0;
  }

  if (node->type_ == TerminalNode) {
    return node->getUtilities()[updatingPl];
  }
  if (node->type_ == ChanceNode) {
    const auto &children = cache_.getChildrenFor(node);
    double ExpectedValue = 0.0;
    auto chanceProbs = node->chanceProbs();

    for (int i = 0; i != children.size(); i++) {
      array<double, 3> newReachProbs = {reachProbs[0],
                                        reachProbs[1],
                                        reachProbs[CHANCE_PLAYER] * chanceProbs[i]};
      ExpectedValue +=
          chanceProbs[i] * runCFRBRIteration(cache_, children[i], newReachProbs, updatingPl,
                                             FixedPlayer);
    }
    return ExpectedValue;
  }

  const auto actingPl = node->getPlayer();
  const auto oppExploringPl = 1 - updatingPl;
  const auto &children = cache_.getChildrenFor(node);
  const auto &infoSet = cache_.getInfosetFor(node);
  const auto numActions = children.size();
  auto &infosetData = cache_.infosetData.at(infoSet);
  auto &reg = infosetData.regrets;
  auto &acc = infosetData.avgStratAccumulator;
  auto &regUpdates = infosetData.regretUpdates;
  auto rmProbs = calcRMProbs(reg);

  if (actingPl == FixedPlayer) {

    if (cache_.AOHPosition(infoSet, current_trunk_, AboveTrunkBorder)) {

      assert(infosetData.fixRMStrategy);
      assert(infosetData.fixAvgStrategy);

      rmProbs = calcAvgProbs(acc);

    }
  }

  auto ChildrenActionValues = vector<double>(numActions, 0.0);
  double ExpectedValue = 0.0;

  for (int i = 0; i != children.size(); i++) {

    array<double, 3> newReachProbs = {
        reachProbs[0], reachProbs[1], reachProbs[CHANCE_PLAYER]};

    newReachProbs[actingPl] *= rmProbs[i];

    ChildrenActionValues[i] += runCFRBRIteration(cache_, children[i], newReachProbs, updatingPl,
                                                 FixedPlayer);

    ExpectedValue += rmProbs[i] * ChildrenActionValues[i];
  }

  if (actingPl == updatingPl) {
    if (actingPl == FixedPlayer && cache_.AOHPosition(infoSet, current_trunk_,InAndBelowTrunkBorder)) {
      assert(!infosetData.fixRMStrategy && !infosetData.fixAvgStrategy);
    }
    for (int i = 0; i < numActions; i++) {
      if (!infosetData.fixRMStrategy) {
        double cfActionRegret = (ChildrenActionValues[i] - ExpectedValue)
            * reachProbs[oppExploringPl] * reachProbs[CHANCE_PLAYER];

        regUpdates[i] += cfActionRegret;
      }

      if (!infosetData.fixAvgStrategy) {
        if (cfrsettings_.accumulatorWeighting == LinearAccWeighting) {

          acc[i] += infosetData.numUpdates * reachProbs[updatingPl] * rmProbs[i];
        } else {

          acc[i] += reachProbs[updatingPl] * rmProbs[i];

        }

      }
    }
  }

  return ExpectedValue;
}

void CFRBR::runCFRBRIterations(DLCFRData &cache_, int numIterations,
                               const Player FixedPlayer) {
  assert(cache_.isCompletelyBuilt());

  for (int i = 0; i < numIterations; ++i) {

    runCFRBRIteration(cache_, cache_.getRootNode(), array<double, 3>{1., 1., 1.}, Player(0),
                      FixedPlayer);

    InfoSetUpdateRegretsPlus(cache_, Player(0));

    runCFRBRIteration(cache_, cache_.getRootNode(), array<double, 3>{1., 1., 1.}, Player(1),
                      FixedPlayer);

    InfoSetUpdateRegretsPlus(cache_, Player(1));

  }
}

void CFRBR::FixAllBottomInfosets(DLCFRData &cache_) {

  assert(cache_.isCompletelyBuilt());

  long countBottomFixedInfSet = 0;

  cout << "Fixing all seed infosets of all players below level " << cache_.trunk_depths_[current_trunk_] << endl;
  cout << "Domain EFG maxdepth is: " << (domainMaxDepth_ * 2) << endl;

  for (auto&[aoh, data]:cache_.infosetData) {

    if (cache_.AOHPosition(aoh, cache_.trunk_depths_[current_trunk_], InAndBelowTrunkBorder)) {
      ++countBottomFixedInfSet;
      data.fixAvgStrategy = true;
      data.fixRMStrategy = true;
      cout << "Player" << static_cast<int>(aoh->getPlayer()) << "avg strat Bottom aoh: ";
      print_vector(calcAvgProbs(data.avgStratAccumulator));
      cout << "\n";

    } else {

      vector<double> zeroes(data.regrets.size(), 0.0);
      data.regrets = zeroes;
      data.avgStratAccumulator = zeroes;
      data.regretUpdates = zeroes;
      data.fixAvgStrategy = false;
      data.fixRMStrategy = false;
      data.numUpdates = 1;
    }
  }

  cout << countBottomFixedInfSet << " seed infosets of both players fixed." << endl;

}

void CFRBR::FixTrunkInfosetsFor(Player FixedPlayer,
                                DLCFRData &cache_) {

  assert(cache_.isCompletelyBuilt());

  long countTrunkFixedInfSet = 0;

  cout << "Fixing all infosets of player " << static_cast<int>(FixedPlayer) << " above level " << cache_.trunk_depths_[current_trunk_]
       << endl;
  cout << "Domain EFG maxdepth is: " << (domainMaxDepth_ * 2) << endl;

  if (cache_.trunk_depths_[current_trunk_] < (domainMaxDepth_ * 2)) {

    cout << "DepthLimited CFRBR" << endl;

    for (auto&[aoh, data]:cache_.infosetData) {

      if (cache_.AOHPosition(aoh, current_trunk_, AboveTrunkBorder) && aoh->getPlayer() == FixedPlayer) {
        ++countTrunkFixedInfSet;
        data.fixAvgStrategy = true;
        data.fixRMStrategy = true;
        cout << "Player" << static_cast<int>(FixedPlayer) << "avg strat aoh: ";
        print_vector(calcAvgProbs(data.avgStratAccumulator));
        cout << "\n";
      } else {

        vector<double> zeroes(data.regrets.size(), 0.0);
        data.regrets = zeroes;
        data.avgStratAccumulator = zeroes;
        data.regretUpdates = zeroes;
        data.fixAvgStrategy = false;
        data.fixRMStrategy = false;
        data.numUpdates = 1;
      }
    };
  } else if (cache_.trunk_depths_[current_trunk_] >= (domainMaxDepth_ * 2)) {

    cout << "CFRBR on whole tree" << endl;

    for (auto&[aoh, data]:cache_.infosetData) {

      if (aoh->getPlayer() == FixedPlayer) {

        data.fixAvgStrategy = true;
        data.fixRMStrategy = true;
      } else {
        vector<double> zeroes(data.regrets.size(), 0.0);
        data.regrets = zeroes;
        data.avgStratAccumulator = zeroes;
        data.regretUpdates = zeroes;
        data.fixAvgStrategy = false;
        data.fixRMStrategy = false;
        data.numUpdates = 1;
      }
    };
  }
  cout << countTrunkFixedInfSet << " infosets of Player" << static_cast<int>(FixedPlayer) << " fixed." << endl;
}

double CFRBR::calcBRValue(int numIterations, Player bestResponder, const DLCFRData cache_) {

  auto cache = cache_;
  auto FixedPlayer = bestResponder == Player(0) ? Player(1) : Player(0);

  cout << "BestResponder is Player" << static_cast<int>(bestResponder) << endl;
  cout << "Fixing Player" << static_cast<int>(FixedPlayer) << " above EFGDepth " << cache_.trunk_depths_[current_trunk_] << endl;
  FixTrunkInfosetsFor(FixedPlayer, cache);
  runCFRBRIterations(cache, numIterations, FixedPlayer);
  const auto &rootnode = cache.getRootNode();
  double BRSum = 0;

  //TODO br value always from p0 perspective
  auto BRValue = algorithms::calcExpectedUtility(cache, rootnode, bestResponder);
  BRSum += BRValue.avgUtility;

  return BRSum;

}

double CFRBR::calcExploitability(int numIterations, const DLCFRData cache_) {
  vector<double> bestresponses(2, 0.0);
  bestresponses[0] = calcBRValue(numIterations, Player(1), cache_);
  cout << "CFRBRV against Player" << static_cast<int>(Player(0)) << " : " << bestresponses[0] << endl;

  bestresponses[1] = calcBRValue(numIterations, Player(0), cache_);
  cout << "CFRBRV against Player" << static_cast<int>(Player(1)) << " : " << bestresponses[1] << endl;

  double expl = 0;

  if (domain_type_ == typeid(const domains::GoofSpielDomain).name() ||
      domain_type_ == typeid(const domains::OshiZumoDomain).name() ||
      domain_type_ == typeid(const domains::KriegspielDomain).name()) {

    expl = (fabs(bestresponses[1]) + fabs(bestresponses[0])) / 2;

  } else if (domain_type_ == typeid(const domains::GenericPokerDomain).name()) {
    expl = (bestresponses[1] + bestresponses[0]) / 2;
  }

  cout << "CFRBR Expl after " << numIterations << " iterations: " << expl << endl;

  return expl;
}

void CFRBR::AssignTrunkUniformStrategy(DLCFRData &cache_) {
  if (!cache_.isCompletelyBuilt()) { cache_.buildTree(); }

  for (auto&[aoh, data]:cache_.infosetData) {

    if (cache_.AOHPosition(aoh, AboveTrunkBorder)) {
      auto &reg = data.regrets;
      auto &acc = data.avgStratAccumulator;
      auto &regUpdates = data.regretUpdates;
      auto &numUpdates = data.numUpdates;
      const auto numActions = static_cast<unsigned int>(acc.size());
      const vector<double> zeroes(numActions, 0.0);
      const vector<double> uniformreg(numActions, 1.0 / numActions);
      reg = uniformreg;
      acc = zeroes;
      regUpdates = zeroes;
      data.fixAvgStrategy = true;
      data.fixRMStrategy = true;
      numUpdates = 1;
    } else if (cache_.AOHPosition(aoh, InAndBelowTrunkBorder)) {

      auto &reg = data.regrets;
      auto &acc = data.avgStratAccumulator;
      auto &regUpdates = data.regretUpdates;
      auto &numUpdates = data.numUpdates;
      const auto numActions = static_cast<unsigned int>(reg.size());
      const vector<double> zeroes(numActions, 0.0);
      const vector<double> uniformreg(numActions, 1.0 / numActions);
      reg = uniformreg;
      acc = zeroes;
      regUpdates = zeroes;
      data.fixAvgStrategy = false;
      data.fixRMStrategy = false;
      numUpdates = 1;

    }
  }
};

//double CFRBR::calcExploitabilityUnifStrat(const Domain &domain_) {
//  DLCFRData cache_(domain_, {trunk_depth_});
//  AssignTrunkUniformStrategy(cache_);
//  return calcExploitability(1000, cache_);
//};

//vector<double>
//CFRBR::computeBRVofAllPubStates(const Domain &domain_, int numIterations, const int &trunk_depth_, Player bestResponder,
//                                CFRSettings &cfrsettings_, const DLCFRData cache_) {
//
//    auto cache = cache_;
//    auto FixedPlayer = Player(0);
//    vector<double> BRPubStateValues;
//    bestResponder == Player(0) ? FixedPlayer = Player(1) : FixedPlayer = Player(0);
//    cout << "BestResponder is Player" << static_cast<int>(bestResponder) << endl;
//    cout << "Fixing Player" << static_cast<int>(FixedPlayer) << " above EFGDepth " << trunk_depth_ << endl;
//    FixTrunkInfosetsFor(FixedPlayer, trunk_depth_, domain_, cache);
//    runCFRBRIterations(cache, cfrsettings_, numIterations, FixedPlayer, trunk_depth_, false);
////    const auto &rootnodes = cache.getRootNodes();
//    double BRSum = 0;
////    cout << "domain has " << rootnodes.size() << " rootnodes." << endl;
//    for (const auto &pubstate:cache.public_state_index_) {
//        BRPubStateValues.emplace_back(calcValueofPublicState(cache, bestResponder, pubstate, trunk_depth_));
//    }
//    return BRPubStateValues;
//
//}

//vector<double>
//CFRBR::getWeightedExpValueofNode(DLCFRData &cache, const unordered_set<shared_ptr<EFGNode>> &nodes,
//                                 int trunk_depth_, const Player player) {
//    vector<double> totalreachXexpV;
//    for (const auto &node:nodes) {
//
//        assert((domain_type_ == typeid(domains::GenericPokerDomain).name() &&
//                cache.InfoSetRound.at(cache.getInfosetFor(node)) == trunk_depth_) ||
//               (domain_type_ != typeid(domains::GenericPokerDomain).name() && node->getEFGDepth() == trunk_depth_));
//
//        auto ev = algorithms::calcExpectedUtility(cache, node, player);
//
//        auto natureprob = node->natureProbability_;
//
//        auto avgStrat = getAverageStrategy(cache, domainMaxDepth_);
//
//        auto reach0 = node->getProbabilityOfActionsSeqOfPlayer(Player(0), avgStrat[0]);
//        auto reach1 = node->getProbabilityOfActionsSeqOfPlayer(Player(1), avgStrat[1]);
//
//        double totalreach = natureprob * reach0 * reach1;
//
//        totalreachXexpV.push_back(totalreach * ev.avgUtility);
//
//    }
//    return totalreachXexpV;
//}

//double CFRBR::calcValueofPublicState(DLCFRData &cache, Player BestResponder,
//                                     const shared_ptr<PublicState> &pubState, int trunk_depth_) {
//    const auto &nodesPubstate = cache.getNodesForPubState(pubState);
//    vector<double> weightedexp = getWeightedExpValueofNode(cache, nodesPubstate, trunk_depth_, BestResponder);
//    double sumPubState = 0;
//    for (const auto &value:weightedexp) {
//        sumPubState += value;
//    }
//    return sumPubState;
//
//}





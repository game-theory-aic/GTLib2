/*
    Copyright 2019 Faculty of Electrical Engineering at CTU in Prague

    This file is part of Game Theoretic Library.

    Game Theoretic Library is free software: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License
    as published by the Free Software Foundation, either version 3
    of the License, or (at your option) any later version.

    Game Theoretic Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Game Theoretic Library.

    If not, see <http://www.gnu.org/licenses/>.
*/

#include "algorithms/cfr_dl.h"

using namespace GTLib2;
using namespace algorithms;

namespace GTLib2::algorithms {

const vector<vector<double>> CFR_DL::getRMTrunkRangesAllP0P1() {

  vector<double> rangesP0;

  vector<double> rangesP1;

  vector<vector<double>> ranges;

  const auto &rmStrat = getRMStrategy(cache_);

  for (const auto &node : cache_.node_index_[current_trunk_]) {
    double range0 = node->getProbabilityOfActionSeq(Player(0), rmStrat.at(0));
    double range1 = node->getProbabilityOfActionSeq(Player(1), rmStrat.at(1));
    rangesP0.push_back(range0);
    rangesP1.push_back(range1);
  }
  ranges.emplace_back(rangesP0);
  ranges.emplace_back(rangesP1);

  return ranges;

};

void CFR_DL::load_nn() {
  auto n = cache_.trunk_depths_.size();
  for (int i = 0; i < n; ++i) {
    assert(network_paths_[i].size() >= 4);
    tfnn_[i] = new TFNeuralNetwork(network_paths_[i][0], network_paths_[i][1], network_paths_[i][2], network_paths_[i][3]);
    cout << "Network #" << i + 1 << " of " << n << " loaded successfully" << endl;
  }
  isLoadedNN_ = true;
}

void CFR_DL::createMappingData() {

  if (!cache_.isCompletelyBuilt()) { cache_.buildTree(); }

  assert(cache_.isCompletelyBuilt());

  num_infosets_at_trunk_depth_ = cache_.infoset_indexes_[current_trunk_].size();
  num_auginfosets_at_trunk_depth_ = cache_.aug_infoset_indexes_[current_trunk_].size();
  num_publicstates_at_trunk_depth_ = cache_.public_state_index_[current_trunk_].size();
  num_possible_actions_sequences_p0_ = cache_.possible_actions_sequences_p0_[current_trunk_].size();
  num_possible_actions_sequences_p1_ = cache_.possible_actions_sequences_p1_[current_trunk_].size();
  generate_public_features();
  generate_maps();
  transform_ = TransformCFRDataToNN(information_set_mapping_,
                                    augmented_information_set_mapping_,
                                    public_features_, num_possible_actions_sequences_p0_,
                                    num_possible_actions_sequences_p1_);

  cout << "Domain: " << domain_type_ << "|Trunkdepth:" << cache_.trunk_depths_[current_trunk_] << "|NumNodes:"
       << cache_.node_index_.at(current_trunk_).size()
       << "|NumInfosets:" << num_infosets_at_trunk_depth_
       << "|NumAugInfosets:" << num_auginfosets_at_trunk_depth_ << "|NumPubStates:"
       << num_publicstates_at_trunk_depth_ << "|NumActionSeq P0:" << num_possible_actions_sequences_p0_ <<
       "|NumActionSeq P1:" << num_possible_actions_sequences_p1_ << endl;
  isMappingBuilt_ = true;

}

void CFR_DL::UpdateInfosetRegrets(Player updatingPl) {

  if (cfrsettings_.cfrUpdating == InfosetsUpdating) {
    for (auto&[aoh, data]:cache_.infosetData) {

      if (aoh->getPlayer() == updatingPl) {

        if (cfrsettings_.accumulatorWeighting == LinearAccWeighting && !data.fixAvgStrategy) {

          ++data.numUpdates;

        }

        if (!data.fixRMStrategy) {

          for (int i = 0; i < data.regrets.size(); ++i) {

            (cfrsettings_.regretMatching == RegretMatchingNormal) ? data.regrets[i] +=
                                                                        data.regretUpdates[i]
                                                                  :
                data.regrets[i] += max(data.regretUpdates[i], 0.0);

            data.regretUpdates[i] = 0.0;

          }
        }
      }
    }
  }
}

void CFR_DL::runCFRDIteration(int numIterations, Domain &domain, Player updatingPlayer) {

  auto cachep0 = cache_;

  FixResetTrunkStrats(cachep0);

  auto *CFRFixedTrunk = new CFRAlgorithm(domain, Player(0), cachep0, cfrsettings_);

  CFRFixedTrunk->runIterations(numIterations);

  predicted_values_ = CFRDcollectExPV(cachep0, updatingPlayer);

  runDepthLimitedCFRIteration(cache_.getRootNode(), array<double, 3>{1., 1., 1.}, updatingPlayer);

  delete CFRFixedTrunk;
}

void CFR_DL::runCFRD(int numIterations, Domain &domain) {

  if (!cache_.isCompletelyBuilt()) {
    cache_.buildTree();
  }
  if (!isMappingBuilt_) {
    createMappingData();
  }

  UnfixTrunkFixBottom();

  assert(UnfixedTrunkFixedBottom_);
  assert(!isLoadedNN_);
  assert(cache_.isCompletelyBuilt());
  assert(isMappingBuilt_);

  isCFV_ = false;

  for (int i = 0; i < numIterations; ++i) {

    runCFRDIteration(numIterations, domain, Player(0));

    UpdateInfosetRegrets(Player(0));

    runCFRDIteration(numIterations, domain, Player(1));

    UpdateInfosetRegrets(Player(1));
  }
};

void CFR_DL::runCFRDSumInfosetsIteration(int numIterations, Domain &domain, Player updatingPlayer) {

  auto cachep0 = cache_;

  FixResetTrunkStrats(cachep0);

  auto *CFRFixedTrunk = new CFRAlgorithm(domain, Player(0), cachep0, cfrsettings_);

  CFRFixedTrunk->runIterations(numIterations);

  predicted_values_ = CFRDcollectExPV(cachep0, updatingPlayer);

  predicted_values_ = computeCFVInfosets(updatingPlayer);

  runDepthLimitedCFRIteration(cache_.getRootNode(), array<double, 3>{1., 1., 1.}, updatingPlayer);

  delete CFRFixedTrunk;
}

void CFR_DL::runCFRDSumInfosets(int numIterations, Domain &domain) {

  if (!cache_.isCompletelyBuilt()) {
    cache_.buildTree();
  }
  if (!isMappingBuilt_) {
    createMappingData();
  }

  UnfixTrunkFixBottom();

  assert(UnfixedTrunkFixedBottom_);
  assert(!isLoadedNN_);
  assert(cache_.isCompletelyBuilt());
  assert(isMappingBuilt_);

  isCFV_ = true;

  for (int i = 0; i < numIterations; ++i) {

    runCFRDSumInfosetsIteration(numIterations, domain, Player(0));

    UpdateInfosetRegrets(Player(0));

    runCFRDSumInfosetsIteration(numIterations, domain, Player(1));

    UpdateInfosetRegrets(Player(1));

  }
};

void CFR_DL::runNoisyCFRDSumInfosetsIteration(int numIterations, Domain &domain, Player updatingPlayer,
                                              double noise_prop_total, double noise_prop_sign,
                                              double noise_mean, double noise_std) {

  auto cachep0 = cache_;

  FixResetTrunkStrats(cachep0);

  auto *CFRFixedTrunk = new CFRAlgorithm(domain, Player(0), cachep0, cfrsettings_);

  CFRFixedTrunk->runIterations(numIterations);

  predicted_values_ = CFRDcollectExPV(cachep0, updatingPlayer);

  predicted_values_ = computeNoisyCFVInfosets(updatingPlayer, noise_prop_total, noise_prop_sign,
                                              noise_mean, noise_std);

  runDepthLimitedCFRIteration(cache_.getRootNode(), array<double, 3>{1., 1., 1.}, updatingPlayer);

  delete CFRFixedTrunk;
}

void
CFR_DL::runNoisyCFRDSumInfosets(int numIterations, Domain &domain, double noise_prop_total, double noise_prop_sign,
                                double noise_mean, double noise_std) {

  if (!cache_.isCompletelyBuilt()) {
    cache_.buildTree();
  }
  if (!isMappingBuilt_) {
    createMappingData();
  }

  UnfixTrunkFixBottom();

  assert(UnfixedTrunkFixedBottom_);
  assert(!isLoadedNN_);
  assert(cache_.isCompletelyBuilt());
  assert(isMappingBuilt_);

  isCFV_ = true;

  for (int i = 0; i < numIterations; ++i) {

    runNoisyCFRDSumInfosetsIteration(numIterations, domain, Player(0), noise_prop_total, noise_prop_sign,
                                     noise_mean, noise_std);

    UpdateInfosetRegrets(Player(0));

    runNoisyCFRDSumInfosetsIteration(numIterations, domain, Player(1), noise_prop_total, noise_prop_sign,
                                     noise_mean, noise_std);

    UpdateInfosetRegrets(Player(1));

  }
  cout << "Gaussian Noise CFRD with:" << noise_prop_total << "% prop of noise|Noise Sign Prop:" << noise_prop_sign
       << "|Noise mean:" << noise_mean <<
       "|Noise std:" << noise_std << endl;
};

void CFR_DL::runSystematicNoiseCFRDSumInfosets(int numIterations, Domain &domain) {

  if (!cache_.isCompletelyBuilt()) {
    cache_.buildTree();
  }
  if (!isMappingBuilt_) {
    createMappingData();
  }

  UnfixTrunkFixBottom();

  assert(UnfixedTrunkFixedBottom_);
  assert(!isLoadedNN_);
  assert(cache_.isCompletelyBuilt());
  assert(isMappingBuilt_);

  isCFV_ = true;

  default_random_engine generator(numIterations);
  bernoulli_distribution addnoise(0.7);
  bernoulli_distribution neg_or_pos(0.5);
//        uniform_real_distribution<double> UniformDraw(0, 10);
  normal_distribution<double> gaussian_noise(0.1, 0.01);

  vector<double> noise_vector(cache_.node_index_.size(), 0.0);

  for (auto &pred:noise_vector) {
    if (addnoise(generator)) {
      if (neg_or_pos(generator)) {
        pred += gaussian_noise(generator);
      } else {
        pred -= gaussian_noise(generator);
      }
    }
  }

  for (int i = 0; i < numIterations; ++i) {

    runSystematicNoiseCFRDSumInfosetsIteration(numIterations, domain, Player(0));

    for (int j = 0; j < predicted_values_.size(); ++j) {
      predicted_values_[j] += noise_vector[j];
    }

    UpdateInfosetRegrets(Player(0));

    runSystematicNoiseCFRDSumInfosetsIteration(numIterations, domain, Player(1));

    for (int j = 0; j < predicted_values_.size(); ++j) {
      predicted_values_[j] += noise_vector[j];
    }
    UpdateInfosetRegrets(Player(1));

  }
};

void CFR_DL::runSystematicNoiseCFRDSumInfosetsIteration(int numIterations, Domain &domain, Player updatingPlayer) {

  auto cachep0 = cache_;

  FixResetTrunkStrats(cachep0);

  auto *CFRFixedTrunk = new CFRAlgorithm(domain, Player(0), cachep0, cfrsettings_);

  CFRFixedTrunk->runIterations(numIterations);

  predicted_values_ = CFRDcollectExPV(cachep0, updatingPlayer);

  predicted_values_ = computeCFVInfosets(updatingPlayer);

  runDepthLimitedCFRIteration(cache_.getRootNode(), array<double, 3>{1., 1., 1.}, updatingPlayer);

  delete CFRFixedTrunk;
}

void CFR_DL::runCFRDNN(int numIterations, Domain &domain, const string &path, bool verbose) {

  if (!cache_.isCompletelyBuilt()) {
    cache_.buildTree();
  }
  if (!isMappingBuilt_) {
    createMappingData();
  }
  if (!isLoadedNN_) {
    load_nn();
  }

  UnfixTrunkFixBottom();

  assert(UnfixedTrunkFixedBottom_);
  assert(isLoadedNN_);
  assert(cache_.isCompletelyBuilt());
  assert(isMappingBuilt_);

  isCFV_ = false;

  for (int i = 0; i < numIterations; ++i) {

    const auto &ranges = getRMTrunkRangesAllP0P1();
    const vector<vector<double>> nn_input = cfr_to_nn_input(ranges[0], ranges[1]);
    vector<vector<double>> pred_matrix;

    for (const vector<double> &row:nn_input) {
      vector<float> float_row(row.begin(), row.end());
      const auto public_state_pred = tfnn_[current_trunk_]->predict(float_row);
      pred_matrix.emplace_back(public_state_pred);
    }

    nn_to_cfr_input(ranges[0], ranges[1], pred_matrix, Player(0));

    debugvector_ = predicted_values_;

    predicted_values_ = vector<double>();

    runCFRDIteration(numIterations, domain, Player(0));

    auto sumcfvcfrd = computeCFVInfosets(Player(0));
    assert(sumcfvcfrd.size() == debugvector_.size());
    double huberp0 = HuberCFRDNN(sumcfvcfrd, debugvector_);
    double linfp0 = linfCFRDNN(sumcfvcfrd, debugvector_, Player(0));
    const auto zeroratiorangesp0p1 = ZeroRatioRanges(ranges[0], ranges[1]);
    if (verbose) {
      cout << "-------Iteration-" + to_string(i) << "---------" << endl;
      cout << "P0 update" << endl;
      cout << "ranges p1" << endl;
      print_vector(ranges[1]);
      cout << " cfrd sum cf values p0:" << endl;
      print_vector(sumcfvcfrd);
      cout << " network sum cf values p0:" << endl;
      print_vector(debugvector_);
      cout << "---------------" << endl;
      cout << "huber between cfrd and nn p0: " << huberp0 << endl;
      cout << "---------------" << endl;
      cout << "linf between cfrd and nn p0: " << linfp0 << endl;
      cout << "---------------" << endl;
      cout << "% of 0 in ranges p0 " << zeroratiorangesp0p1.first << endl;
      cout << "% of 0 in ranges p1 " << zeroratiorangesp0p1.second << endl;
    }

    debugvector_ = vector<double>();
    nodevector_ = vector<shared_ptr<EFGNode>>();

    UpdateInfosetRegrets(Player(0));

    const auto &ranges1 = getRMTrunkRangesAllP0P1();

    const vector<vector<double>> nn_input1 = cfr_to_nn_input(ranges1[0], ranges1[1]);

    vector<vector<double>> pred_matrix1;

    for (const vector<double> &row:nn_input1) {
      vector<float> float_row(row.begin(), row.end());
      const auto public_state_pred = tfnn_[current_trunk_]->predict(float_row);
      pred_matrix1.emplace_back(public_state_pred);
    }

    nn_to_cfr_input(ranges1[0], ranges1[1], pred_matrix1, Player(1));

    debugvector_ = predicted_values_;

    predicted_values_ = vector<double>();

    runCFRDIteration(numIterations, domain, Player(1));

    auto sumcfvcfrd1 = computeCFVInfosets(Player(1));
    double huberp1 = HuberCFRDNN(sumcfvcfrd1, debugvector_);
    double linfp1 = linfCFRDNN(sumcfvcfrd1, debugvector_, Player(1));
    const auto zeroratiorangesp0p1_p1update = ZeroRatioRanges(ranges1[0], ranges1[1]);
    if (verbose) {

      cout << "P1 update" << endl;
      cout << "ranges p0" << endl;
      print_vector(ranges1[0]);
      cout << " cfrd sum cf values p1 aug:" << endl;
      print_vector(sumcfvcfrd1);
      cout << " network sum cf values p1 aug:" << endl;
      print_vector(debugvector_);
      cout << "---------------" << endl;
      cout << "huber between cfrd and nn: " << huberp1 << endl;
      cout << "---------------" << endl;
      cout << "linf between cfrd and nn: " << linfp1 << endl;
      cout << "% of 0 in ranges p0 " << zeroratiorangesp0p1_p1update.first << endl;
      cout << "% of 0 in ranges p1 " << zeroratiorangesp0p1_p1update.second << endl;
      cout << "---------------" << endl;

    }

    debugvector_ = vector<double>();
    nodevector_ = vector<shared_ptr<EFGNode>>();

    UpdateInfosetRegrets(Player(1));

    vector<double> data_iteration0 = {fabs(zeroratiorangesp0p1.first - zeroratiorangesp0p1.second),
                                      huberp0, linfp0};

    vector<double> data_iteration1 = {
        fabs(zeroratiorangesp0p1_p1update.first - zeroratiorangesp0p1_p1update.second),
        huberp1, linfp1};

    sparseness_error_.emplace_back(data_iteration0);

    sparseness_error_.emplace_back(data_iteration1);

  }

  ostringstream oss;

  oss << path << "/sparsenesserror_" << domain_type_ << "_" << cache_.trunk_depths_[current_trunk_] << ".csv";

  string filename = oss.str();

  cout << "writing " << sparseness_error_.size() << " rows " << cache_.trunk_depths_[current_trunk_] << " to file: "
       << filename << endl;

  ofstream outFile_(filename);

  for (const auto &iteration:sparseness_error_) {

    outFile_ << iteration << endl;

  }

};

void CFR_DL::runDepthLimitedCFRIterations(int numIterations) {
  if (!cache_.isCompletelyBuilt()) {
    cache_.buildTree();
  }
  if (!isMappingBuilt_) {
    createMappingData();
  }
  if (!isLoadedNN_) {
    load_nn();
  }
  if (!UnfixedTrunkFixedBottom_) {
    UnfixTrunkFixBottom();
  }

  assert(UnfixedTrunkFixedBottom_);
  assert(isLoadedNN_);
  assert(cache_.isCompletelyBuilt());
  assert(isMappingBuilt_);

  if (isCFV_) {

    cout << "Running CFV network" << endl;
  } else {
    cout << "Running ExpV network" << endl;
  }

  cout << numIterations << " iterations" << endl;

  for (int i = 0; i < numIterations; ++i) {

    runCFRNNIteration(Player(0));

    UpdateInfosetRegrets(Player(0));

    runCFRNNIteration(Player(1));

    UpdateInfosetRegrets(Player(1));

  }
}

void CFR_DL::runCFRNNIteration(Player updatingPlayer) {

  const auto &ranges = getRMTrunkRangesAllP0P1();

  const vector<vector<double>> nn_input = cfr_to_nn_input(ranges[0], ranges[1]);

  vector<vector<double>> pred_matrix;

  for (const vector<double> &row:nn_input) {
    vector<float> float_row(row.begin(), row.end());
    const auto public_state_pred = tfnn_[current_trunk_]->predict(float_row);
    pred_matrix.emplace_back(public_state_pred);
  }

  nn_to_cfr_input(ranges[0], ranges[1], pred_matrix, updatingPlayer);

  runDepthLimitedCFRIteration(cache_.getRootNode(), array<double, 3>{1., 1., 1.}, updatingPlayer);

}

const double CFR_DL::getNodePredAtIndex(const shared_ptr<EFGNode> &node) const {

  return predicted_values_.at(cache_.getNodeIndexFor(node, current_trunk_));
}

void CFR_DL::printTrunkRegrets() const {

  for (const auto&[aoh, data]:cache_.infosetData) {
    if (cache_.AOHPosition(aoh, current_trunk_,AboveTrunkBorder)) {
      assert(!data.fixAvgStrategy && !data.fixRMStrategy);
      const auto reg = data.regrets;
      cout << "Player" << static_cast<int>(aoh->getPlayer()) << " | Depth" << cache_.InfoSetEFGDepth_.at(aoh)
           << " |Regrets:";
      print_vector(reg);
    }
  }
}

void CFR_DL::printCumulativeRegrets(Player player) const {

  for (const auto&[aoh, data]:cache_.infosetData) {
    if (cache_.InfoSetEFGDepth_.at(aoh) < cache_.trunk_depths_[current_trunk_] && !data.fixAvgStrategy && !data.fixRMStrategy &&
        aoh->getPlayer() == player) {

      cout << "Player" << static_cast<int>(aoh->getPlayer()) << " | Depth" << cache_.InfoSetEFGDepth_.at(aoh)
           << " |Cumulative Regrets:";
      print_vector(data.regrets);
    }
  }

}

void CFR_DL::printBottomAVGStrats(const DLCFRData &cache, const int trunk_depth) const {
  int InfoSetsVisited = 0;
  for (const auto&[aoh, data]:cache.infosetData) {
    if (cache_.InfoSetEFGDepth_.at(aoh) >= cache_.trunk_depths_[current_trunk_] && !data.fixAvgStrategy && !data.fixRMStrategy &&
        data.regrets.size() > 1) {
      ++InfoSetsVisited;
      const auto rmProbs = calcAvgProbs(data.avgStratAccumulator);
      cout << "Player" << static_cast<int>(aoh->getPlayer()) << " | Depth" << cache_.InfoSetEFGDepth_.at(aoh)
           << " |AVGStrat:";
      print_vector(rmProbs);
    }
  }

  cout << InfoSetsVisited << " unfixed bottom infosets updated." << endl;
}

void CFR_DL::UnfixTrunkFixBottom() {

  assert(isMappingBuilt_);
  assert(cache_.isCompletelyBuilt());

  long numInfSetFixed = 0;
  long numInfSetUnFixed = 0;
  cout << "InfoSetEFGDepth has " << cache_.InfoSetEFGDepth_.size() << " infosets" << endl;
  for (auto&[aoh, data]:cache_.infosetData) {

    if (cache_.AOHPosition(aoh, current_trunk_,AboveTrunkBorder)) {
      ++numInfSetUnFixed;
      data.fixAvgStrategy = false;
      data.fixRMStrategy = false;

    } else if (cache_.AOHPosition(aoh, current_trunk_, InAndBelowTrunkBorder)) {
      ++numInfSetFixed;
      const vector<double> zeroes(data.regrets.size(), 0.0);
      data.regrets = zeroes;
      data.avgStratAccumulator = zeroes;
      data.regretUpdates = zeroes;
      data.fixAvgStrategy = true;
      data.fixRMStrategy = true;
      data.numUpdates = 1;
    }
  };
  cout << "Unfixed " << numInfSetUnFixed << " Trunk Infosets " << endl;
  cout << "Fixed " << numInfSetFixed << " Bottom Infosets " << endl;
  UnfixedTrunkFixedBottom_ = true;

};

double CFR_DL::runDepthLimitedCFRIteration(const shared_ptr<EFGNode> &node,
                                           const array<double, 3> reachProbs,
                                           const Player updatingPl) {
  assert(cache_.isCompletelyBuilt());
  assert(isMappingBuilt_);
  assert(UnfixedTrunkFixedBottom_);
  if (node->type_ == TerminalNode) {

    return node->getUtilities()[updatingPl];
  }
  if (node->type_ == ChanceNode) {
    const auto &children = cache_.getChildrenFor(node);
    double ExpectedValue = 0.0;
    auto chanceProbs = node->chanceProbs();

    for (int i = 0; i != children.size(); i++) {
      array<double, 3> newReachProbs = {reachProbs[0],
                                        reachProbs[1],
                                        reachProbs[CHANCE_PLAYER] * chanceProbs[i]};
      ExpectedValue += chanceProbs[i] * runDepthLimitedCFRIteration(children[i], newReachProbs, updatingPl);
    }
    return ExpectedValue;
  }

  if (reachProbs[0] == 0 && reachProbs[1] == 0.0) {
    return 0.0;
  }

  const auto actingPl = node->getPlayer();
  const auto Opponent = 1 - updatingPl;
  const auto &infoSet = cache_.getInfosetFor(node);

  if (cache_.NodePosition(node, current_trunk_, GameTreePosition::InTrunkBorder)) {

    if (isCFV_) {

      const auto &cfvInfosetPred = getNodePredAtIndex(node);

      if (cfvInfosetPred == 0.0) {
        return 0.0;
      } else {

        if (fabs(cfvInfosetPred) > domainMaxUtility_) {

          if (cfvInfosetPred > 0) {
            return domainMaxUtility_;
          } else {
            return -domainMaxUtility_;
          }

        } else {

          return (cfvInfosetPred / (reachProbs[Opponent] * reachProbs[CHANCE_PLAYER]));

        }

      }
    } else {

      return getNodePredAtIndex(node);
    }
  }

  const auto &children = cache_.getChildrenFor(node);
  const auto numActions = children.size();
  auto &infosetData = cache_.infosetData.at(infoSet);
  auto &reg = infosetData.regrets;
  auto &acc = infosetData.avgStratAccumulator;
  auto &regUpdates = infosetData.regretUpdates;
  auto rmProbs = calcRMProbs(reg);
  auto ChildrenActionValues = vector<double>(numActions, 0.0);
  double ExpectedValue = 0.0;

  for (int i = 0; i != children.size(); i++) {

    array<double, 3> newReachProbs = {reachProbs[0],
                                      reachProbs[1],
                                      reachProbs[CHANCE_PLAYER]};
    newReachProbs[actingPl] *= rmProbs[i];

    ChildrenActionValues[i] += runDepthLimitedCFRIteration(children[i], newReachProbs, updatingPl);
    ExpectedValue += rmProbs[i] * ChildrenActionValues[i];

  }

  assert(cache_.NodePosition(node, current_trunk_, AboveTrunkBorder));

  if (actingPl == updatingPl) {
    for (int i = 0; i < numActions; i++) {
      if (!infosetData.fixRMStrategy) {

        double cfActionRegret = (ChildrenActionValues[i] - ExpectedValue)
            * reachProbs[Opponent] * reachProbs[CHANCE_PLAYER];

        regUpdates[i] += cfActionRegret;

      }

      if (!infosetData.fixAvgStrategy) {
        if (cfrsettings_.accumulatorWeighting == LinearAccWeighting) {

          acc[i] += infosetData.numUpdates * reachProbs[updatingPl] * rmProbs[i];

        } else {

          acc[i] += reachProbs[updatingPl] * rmProbs[i];

        }
      }
    }
  }

  return ExpectedValue;
}

const vector<double> CFR_DL::CFRDcollectExPV(CFRData &cache, Player player) {
  vector<double> predicted_values;
  auto collect_data = [&](const shared_ptr<EFGNode> node) {
    if (node->type_ == TerminalNode) return;
    if (cache_.NodePosition(node, current_trunk_,GameTreePosition::InTrunkBorder)) {

      auto ev = algorithms::calcExpectedUtility(cache, node, player);

      predicted_values.push_back(ev.avgUtility);
    }
  };

  treeWalk(cache, collect_data, domainMaxDepth_);
  assert(predicted_values.size() == cache_.node_index_.at(current_trunk_).size());
  return predicted_values;
}

vector<double> CFR_DL::getAllRMRangesForNode(const shared_ptr<EFGNode> &node) {

  const auto &rmStrat = getRMStrategy(cache_);

  const auto &fognode = CastEFGtoFOG(node);

  double range_p0 = fognode->getProbabilityOfActionSeq(Player(0), rmStrat.at(0));

  double range_p1 = fognode->getProbabilityOfActionSeq(Player(1), rmStrat.at(1));

  double range_chance = 1; //TODO fix change prob
  vector<double> nodal_ranges = {range_p0, range_p1, range_chance};

  assert(nodal_ranges.size() == 3);

//        const auto &nodetofog = CastEFGtoFOG(node);
//        nodetofog->chanceProbs();

//        double range_chance = nodetofog->natureProbability_; //TODO fix change prob

  return nodal_ranges;

}

vector<long> CFR_DL::getPublicAugInfIndices(const shared_ptr<EFGNode> &node) {
  return cache_.getPublicAugInfIndices(node, current_trunk_);

};

vector<double> CFR_DL::getNodalPublicFeaturesAllPlayers(const shared_ptr<EFGNode> &node) {
  assert(isMappingBuilt_);

  const auto &nodeidx = cache_.getNodeIndexFor(node, current_trunk_);
  const auto &pubstateidx = cache_.getPublicStateIndex(cache_.getPublicStateFor(node), current_trunk_);
  return public_features_[pubstateidx];

  if (domain_type_ == typeid(const domains::KriegspielDomain).name()) {
    const auto &nodeidx = cache_.getNodeIndexFor(node, current_trunk_);
    const auto &pubstateidx = cache_.getPublicStateIndex(cache_.getPublicStateFor(node), current_trunk_);
//            cout << "node idx:" << nodeidx<< " belongs to publicstate idx " << pubstateidx << " and has pubfeatures: ";
//            print_vector(public_features_[pubstateidx]);
    return public_features_[pubstateidx];

  } else if (domain_type_ == typeid(const domains::GoofSpielDomain).name()) {
    return public_features_[cache_.getNodeIndexFor(node, current_trunk_)];
//            const auto pubState = cache_.getPublicStateFor(node);
//            const vector<shared_ptr<Observation>> obsHistory = pubState->getHistory();
//            const vector<shared_ptr<domains::GoofSpielObservation>> ObsHistory = Cast<Observation, domains::GoofSpielObservation>(
//                    obsHistory);
//            vector<double> public_hist_int;
//
//            for (auto &item:ObsHistory) {
//
//                public_hist_int.emplace_back(item->roundResult_);
//            }
//
//            vector<double> nodal_obs_double(public_hist_int.begin(), public_hist_int.end());
//            return nodal_obs_double;
  } else if (domain_type_ == typeid(const domains::OshiZumoDomain).name()) {
//            const auto pubState = cache_.getPublicStateFor(node);
//            const vector<shared_ptr<Observation>> obsHistory = pubState->getHistory();
//            const vector<shared_ptr<domains::OshiZumoObservation>> ObsHistory = Cast<Observation, domains::OshiZumoObservation>(
//                    obsHistory);
//            vector<double> public_hist_int;
//
//            for (auto &item:ObsHistory) {
//
//                public_hist_int.emplace_back(item->roundResult_);
//            }
//
//            vector<double> nodal_obs_double(public_hist_int.begin(), public_hist_int.end());
//            return nodal_obs_double;
  } else if (domain_type_ == typeid(const domains::GenericPokerDomain).name()) {
//
//            const auto pubState = cache_.getPublicStateFor(node);
//
//            const vector<shared_ptr<Observation>> obsHistory = pubState->getHistory();
//
//            vector<int> public_hist_int = domains::EncodePokerObsHistoryFixed(obsHistory, 3, 2, 2,
//                                                                              pair<int, int>(2, 2));
//
//            vector<double> nodal_obs_double(public_hist_int.begin(), public_hist_int.end());
//
//            return nodal_obs_double;
  }

};

void CFR_DL::CollectNodeData() {

  for (const auto &node:cache_.node_index_[current_trunk_]) {

    double ev_avg = getNodePredAtIndex(node);

    vector<long> public_aug_inf_indices = getPublicAugInfIndices(node);

    vector<double> public_aug_inf_indices_double = vector<double>(
        public_aug_inf_indices.begin(), public_aug_inf_indices.end());

    vector<double> nodal_ranges = getAllRMRangesForNode(node);

    vector<double> public_obs = getNodalPublicFeaturesAllPlayers(
        node);

    nodal_ranges.emplace_back(ev_avg);

    public_aug_inf_indices_double.insert(public_aug_inf_indices_double.end(), public_obs.begin(),
                                         public_obs.end());

    public_aug_inf_indices_double.insert(public_aug_inf_indices_double.end(), nodal_ranges.begin(),
                                         nodal_ranges.end());

    nodal_features_.emplace_back(make_pair(node, public_aug_inf_indices_double));

  }

};

void CFR_DL::FixResetTrunkStrats(DLCFRData &cache) {
  assert(cache.isCompletelyBuilt());

  for (auto&[aoh, data]:cache.infosetData) {
    if (cache_.AOHPosition(aoh, current_trunk_,AboveTrunkBorder)) {

      assert(!data.fixRMStrategy);
      assert(!data.fixAvgStrategy);
      data.fixAvgStrategy = true;
      data.fixRMStrategy = true;

    } else if (cache_.AOHPosition(aoh, current_trunk_,InAndBelowTrunkBorder)) {
      assert(data.fixRMStrategy);
      assert(data.fixAvgStrategy);
      const vector<double> zeroes(data.regrets.size(), 0.0);
      data.regrets = zeroes;
      data.avgStratAccumulator = zeroes;
      data.regretUpdates = zeroes;
      data.fixAvgStrategy = false;
      data.fixRMStrategy = false;
      data.numUpdates = 1;
    }
  }

}

void CFR_DL::ResetNodalFeatures() {
  nodal_features_ = vector<pair<shared_ptr<EFGNode>, vector<double>>>();
};

void
CFR_DL::GenerateSeed(int numIterations, string path, GeneratedDataType TypeOfData, Domain &domain, int Iteration) {
  if (!isMappingBuilt_) createMappingData();
  assert(isMappingBuilt_);

  if (TypeOfData == RandomData) {
    //TODO look into bug where fixresetstrategies fails at assert that bottom strats are unfixed
    auto CacheCopy = cache_;

//            FixResetTrunkStrats(CacheCopy);
    auto *CFRFixedTrunk = new CFRAlgorithm(domain, Player(0), CacheCopy, cfrsettings_);

    CFRFixedTrunk->runIterations(numIterations);

    predicted_values_ = CFRDcollectExPV(CacheCopy, Player(0));

  } else if (TypeOfData == CFRDData) {

    runCFRDIteration(numIterations, domain, Player(0));

    UpdateInfosetRegrets(Player(0));

    runCFRDIteration(numIterations, domain, Player(1));

    UpdateInfosetRegrets(Player(1));

    for (auto &pred:predicted_values_) {
      pred *= -1;
    }

  } else if (TypeOfData == CFRNNData) {

    runCFRNNIteration(Player(0));

    UpdateInfosetRegrets(Player(0));

    runCFRNNIteration(Player(1));

    UpdateInfosetRegrets(Player(1));

    for (auto &pred:predicted_values_) {
      pred *= -1;
    }

  }

  CollectNodeData();

  ostringstream oss;

  string DataTypeStr;

  string DomainTypeStr;

  if (domain_type_ == typeid(domains::GenericPokerDomain).name()) {
    DomainTypeStr = "GP";
  } else if (domain_type_ == typeid(domains::GoofSpielDomain).name()) {
    DomainTypeStr = "IIGS";
  } else if (domain_type_ == typeid(domains::OshiZumoDomain).name()) {
    DomainTypeStr = "OZ";
  } else if (domain_type_ == typeid(domains::KriegspielDomain).name()) {
    DomainTypeStr = "KS";
  } else if (domain_type_ == typeid(domains::DarkchessDomain).name()) {
    DomainTypeStr = "DC";
  }

  if (TypeOfData == RandomData) {
    DataTypeStr = "Random";

  } else if (TypeOfData == CFRDData) {
    DataTypeStr = "CFRD";

  } else if (TypeOfData == CFRNNData) {
    DataTypeStr = "CFRNN";

  }

  oss << path << "/" << DomainTypeStr << domain.getMaxStateDepth() - 1 << "_T" << cache_.trunk_depths_.at(current_trunk_) << "_"
      << DataTypeStr << "_" << Iteration << ".csv";

  string filename = oss.str();

  cout << "Writing " << nodal_features_.size() << " nodes in depth " << cache_.trunk_depths_.at(current_trunk_) << " of " << DataTypeStr
       << " data of " << DomainTypeStr
       << " to file: " << filename << endl;

  ofstream outFile_(filename);

  for (const auto &nodal_pairs:nodal_features_) {

    outFile_ << nodal_pairs.second << endl;

  }

};

void CFR_DL::GenerateSeeds(int numSeeds, int numIterations, const string path, GeneratedDataType TypeOfData,
                           Domain &domain) {

  if (!cache_.isCompletelyBuilt()) {
    cache_.buildTree();
  }
  if (!isMappingBuilt_) {
    createMappingData();
  }

  if (TypeOfData == CFRNNData) {
    if (!isLoadedNN_) {
      load_nn();
    }
    assert(isLoadedNN_);

  }

  if (TypeOfData != RandomData) {
    if (!UnfixedTrunkFixedBottom_) {
      UnfixTrunkFixBottom();
    }
    assert(UnfixedTrunkFixedBottom_);

  }

  assert(cache_.isCompletelyBuilt());
  assert(isMappingBuilt_);

  for (int i = 1; i < numSeeds; ++i) {

    if (TypeOfData == RandomData) {
      RandomizeTrunkStrategies(i, 1);
    }

    ResetNodalFeatures();

    cout << i << " of " << numIterations << endl;
    clock_t begin = clock();
    GenerateSeed(numIterations, path, TypeOfData, domain, i);
    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    cout << "took: " << elapsed_secs / 60 << " minutes " << endl;
  }

}

void CFR_DL::RandomizeTrunkStrategies(int seed, double nonzero = 1) {
  if (!cache_.isCompletelyBuilt()) { cache_.buildTree(); }
  default_random_engine generator(seed);
  bernoulli_distribution BernoulliDraw(nonzero);
  uniform_real_distribution<double> UniformDraw(0, 10);
  for (auto&[aoh, data]:cache_.infosetData) {

    if (cache_.AOHPosition(aoh, current_trunk_,AboveTrunkBorder)) {
      auto &reg = data.regrets;
      auto &acc = data.avgStratAccumulator;
      auto &regUpdates = data.regretUpdates;
      auto &numUpdates = data.numUpdates;
      const auto numActions = static_cast<unsigned int>(acc.size());
      const vector<double> zeroes(numActions, 0.0);
      reg = zeroes;
      acc = zeroes;
      regUpdates = zeroes;
      const bool MixedStrat = BernoulliDraw(generator);
      uniform_int_distribution<int> dis(0, numActions - 1);
      const int rand_pos = dis(generator);
      if (MixedStrat) {
        for (int i = 0; i < numActions; ++i) {

          if (BernoulliDraw(generator)) {

            reg[i] = UniformDraw(generator);
          }
        }

        acc = calcRMProbs(reg);

      } else {

        reg[rand_pos] = 1.0;
        acc = calcRMProbs(reg);
      }
      double sum = 0.0;
      for (const auto &i:acc) { sum += i; }
      data.fixAvgStrategy = true;
      data.fixRMStrategy = true;
      numUpdates = 1;
    } else if (cache_.AOHPosition(aoh, current_trunk_,InAndBelowTrunkBorder)) {

      auto &reg = data.regrets;
      auto &acc = data.avgStratAccumulator;
      auto &regUpdates = data.regretUpdates;
      auto &numUpdates = data.numUpdates;
      const auto K = static_cast<unsigned int>(reg.size());
      const vector<double> zeroes(K, 0.0);
      reg = zeroes;
      acc = zeroes;
      regUpdates = zeroes;
      data.fixAvgStrategy = false;
      data.fixRMStrategy = false;
      numUpdates = 1;

    }
  }
}

void CFR_DL::printMappings() {

  if (!isMappingBuilt_) createMappingData();

  cout << "INFORMATION SET DICT: " << endl;

  unsigned int k_dim = 0;

  for (int i = 0; i < num_publicstates_at_trunk_depth_; ++i) {

    for (int j = 0; j < num_possible_actions_sequences_p0_; ++j) {

      if (information_set_mapping_[i][j]) {

        k_dim = information_set_mapping_[i][j]->size();

        cout << "(" << i << ", " << j << ") =";

        for (int k = 0; k < k_dim; ++k) {
          cout << " " << information_set_mapping_[i][j]->at(k);
        }

        cout << endl;
      }
    }
  }

  cout << "AUGMENTED INFORMATION SET DICT: " << endl;
  for (int i = 0; i < num_publicstates_at_trunk_depth_; ++i) {
    for (int j = 0; j < num_possible_actions_sequences_p1_; ++j) {
      if (augmented_information_set_mapping_[i][j]) {
        k_dim = augmented_information_set_mapping_[i][j]->size();

        cout << "(" << i << ", " << j << ") =";

        for (int k = 0; k < k_dim; ++k) {
          cout << " " << augmented_information_set_mapping_[i][j]->at(k);
        }

        cout << endl;
      }
    }
  }
}

const vector<vector<double>>
CFR_DL::cfr_to_nn_input(const vector<double> &range_acting_player, const vector<double> &range_opponent_player) {

  return transform_.to_nn_input(range_acting_player, range_opponent_player);

}

void CFR_DL::generate_public_features() {
  assert(cache_.isCompletelyBuilt());

  for (const auto &pubState:cache_.public_state_index_[current_trunk_]) {

    if (domain_type_ == typeid(const domains::KriegspielDomain).name()) {

      const vector<ActionId> obsHistory = pubState->getHistory();
      vector<double> public_hist_int = domains::encodeKriegSpielObsHistory(obsHistory);
      public_features_.emplace_back(public_hist_int);

    } else if (domain_type_ == typeid(const domains::DarkchessDomain).name()) {
      const vector<ActionId> obsHistory = pubState->getHistory();
      vector<double> public_hist_int = domains::encodeDarkchessObsHistory(cache_.observations, obsHistory);
      public_features_.emplace_back(public_hist_int);

    } else if (domain_type_ == typeid(const domains::GoofSpielDomain).name()) {
      const vector<ActionId> obsHistory = pubState->getHistory();

      vector<double> public_hist_int;
      for (auto &item:obsHistory) {

        public_hist_int.emplace_back(item);
      }

      vector<double> nodal_obs_double(public_hist_int.begin(), public_hist_int.end());
      public_features_.emplace_back(public_hist_int);

//                const vector<shared_ptr<Observation>> obsHistory = pubState->getHistory();
//                const vector<shared_ptr<domains::GoofSpielObservation>> ObsHistory = Cast<Observation, domains::GoofSpielObservation>(
//                        obsHistory);
//                vector<double> public_hist_int;
//
//                for (auto &item:ObsHistory) {
//
//                    public_hist_int.emplace_back(item->roundResult_);
//                }
//
//                vector<double> nodal_obs_double(public_hist_int.begin(), public_hist_int.end());
//                public_features_.emplace_back(nodal_obs_double);
    } else if (domain_type_ == typeid(const domains::OshiZumoDomain).name()) {
//                const vector<Observation> obsHistory = pubState->getHistory();
//                const vector<shared_ptr<domains::OshiZumoObservation>> ObsHistory = Cast<Observation, domains::OshiZumoObservation>(
//                        obsHistory);
//                vector<double> public_hist_int;
//
//                for (auto &item:ObsHistory) {
//                    public_hist_int.emplace_back(item->roundResult_);
//                }
//
//                vector<double> nodal_obs_double(public_hist_int.begin(), public_hist_int.end());
//                public_features_.emplace_back(nodal_obs_double);
    } else if (domain_type_ == typeid(const domains::GenericPokerDomain).name()) {

//                const vector<shared_ptr<Observation>> obsHistory = pubState->getHistory();
//
//                vector<int> public_hist_int = domains::EncodePokerObsHistoryFixed(obsHistory, 3, 2, 2,
//                                                                                  pair<int, int>(2, 2));
//
//                vector<double> nodal_obs_double(public_hist_int.begin(), public_hist_int.end());
//
//                public_features_.emplace_back(nodal_obs_double);
    }
//            else if (domain_type_ == typeid(const domains::GenericPokerDomain).name()) {
//
//                const vector<shared_ptr<Observation>> obsHistory = pubState->getHistory();
//
//                vector<int> public_hist_int = domains::EncodePokerObsHistory(obsHistory, 2, 2, 2, pair<int, int>(2, 2));
//
//                vector<double> nodal_obs_double(public_hist_int.begin(), public_hist_int.end());
//
//                public_features_.emplace_back(nodal_obs_double);
//            }
//            else if (domain_type_ == typeid(const domains::GenericPokerDomain).name()) {
//
//                const vector<shared_ptr<Observation>> obsHistory = pubState->getHistory();
//
//                vector<double> nodal_obs_double = domains::PokerObsToPotsizePublicCard(obsHistory, domainMaxUtility_,
//                                                                                       3);
//
//                public_features_.emplace_back(nodal_obs_double);
//            }
  }
}

void CFR_DL::generate_maps() {

  vector<unsigned int> ***information_set_mapping = nullptr;
  information_set_mapping = new vector<unsigned int> **[static_cast<int>(cache_.public_state_index_[current_trunk_].size())];
  for (int i = 0; i < static_cast<int>(cache_.public_state_index_[current_trunk_].size()); ++i) {
    vector<unsigned int> **infosetvec = nullptr;
    infosetvec = new vector<unsigned int> *[num_possible_actions_sequences_p0_];
    for (int j = 0; j < num_possible_actions_sequences_p0_; ++j) {
      infosetvec[j] = nullptr;
    }
    const auto &pubState = cache_.public_state_index_[current_trunk_][i];
    const auto &pubStateHands = cache_.getHandsForPubState(pubState, Player(0), current_trunk_);
    const auto &handindices = cache_.getHandIndices(pubStateHands, Player(0), current_trunk_);
    for (const auto &handidx:handindices) {
      const auto &aoh = cache_.getAOHforHandandPubState(pubState, Player(0),
                                                        cache_.possible_actions_sequences_p0_[current_trunk_][handidx], current_trunk_);
      const auto &nodes = cache_.getAugInfosetNodes(aoh, Player(0), current_trunk_);
      auto *indices = new vector<unsigned int>;
      for (const auto &node:nodes) {
        if (find_if(cache_.node_index_.at(current_trunk_).begin(), cache_.node_index_.at(current_trunk_).end(),
                    [&node](shared_ptr<EFGNode> node_in_list) { return *node_in_list == *node; }) !=
            cache_.node_index_.at(current_trunk_).end()) {
          const auto idx = cache_.getNodeIndexFor(node, current_trunk_);
          indices->emplace_back(idx);
        }
      }
      infosetvec[handidx] = indices;
    }
    information_set_mapping[i] = infosetvec;
  }

  information_set_mapping_ = information_set_mapping;

  vector<unsigned int> ***augmented_information_set_mapping = nullptr;
  augmented_information_set_mapping = new vector<unsigned int> **[num_publicstates_at_trunk_depth_];
  for (int i = 0; i < cache_.public_state_index_.at(current_trunk_).size(); ++i) {
    vector<unsigned int> **auginfosetvec = nullptr;
    auginfosetvec = new vector<unsigned int> *[num_possible_actions_sequences_p1_];
    for (int j = 0; j < num_possible_actions_sequences_p1_; ++j) {
      auginfosetvec[j] = nullptr;
    }
    const auto &pubState = cache_.public_state_index_.at(current_trunk_)[i];
    const auto &pubStateHands = cache_.getHandsForPubState(pubState, Player(1), current_trunk_);
    const auto &handindices = cache_.getHandIndices(pubStateHands, Player(1), current_trunk_);
    for (const auto &handidx:handindices) {
      const auto &aoh = cache_.getAOHforHandandPubState(pubState, Player(1),
                                                        cache_.possible_actions_sequences_p1_.at(current_trunk_)[handidx], current_trunk_);
      auto *indices = new vector<unsigned int>;
      const auto &nodes = cache_.getAugInfosetNodes(aoh, Player(1), current_trunk_);
      for (const auto &node:nodes) {
        if (find_if(cache_.node_index_.at(current_trunk_).begin(), cache_.node_index_.at(current_trunk_).end(),
                    [&node](shared_ptr<EFGNode> node_in_list) { return *node_in_list == *node; }) !=
            cache_.node_index_.at(current_trunk_).end()) {
          const auto idx = cache_.getNodeIndexFor(node,  current_trunk_);
          indices->emplace_back(idx);
        }

      }
      auginfosetvec[handidx] = indices;
    }
    augmented_information_set_mapping[i] = auginfosetvec;
  }
  augmented_information_set_mapping_ = augmented_information_set_mapping;

};

vector<unsigned int> ***CFR_DL::return_infoset_mapping() const {
  return information_set_mapping_;
}

vector<unsigned int> ***CFR_DL::return_auginfoset_mapping() const {
  return augmented_information_set_mapping_;
};

void CFR_DL::nn_to_cfr_input(const vector<double> &range_acting_player, const vector<double> &range_opponent_player,
                             const vector<vector<double>> &nn_output_matrix, Player updatingPlayer) {

  const auto &returning_vector = transform_.from_nn_output(range_acting_player, range_opponent_player,
                                                           nn_output_matrix, updatingPlayer);

  predicted_values_ = returning_vector;
}

vector<double> CFR_DL::ExpVtoCFVInfoset(const Player player) {

  vector<double> predicted_values;
  auto collect_data = [&](const shared_ptr<EFGNode> node) {
    if (node->type_ == TerminalNode) return;
    if (cache_.NodePosition(node, current_trunk_,InTrunkBorder)) {

      auto ev = algorithms::calcExpectedUtility(cache_, node, player);

      predicted_values.push_back(ev.avgUtility);
    }
  };

  treeWalk(cache_, collect_data, domainMaxDepth_);

  return predicted_values;

}

vector<double> CFR_DL::computeCFVInfosets(Player player) {
  vector<double> cfvinfosetslist(cache_.node_index_.at(current_trunk_).size(), 0.0);

  auto ranges = getRMTrunkRangesAllP0P1();

  const auto &auginfsetlist = player == Player(0) ? cache_.infoset_indexes_[current_trunk_] : cache_.aug_infoset_indexes_[current_trunk_];

  for (const auto &infoset:auginfsetlist) {
    if (infoset->getPlayer() == player) {

      const auto &nodes = cache_.getAugInfosetNodes(infoset, player, current_trunk_);

      int idxfirstnonzeronode = -1;

      for (const auto &node:nodes) {

        if (ranges[1 - player][cache_.getNodeIndexFor(node, current_trunk_)] > 0.0) {
          idxfirstnonzeronode = cache_.getNodeIndexFor(node, current_trunk_);
          break;
        }
      }

      double cfvinfoset = 0;
      for (const auto &node:nodes) {
        cfvinfoset += getNodePredAtIndex(node) * ranges.at(1 - player).at(cache_.getNodeIndexFor(node, current_trunk_));
      }

      if (idxfirstnonzeronode != -1) {

        cfvinfosetslist.at(idxfirstnonzeronode) = cfvinfoset;

      }
    }
  }
  return cfvinfosetslist;
}

vector<double>
CFR_DL::computeNoisyCFVInfosets(Player player, double noise_prop_total, double noise_prop_sign, double noise_mean,
                                double noise_std) {
  vector<double> cfvinfosetslist(cache_.node_index_.at(current_trunk_).size(), 0.0);

  auto ranges = getRMTrunkRangesAllP0P1();

  const auto &auginfsetlist = player == Player(0) ? cache_.infoset_indexes_[current_trunk_] : cache_.aug_infoset_indexes_[current_trunk_];

  default_random_engine generator(player);
  bernoulli_distribution addnoise(noise_prop_total);
  bernoulli_distribution neg_or_pos(noise_prop_sign);
  normal_distribution<double> gaussian_noise(noise_mean, noise_std);

  for (const auto &infoset:auginfsetlist) {
    if (infoset->getPlayer() == player) {

      const auto &nodes = cache_.getAugInfosetNodes(infoset, player, current_trunk_);

      int idxfirstnonzeronode = -1;

      for (const auto &node:nodes) {

        if (ranges[1 - player][cache_.getNodeIndexFor(node, current_trunk_)] > 0.0) {
          idxfirstnonzeronode = cache_.getNodeIndexFor(node, current_trunk_);
          break;
        }
      }

      double cfvinfoset = 0;
      for (const auto &node:nodes) {
        cfvinfoset += getNodePredAtIndex(node) * ranges[1 - player][cache_.getNodeIndexFor(node, current_trunk_)];
      }

      if (addnoise(generator)) {

        cfvinfoset += gaussian_noise(generator);

      }

      if (idxfirstnonzeronode != -1) {

        cfvinfosetslist.at(idxfirstnonzeronode) = cfvinfoset;

      }
    }
  }
  return cfvinfosetslist;
}

double CFR_DL::HuberCFRDNN(vector<double> &cfrdvalues, vector<double> &nnvalues, double clip_delta) {
  double squareerrorsum = 0;

  assert(cfrdvalues.size() == nnvalues.size());

  int numnonzero = 0;

  for (int i = 0; i < cfrdvalues.size(); ++i) {
    if (cfrdvalues[i] != 0.0) {
      ++numnonzero;
    }
    auto error = fabs(cfrdvalues[i] - nnvalues[i]);
    if (error > clip_delta) {
      squareerrorsum += error;
    } else {
      squareerrorsum += pow(error, 2);
    }

  }
  assert(numnonzero > 0);
  return squareerrorsum / numnonzero;

};

double
CFR_DL::linfCFRDNN(vector<double> &cfrdvalues, vector<double> &nnvalues, Player player, double linf_tolerance,
                   bool verbose) {
  double currentmaxabserror = 0;
  int idxmax = 0;
  int errAboveTolerance = 0;

  assert(cfrdvalues.size() == nnvalues.size());

  for (int i = 0; i < cfrdvalues.size(); ++i) {

    auto error = fabs(cfrdvalues[i] - nnvalues[i]);

    if (error >= linf_tolerance) {

      ++errAboveTolerance;

    }

    if (error > currentmaxabserror) {

      idxmax = i;
      currentmaxabserror = error;

    }

  }

  if (verbose) {

    const auto &aoh = cache_.getAugInfosetforNode(cache_.node_index_.at(current_trunk_).at(idxmax), player, current_trunk_);
    const auto &pubState = cache_.getPublicStateFor(aoh);

    if (domain_type_ == typeid(const domains::GoofSpielDomain).name()) {

//                const vector<shared_ptr<Observation>> obsHistory = pubState->getHistory();
//                const vector<shared_ptr<domains::GoofSpielObservation>> ObsHistory = Cast<Observation, domains::GoofSpielObservation>(
//                        obsHistory);
//                vector<double> public_hist_int;
//
//                for (auto &item:ObsHistory) {
//
//                    public_hist_int.emplace_back(item->roundResult_);
//                }
//
//                cout << "pubstate: ";
//                print_vector(public_hist_int);
    } else if (domain_type_ == typeid(const domains::OshiZumoDomain).name()) {
//                const vector<shared_ptr<Observation>> obsHistory = pubState->getHistory();
//                const vector<shared_ptr<domains::OshiZumoObservation>> ObsHistory = Cast<Observation, domains::OshiZumoObservation>(
//                        obsHistory);
//                vector<double> public_hist_int;
//
//                for (auto &item:ObsHistory) {
//                    public_hist_int.emplace_back(item->roundResult_);
//                }
//                cout << "pubstate: ";
//                print_vector(public_hist_int);

    } else if (domain_type_ == typeid(const domains::GenericPokerDomain).name()) {

//                const vector<shared_ptr<Observation>> obsHistory = pubState->getHistory();
//
//                vector<int> public_hist_int = domains::EncodePokerObsHistory(obsHistory, 3, 2, 2, pair<int, int>(2, 2));
//
//                cout << "pubstate: ";
//                print_vector(public_hist_int);

    }

    if (player == Player(0)) {
      cout << "idx of infset with highest error: " << cache_.getInfoSetIndex(aoh, current_trunk_) << endl;
      cout << aoh->toString() << endl;
    } else {
      cout << "idx of auginfset with highest error: " << cache_.getAugInfoSetIndex(aoh, current_trunk_) << endl;
      cout << aoh->toString() << endl;
    }

    cout << "Number of elements with linf error above " << linf_tolerance << " : " << errAboveTolerance << endl;

  }

  return fabs(currentmaxabserror);

};

pair<double, double> CFR_DL::ZeroRatioRanges(vector<double> rangep0, vector<double> rangep1) {

  double numzerop0 = 0;
  double numzerop1 = 0;

  for (const auto &prob:rangep0) {
    if (prob == 0.0) {
      ++numzerop0;
    }
  }

  for (const auto &prob:rangep1) {
    if (prob == 0.0) {
      ++numzerop1;
    }
  }

  return make_pair<double, double>(numzerop0 / rangep0.size(), numzerop1 / rangep1.size());

}

void CFR_DL::print_public_features() {
  if (!isMappingBuilt_) createMappingData();
  const int numPubStates = cache_.public_state_index_.size();
  cout << numPubStates << " and " << public_features_.size() << endl;
  assert(public_features_.size() == numPubStates);
  for (int i = 0; i < numPubStates; i++) {
    cout << "public state " << i << endl;
    if (domain_type_ == typeid(domains::KriegspielDomain).name()) {
      domains::printKriegSpielObsHistory(cache_.public_state_index_[current_trunk_][i]->getHistory());
    } else if (domain_type_ == typeid(domains::DarkchessDomain).name()) {
      domains::printDarkchessObsHistory(cache_.observations, cache_.public_state_index_[current_trunk_][i]->getHistory());
    }
    print_vector(public_features_[i]);
  }
}

const vector<double> CFR_DL::collectSubgameValues(DLCFRData &cache, Player player) {

  vector<double> predicted_values;

  for (const auto &node:cache.node_index_[current_trunk_]) {

    auto ev = algorithms::calcExpectedUtility(cache, node, player);

    predicted_values.push_back(ev.avgUtility);

  }

  assert(predicted_values.size() == predicted_values_.size());
  return predicted_values;
};

}//namespace GTLib2

/*
    Copyright 2019 Faculty of Electrical Engineering at CTU in Prague

    This file is part of Game Theoretic Library.

    Game Theoretic Library is free software: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License
    as published by the Free Software Foundation, either version 3
    of the License, or (at your option) any later version.

    Game Theoretic Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Game Theoretic Library.

    If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GTLIB2_CFR_DL_H
#define GTLIB2_CFR_DL_H

#include <memory>
#include <functional>
#include <cmath>
#include <fstream>
#include <utility>
#include "base/includes.h"

#include "algorithms/cfr.h"
#include "algorithms/strategy.h"

#include "domains/genericPoker.h"
#include "domains/goofSpiel.h"
#include "domains/oshiZumo.h"
#include "domains/kriegspiel.h"

#include "utils/tfNN.h"
#include "utils/nn_transform_data.h"
#include "utils/utils.h"

namespace GTLib2::algorithms {

enum GameTreePosition {
  AboveTrunkBorder, BelowTrunkBorder, InTrunkBorder, InAndBelowTrunkBorder, InAndAboveTrunkBorder
};

class DLCFRData : public virtual CFRData, public virtual PublicStateCache {
 public:
  DLCFRData(const Domain &domain, vector<int> trunk_depth) :
      PublicStateCache(domain),
      CFRData(domain, InfosetsUpdating),
      EFGCache(domain),
      InfosetCache(domain), trunk_depths_(std::move(trunk_depth)) {
    const auto n = trunk_depths_.size();
    node_index_ = vector<vector<shared_ptr<EFGNode>>>(n, vector<shared_ptr<EFGNode>>());
    infoset_indexes_ = vector<vector<shared_ptr<AOH>>>(n, vector<shared_ptr<AOH>>());
    aug_infoset_indexes_ = vector<vector<shared_ptr<AOH>>>(n, vector<shared_ptr<AOH>>());
    public_state_index_ = vector<vector<shared_ptr<PublicState>>>(n, vector<shared_ptr<PublicState>>());
    possible_actions_sequences_p0_ = vector<vector<shared_ptr<EFGHand>>>(n, vector<shared_ptr<EFGHand>>());
    possible_actions_sequences_p1_ = vector<vector<shared_ptr<EFGHand>>>(n, vector<shared_ptr<EFGHand>>());
    publicState2HandsP0_ = vector<unordered_map<shared_ptr<PublicState>, vector<shared_ptr<EFGHand>>>>(n, unordered_map<shared_ptr<PublicState>, vector<shared_ptr<EFGHand>>>());
    publicState2HandsP1_ = vector<unordered_map<shared_ptr<PublicState>, vector<shared_ptr<EFGHand>>>>(n, unordered_map<shared_ptr<PublicState>, vector<shared_ptr<EFGHand>>>());
    Hand2AOHsP0_ =  vector<unordered_map<shared_ptr<EFGHand>, vector<shared_ptr<AOH>>>>(n, unordered_map<shared_ptr<EFGHand>, vector<shared_ptr<AOH>>>());
    Hand2AOHsP1_ = vector<unordered_map<shared_ptr<EFGHand>, vector<shared_ptr<AOH>>>>(n, unordered_map<shared_ptr<EFGHand>, vector<shared_ptr<AOH>>>());
    publicState2Hands2AOHP0_ = vector<unordered_map<shared_ptr<PublicState>, unordered_map<shared_ptr<EFGHand>, shared_ptr<AOH>>>>(n, unordered_map<shared_ptr<PublicState>, unordered_map<shared_ptr<EFGHand>, shared_ptr<AOH>>>());
    publicState2Hands2AOHP1_ = vector<unordered_map<shared_ptr<PublicState>, unordered_map<shared_ptr<EFGHand>, shared_ptr<AOH>>>>(n, unordered_map<shared_ptr<PublicState>, unordered_map<shared_ptr<EFGHand>, shared_ptr<AOH>>>());
    infosetnodes_ = vector<unordered_map<shared_ptr<AOH>, vector<shared_ptr<EFGNode>>>>(n, unordered_map<shared_ptr<AOH>, vector<shared_ptr<EFGNode>>>()) ;
    nodeinfosets_ = vector<unordered_map<shared_ptr<EFGNode>, shared_ptr<AOH>>>(n, unordered_map<shared_ptr<EFGNode>, shared_ptr<AOH>>());
    auginfosetnodes_ = vector<unordered_map<shared_ptr<AOH>, vector<shared_ptr<EFGNode>>>>(n, unordered_map<shared_ptr<AOH>, vector<shared_ptr<EFGNode>>>());
    nodeauginfosets_ = vector<unordered_map<shared_ptr<EFGNode>, shared_ptr<AOH>>>(n, unordered_map<shared_ptr<EFGNode>, shared_ptr<AOH>>());
    nodetohandp0_ = vector<unordered_map<shared_ptr<EFGNode>, shared_ptr<EFGHand>>>(n, unordered_map<shared_ptr<EFGNode>, shared_ptr<EFGHand>>());
    nodetohandp1_ = vector<unordered_map<shared_ptr<EFGNode>, shared_ptr<EFGHand>>>(n, unordered_map<shared_ptr<EFGNode>, shared_ptr<EFGHand>>());
    addCallback([&](const shared_ptr<EFGNode> &node) {
      this->createInfoSetRound(node);
      this->createInfoSetEFGDepth(node);
      this->createDLCFRMapping(node);
      this->createObservationMapping(node);
    });
    this->createInfoSetRound(getRootNode());
    this->createInfoSetEFGDepth(getRootNode());
    this->createDLCFRMapping(getRootNode());
  };
  vector<int> trunk_depths_;
  vector<vector<shared_ptr<EFGNode>>> node_index_;
  vector<vector<shared_ptr<AOH>>> infoset_indexes_;
  vector<vector<shared_ptr<AOH>>> aug_infoset_indexes_;
  vector<vector<shared_ptr<PublicState>>> public_state_index_;
  vector<vector<shared_ptr<EFGHand>>> possible_actions_sequences_p0_;
  vector<vector<shared_ptr<EFGHand>>> possible_actions_sequences_p1_;
  vector<unordered_map<shared_ptr<PublicState>, vector<shared_ptr<EFGHand>>>> publicState2HandsP0_;
  vector<unordered_map<shared_ptr<PublicState>, vector<shared_ptr<EFGHand>>>> publicState2HandsP1_;
  vector<unordered_map<shared_ptr<EFGHand>, vector<shared_ptr<AOH>>>> Hand2AOHsP0_;
  vector<unordered_map<shared_ptr<EFGHand>, vector<shared_ptr<AOH>>>> Hand2AOHsP1_;
  vector<unordered_map<shared_ptr<PublicState>, unordered_map<shared_ptr<EFGHand>, shared_ptr<AOH>>>>
      publicState2Hands2AOHP0_;
  vector<unordered_map<shared_ptr<PublicState>, unordered_map<shared_ptr<EFGHand>, shared_ptr<AOH>>>>
      publicState2Hands2AOHP1_;
  vector<unordered_map<shared_ptr<AOH>, vector<shared_ptr<EFGNode>>>> infosetnodes_;
  vector<unordered_map<shared_ptr<EFGNode>, shared_ptr<AOH>>> nodeinfosets_;
  vector<unordered_map<shared_ptr<AOH>, vector<shared_ptr<EFGNode>>>> auginfosetnodes_;
  vector<unordered_map<shared_ptr<EFGNode>, shared_ptr<AOH>>> nodeauginfosets_;
  unordered_map<shared_ptr<AOH>, int> InfoSetEFGDepth_; //contains all infosets
  unordered_map<shared_ptr<AOH>, unsigned int> InfoSetRound_;
  vector<unordered_map<shared_ptr<EFGNode>, shared_ptr<EFGHand>>> nodetohandp0_;
  vector<unordered_map<shared_ptr<EFGNode>, shared_ptr<EFGHand>>> nodetohandp1_;
  unordered_map<ObservationId, shared_ptr<Observation>> observations;

  const inline DLCFRData *getCache() const {
    return this;
  }

  inline void createObservationMapping(const shared_ptr<EFGNode> &node) {
    if (node->type_ == TerminalNode || node->type_ == ChanceNode) return;
    auto *n = dynamic_cast<FOG2EFGNode *>(node.get());
    auto obs = n->getLastOutcome()->publicObservation;
    if (observations.find(obs->getId()) == observations.end()) {
      observations.emplace(obs->getId(), obs);
    }
  }

  inline void createInfoSetEFGDepth(const shared_ptr<EFGNode> &node) {
    if (node->type_ == TerminalNode || node->type_ == ChanceNode) return;

    if (node->getPlayer() == Player(0)) {
      const auto &aoh0 = getAugInfosetFor(node, Player(0));
      const auto depth = node->efgDepth();
      if (InfoSetEFGDepth_.find(aoh0) == InfoSetEFGDepth_.end()) {
        InfoSetEFGDepth_.emplace(aoh0, depth);
      }
    } else {
      const auto &aoh1 = getAugInfosetFor(node, Player(1));
      const auto depth = node->efgDepth();
      if (InfoSetEFGDepth_.find(aoh1) == InfoSetEFGDepth_.end()) {
        InfoSetEFGDepth_.emplace(aoh1, depth);
      }

    }
  }

//        inline unsigned int getParentalChanceNodes(const shared_ptr<EFGNode> &node) {
//
//            if (typeid(domain_).name() == typeid(domains::GenericPokerDomain).name()) {
//                unsigned int numParentalChanceNodes = 0;
//                if (node->type_ == ChanceNode)numParentalChanceNodes += 1;
//                if (node->getParent())
//                    numParentalChanceNodes += getParentalChanceNodes(make_shared<EFGNode>(*node->getParent()));
//                return numParentalChanceNodes;
//            } else {
//                if (node->efgDepth() % 2 != 0) {
//                    const auto &tofognode = CastEFGtoFOG(node);
//                    return tofognode->stateDepth();
//                } else {
//                    return node->efgDepth() / 2;
//
//                }
//            }
//
//        }

  inline void createInfoSetRound(const shared_ptr<EFGNode> &node) {
    if (node->type_ == TerminalNode || node->type_ == ChanceNode) return;
    
    if (node->getPlayer() == Player(0)) {
      const auto &fognode = CastEFGtoFOG(node);
      const auto &round = fognode->stateDepth();//TODO HOTFIX
      const auto &aoh0 = getAugInfosetFor(node, Player(0));

      if (InfoSetRound_.find(aoh0) == InfoSetRound_.end()) {
        InfoSetRound_.emplace(aoh0, round);
      }
    } else if (node->getPlayer() == Player(1)) {

      const auto &aoh1 = getAugInfosetFor(node, Player(1));

      const auto &fognode = CastEFGtoFOG(node);
      const auto &round = fognode->stateDepth();//TODO HOTFIX

      if (InfoSetRound_.find(aoh1) == InfoSetRound_.end()) {
        InfoSetRound_.emplace(aoh1, round);
      }

    }
  }

  const inline vector<shared_ptr<Action>> AOHgetAvailableActionsfor(const shared_ptr<AOH> &aoh) {
    const auto &node = getNodesFor(aoh)[0];
    return node->availableActions();
  }

  inline const vector<shared_ptr<EFGHand>> &
  getHandsForPubState(const shared_ptr<PublicState> &pubState, const Player player, const int trunk_id) const {
    if (player == Player(0))return publicState2HandsP0_.at(trunk_id).at(pubState);
    else { return publicState2HandsP1_.at(trunk_id).at(pubState); }

  };

  inline shared_ptr<AOH>
  getAOHforHandandPubState(const shared_ptr<PublicState> &pubState, const Player player,
                           const shared_ptr<EFGHand> &hand, const int trunk_id) const {

    if (player == Player(0)) {
      const auto &handexistsp0 = publicState2Hands2AOHP0_.at(trunk_id).at(pubState);

      return getValueOfMap<EFGHand, AOH>(handexistsp0, hand);
    } else {

      const auto &handexistsp1 = publicState2Hands2AOHP1_.at(trunk_id).at(pubState);

      return getValueOfMap<EFGHand, AOH>(handexistsp1, hand);
    }

  }

  inline const vector<shared_ptr<AOH>> &
  getAOHsForHand(const shared_ptr<EFGHand> &hand, const Player player, const int trunk_id) const {

    if (player == Player(0))return Hand2AOHsP0_.at(trunk_id).at(hand);
    else { return Hand2AOHsP1_.at(trunk_id).at(hand); }
  }


  inline long getHandIndex(const shared_ptr<EFGHand> &hand, Player player, const int trunk_id) const {

    long handidx = -1;

    if (player == Player(0)) {
      assert(find_if(possible_actions_sequences_p0_.at(trunk_id).begin(), possible_actions_sequences_p0_.at(trunk_id).end(),
                     [&hand](shared_ptr<EFGHand> hand_in_list) { return *hand == *hand_in_list; }) !=
          possible_actions_sequences_p0_.at(trunk_id).end());
      handidx =
          find_if(possible_actions_sequences_p0_.at(trunk_id).begin(), possible_actions_sequences_p0_.at(trunk_id).end(),
                  [&hand](shared_ptr<EFGHand> hand_in_list) { return *hand == *hand_in_list; }) -
              possible_actions_sequences_p0_.at(trunk_id).begin();

    } else if (player == Player(1)) {
      assert(find_if(possible_actions_sequences_p1_.at(trunk_id).begin(), possible_actions_sequences_p1_.at(trunk_id).end(),
                     [&hand](shared_ptr<EFGHand> hand_in_list) { return *hand == *hand_in_list; }) !=
          possible_actions_sequences_p1_.at(trunk_id).end());
      handidx =
          find_if(possible_actions_sequences_p1_.at(trunk_id).begin(), possible_actions_sequences_p1_.at(trunk_id).end(),
                  [&hand](shared_ptr<EFGHand> hand_in_list) { return *hand == *hand_in_list; }) -
              possible_actions_sequences_p1_.at(trunk_id).begin();
    }
    assert(handidx != -1);
    return handidx;
  }

  inline const vector<long> getHandIndices(const vector<shared_ptr<EFGHand>> &hands, Player player, const int trunk_id) const {
    vector<long> handindices;

    for (const auto &hand:hands) {
      const auto idx = getHandIndex(hand, player, trunk_id);
      handindices.emplace_back(idx);

    }
    return handindices;
  }

  inline const double
  getRangeForAOH(shared_ptr<AOH> &aoh, Player player, vector<double> range_p0_, vector<double> range_p1_, const int trunk_id) {
    const auto &firstnode = getNodesFor(aoh)[0];
    const auto idx = getNodeIndexFor(firstnode, trunk_id);
    if (player == Player(0)) { return range_p0_[idx]; }
    else { return range_p1_[idx]; }
  }

//  inline const vector<shared_ptr<EFGHand>>
//  getHandsFor(const shared_ptr<PublicState> &pubState, Player player) const {
//    if (player == Player(0))return publicState2HandsP0_.at(pubState);
//    else { return publicState2HandsP1_.at(pubState); }
//  }

  inline const shared_ptr<AOH>
  getInfoSetForHandPubState(const shared_ptr<PublicState> &pubState, const shared_ptr<EFGHand> &hand,
                            Player player, const int trunk_id) const {
    if (player == Player(0))return publicState2Hands2AOHP0_.at(trunk_id).at(pubState).at(hand);
    else { return publicState2Hands2AOHP1_.at(trunk_id).at(pubState).at(hand); }
  }

  inline const unsigned int getNodeIndexFor(const shared_ptr<EFGNode> &node, const int trunk_id) const {

    const auto &it = std::find_if(node_index_.at(trunk_id).begin(), node_index_.at(trunk_id).end(),
                                  [&node](shared_ptr<EFGNode> node_in_list) { return *node_in_list == *node; });
    const auto &idx = distance(node_index_.at(trunk_id).begin(), it);

    return idx;
  }

  inline const vector<shared_ptr<EFGNode>> &getAugInfosetNodes(const shared_ptr<AOH> &aoh, Player player, const int trunk_id) const {
    if (player == Player(0)) { return infosetnodes_.at(trunk_id).at(aoh); }
    else { return auginfosetnodes_.at(trunk_id).at(aoh); }

  }

  inline const shared_ptr<AOH> &getAugInfosetforNode(const shared_ptr<EFGNode> &node, Player player, const int trunk_id) const {
    if (player == Player(0)) { return nodeinfosets_.at(trunk_id).at(node); }
    else { return nodeauginfosets_.at(trunk_id).at(node); }
  }

  inline const long getInfoSetIndex(const shared_ptr<AOH> &info_set, const int trunk_id) {
    return find_if(infoset_indexes_.at(trunk_id).begin(), infoset_indexes_.at(trunk_id).end(), [&info_set]
        (shared_ptr<AOH> &infoset_in_list) { return *info_set == *infoset_in_list; }) -
        infoset_indexes_.at(trunk_id).begin();
  }

  inline const long getAugInfoSetIndex(const shared_ptr<AOH> &aug_info_set, const int trunk_id) {
    return find_if(aug_infoset_indexes_.at(trunk_id).begin(), aug_infoset_indexes_.at(trunk_id).end(), [&aug_info_set]
        (shared_ptr<AOH> &aug_infoset_in_list) { return *aug_info_set == *aug_infoset_in_list; }) -
        aug_infoset_indexes_.at(trunk_id).begin();
  }

  inline const long getPublicStateIndex(const shared_ptr<PublicState> &public_state, const int trunk_id) {
    return find_if(public_state_index_.at(trunk_id).begin(), public_state_index_.at(trunk_id).end(), [&public_state]
        (shared_ptr<PublicState> &state_in_list) { return *public_state == *state_in_list; }) -
        public_state_index_.at(trunk_id).begin();
  }

  inline const vector<long> getPublicAugInfIndices(const shared_ptr<EFGNode> &node, const int trunk_id) {

    vector<long> node_public_aug_inf_indices;
    const auto &public_state = getPublicStateFor(node);
    const auto &info_set = getAugInfosetFor(node, Player(0));
    const auto &aug_info_set = getAugInfosetFor(node, Player(1));

    auto aseqp0 = shared_ptr<EFGHand>();
    auto aseqp1 = shared_ptr<EFGHand>();
    if (typeid(domain_).name() == typeid(domains::GenericPokerDomain).name()) {

//                const auto& fognode= CastEFGtoFOG(node);
//
//                aseqp0 = make_shared<EFGHand>(fognode->getPrivateHand(Player(0)));
//                aseqp1 = make_shared<EFGHand>(fognode->getPrivateHand(Player(1)));

    } else {
      aseqp0 = nodetohandp0_.at(trunk_id).at(node);
      aseqp1 = nodetohandp1_.at(trunk_id).at(node);

//                const auto &fognode = CastEFGtoFOG(node);
//
//                aseqp0 = make_shared<EFGHand>(fognode->getHand(Player(0)));
//                aseqp1 = make_shared<EFGHand>(fognode->getHand(Player(1)));


    }

    node_public_aug_inf_indices.push_back(getPublicStateIndex(public_state, trunk_id));

    node_public_aug_inf_indices.push_back(getInfoSetIndex(info_set, trunk_id));

    node_public_aug_inf_indices.push_back(getAugInfoSetIndex(aug_info_set, trunk_id));

    node_public_aug_inf_indices.push_back(getNodeIndexFor(node, trunk_id));

    node_public_aug_inf_indices.push_back(getHandIndex(aseqp0, Player(0), trunk_id));

    node_public_aug_inf_indices.push_back(getHandIndex(aseqp1, Player(1), trunk_id));

    return node_public_aug_inf_indices;

  }

  using PublicStateCache::getInfosetsFor;
  using InfosetCache::getNodesFor;

  inline int getRoundOfNode(const shared_ptr<EFGNode> &node) {
    const auto &auginfoset = getAugInfosetFor(node, node->getPlayer());
    return InfoSetRound_.at(auginfoset);
  };

  inline const bool AOHPosition(const shared_ptr<AOH> &aoh, int id, const GameTreePosition &position = InTrunkBorder) {

    const auto &domain_name = typeid(domain_).name();
    const auto &gp_domain = typeid(domains::GenericPokerDomain).name();
    const auto &oz_domain = typeid(domains::OshiZumoDomain).name();
    const auto &gs_domain = typeid(domains::GoofSpielDomain).name();
    const auto &ks_domain = typeid(domains::KriegspielDomain).name();
    const auto &dc_domain = typeid(domains::DarkchessDomain).name();

    if (position == AboveTrunkBorder) {

      if (domain_name == gp_domain) {
        return InfoSetRound_.at(aoh) < trunk_depths_[id];
      } else if (domain_name == oz_domain) {
        return InfoSetEFGDepth_.at(aoh) < trunk_depths_[id];
      } else if (domain_name == gs_domain) {
        return InfoSetEFGDepth_.at(aoh) < trunk_depths_[id];
      } else if (domain_name == ks_domain) {
        return getPublicStateFor(aoh)->getHistory().size() < trunk_depths_[id];
      } else if (domain_name == dc_domain) {
        return getPublicStateFor(aoh)->getHistory().size() < trunk_depths_[id];
      }

    } else if (position == InTrunkBorder) {

      if (domain_name == gp_domain) {
        return InfoSetRound_.at(aoh) == trunk_depths_[id];
      } else if (domain_name == oz_domain) {
        return InfoSetEFGDepth_.at(aoh) == trunk_depths_[id];
      } else if (domain_name == gs_domain) {
        return InfoSetEFGDepth_.at(aoh) == trunk_depths_[id];
      } else if (domain_name == ks_domain) {
        return getPublicStateFor(aoh)->getHistory().size() == trunk_depths_[id];
      } else if (domain_name == dc_domain) {
        return getPublicStateFor(aoh)->getHistory().size() == trunk_depths_[id];
      }

    } else if (position == InAndBelowTrunkBorder) {
      if (domain_name == gp_domain) {
        return InfoSetRound_.at(aoh) >= trunk_depths_[id];
      } else if (domain_name == oz_domain) {
        return InfoSetEFGDepth_.at(aoh) >= trunk_depths_[id];
      } else if (domain_name == gs_domain) {
        return InfoSetEFGDepth_.at(aoh) >= trunk_depths_[id];
      } else if (domain_name == ks_domain) {
        return getPublicStateFor(aoh)->getHistory().size() >= trunk_depths_[id];
      } else if (domain_name == dc_domain) {
        return getPublicStateFor(aoh)->getHistory().size() >= trunk_depths_[id];
      }
    }
    return false;
  }

  inline const bool
  NodePosition(const shared_ptr<EFGNode> &node, int id, const GameTreePosition &position = InTrunkBorder) {
    assert(node->type_ != TerminalNode);
    const auto &domain_name = typeid(domain_).name();
    const auto &gp_domain = typeid(domains::GenericPokerDomain).name();
    const auto &oz_domain = typeid(domains::OshiZumoDomain).name();
    const auto &gs_domain = typeid(domains::GoofSpielDomain).name();
    const auto &ks_domain = typeid(domains::KriegspielDomain).name();
    const auto &dc_domain = typeid(domains::DarkchessDomain).name();

    if (position == AboveTrunkBorder) {
      if (domain_name == gp_domain) {
        return getRoundOfNode(node) < trunk_depths_[id];
      } else if (domain_name == oz_domain) {
        return node->efgDepth() < trunk_depths_[id];
      } else if (domain_name == gs_domain) {
        return node->efgDepth() < trunk_depths_[id];
      } else if (domain_name == ks_domain) {
        return node->getPublicState()->getHistory().size() < trunk_depths_[id];
      } else if (domain_name == dc_domain) {
        return node->getPublicState()->getHistory().size() < trunk_depths_[id];
      }

    } else if (position == InTrunkBorder) {
      if (domain_name == gp_domain) {
        return getRoundOfNode(node) == trunk_depths_[id];
      } else if (domain_name == oz_domain) {
        return node->efgDepth() == trunk_depths_[id];
      } else if (domain_name == gs_domain) {
        return node->efgDepth() == trunk_depths_[id];
      } else if (domain_name == ks_domain) {
        return node->getPublicState()->getHistory().size() == trunk_depths_[id];
      } else if (domain_name == dc_domain) {
        return node->getPublicState()->getHistory().size() == trunk_depths_[id];
      }

    } else if (position == InAndBelowTrunkBorder) {
      if (domain_name == gp_domain) {
        return getRoundOfNode(node) >= trunk_depths_[id];
      } else if (domain_name == oz_domain) {
        return node->efgDepth() >= trunk_depths_[id];
      } else if (domain_name == gs_domain) {
        return node->efgDepth() >= trunk_depths_[id];
      } else if (domain_name == ks_domain) {
        return node->getPublicState()->getHistory().size() >= trunk_depths_[id];
      } else if (domain_name == dc_domain) {
        return node->getPublicState()->getHistory().size() >= trunk_depths_[id];
      }
    }
    return false;
  };

 protected:

  inline void createTrunkNodeIndex(const shared_ptr<EFGNode> &node, int id) {
    auto &_node_index = node_index_[id];
    if (std::find_if(_node_index.begin(), _node_index.end(), [&node]
        (shared_ptr<EFGNode> &node_in_index) { return *node == *node_in_index; }) ==
        _node_index.end()) {
      _node_index.push_back(node);
    }

  };

  inline void createTrunkInfosetIndex(const shared_ptr<AOH> &infoset, int id) {
    auto &infoset_index_ = infoset_indexes_[id];
    if (std::find_if(infoset_index_.begin(), infoset_index_.end(), [&infoset]
        (shared_ptr<AOH> &infoset_in_list) { return *infoset == *infoset_in_list; }) ==
        infoset_index_.end()) {
      infoset_index_.push_back(infoset);

    }
  };

  inline void createTrunkAugInfosetIndex(const shared_ptr<AOH> &auginfoset, int id) {
    auto &aug_infoset_index_ = aug_infoset_indexes_[id];
    if (std::find_if(aug_infoset_index_.begin(), aug_infoset_index_.end(), [&auginfoset]
        (shared_ptr<AOH> &aug_infoset_in_list) { return *auginfoset == *aug_infoset_in_list; }) ==
        aug_infoset_index_.end()) {
      aug_infoset_index_.push_back(auginfoset);

    }
  };

  inline void createTrunkNodetoHandIndex(const shared_ptr<EFGNode> &node, const shared_ptr<EFGHand> &handp0,
                                         const shared_ptr<EFGHand> &handp1, int id) {
    auto &nodetohandp0 = nodetohandp0_[id];
    auto &nodetohandp1 = nodetohandp1_[id];

    auto maybenodep0 = nodetohandp0.find(node);
    auto maybenodep1 = nodetohandp1.find(node);

    if (maybenodep0 == nodetohandp0.end()) {
      nodetohandp0.emplace(node, handp0);
    }
    if (maybenodep1 == nodetohandp1.end()) {
      nodetohandp1.emplace(node, handp1);
    }
  };

  inline void createTrunkInfosetToNodeIndex(const shared_ptr<AOH> &infoset, const shared_ptr<EFGNode> &node, int id) {

    auto &infosetNodes = infosetnodes_[id];
    auto maybeInfoset = infosetNodes.find(infoset);

    if (maybeInfoset == infosetNodes.end()) {
      infosetNodes.emplace(infoset, vector<shared_ptr<EFGNode>>{node});
    } else {
      auto &nodes = infosetNodes.at(infoset);
      if (std::find_if(nodes.begin(), nodes.end(),
                       [&node](shared_ptr<EFGNode> &efg_node) { return *node == *efg_node; }) == nodes.end()) {
        nodes.push_back(node);
      }
    }
  };

  inline void
  createTrunkAugInfosetToNodeIndex(const shared_ptr<AOH> &auginfoset, const shared_ptr<EFGNode> &node, int id) {

    auto &augInfosetNodes = auginfosetnodes_[id];
    auto maybeAugInfoset = augInfosetNodes.find(auginfoset);

    if (maybeAugInfoset == augInfosetNodes.end()) {
      augInfosetNodes.emplace(auginfoset, vector<shared_ptr<EFGNode>>{node});
    } else {
      auto &nodes = augInfosetNodes.at(auginfoset);

      if (std::find_if(nodes.begin(), nodes.end(),
                       [&node](shared_ptr<EFGNode> &efg_node) { return *node == *efg_node; }) == nodes.end()) {
        nodes.push_back(node);
      }
    }
  };

  inline void createTrunkNodeToInfosetIndex(const shared_ptr<EFGNode> &node, const shared_ptr<AOH> &infoset, int id) {
    auto &nodeInfosets = nodeinfosets_[id];
    if (nodeInfosets.find(node) == nodeInfosets.end()) {
      nodeInfosets.emplace(node, infoset);
    }
  }

  inline void
  createTrunkNodeToAugInfosetIndex(const shared_ptr<EFGNode> &node, const shared_ptr<AOH> &auginfoset, int id) {
    auto &nodeAugInfoset = nodeauginfosets_[id];
    if (nodeAugInfoset.find(node) == nodeAugInfoset.end()) {
      nodeAugInfoset.emplace(node, auginfoset);
    }
  }

  inline void createTrunkPubStateIndex(const shared_ptr<PublicState> &pubState, int id) {
    auto &publicStateIndex = public_state_index_[id];
    if (std::find_if(publicStateIndex.begin(), publicStateIndex.end(), [&pubState]
        (shared_ptr<PublicState> &state_in_list) { return *pubState == *state_in_list; }) ==
        publicStateIndex.end()) {
      publicStateIndex.push_back(pubState);
    }
  }

  inline void createTrunkPossibleActionSeqP0(const shared_ptr<EFGHand> &nodeHandp0, int id) {
    auto &possibleActionSeqP0 = possible_actions_sequences_p0_[id];
    if (vectorNotFound<EFGHand>(possibleActionSeqP0, nodeHandp0)) {
      possibleActionSeqP0.emplace_back(nodeHandp0);
    }
  };

  inline void createTrunkPossibleActionSeqP1(const shared_ptr<EFGHand> &nodeHandp1, int id) {
    auto &possibleActionSeqP1 = possible_actions_sequences_p1_[id];
    if (vectorNotFound<EFGHand>(possibleActionSeqP1, nodeHandp1)) {
      possibleActionSeqP1.emplace_back(nodeHandp1);
    }
  };

  inline void
  createTrunkPubStateToHandP0Idx(const shared_ptr<PublicState> &pubState,
                                 const shared_ptr<EFGHand> &nodeHandp0,
                                 int id) {
    auto &publicState2HandP0 = publicState2HandsP0_[id];
    const auto maybepub2handp0 = publicState2HandP0.find(pubState);
    if (maybepub2handp0 == publicState2HandP0.end()) {
      publicState2HandP0.emplace(pubState, vector<shared_ptr<EFGHand>>{nodeHandp0});
    } else {
      //check if hand is in there
      vector<shared_ptr<EFGHand>> &pubstatehandp0 = publicState2HandP0.at(pubState);
      if (vectorNotFound<EFGHand>(pubstatehandp0, nodeHandp0)) {
        pubstatehandp0.emplace_back(nodeHandp0);
      }
    }
  };

  inline void
  createTrunkPubStateToHandP1Idx(const shared_ptr<PublicState> &pubState,
                                 const shared_ptr<EFGHand> &nodeHandp1,
                                 int id) {
    auto &publicState2HandP1 = publicState2HandsP1_[id];
    const auto maybepub2handp1 = publicState2HandP1.find(pubState);
    if (maybepub2handp1 == publicState2HandP1.end()) {
      publicState2HandP1.emplace(pubState, vector<shared_ptr<EFGHand>>{nodeHandp1});
    } else {
      //check if hand is in there
      vector<shared_ptr<EFGHand>> &pubstatehandp1 = publicState2HandP1.at(pubState);
      if (vectorNotFound<EFGHand>(pubstatehandp1, nodeHandp1)) {
        pubstatehandp1.emplace_back(nodeHandp1);
      }
    }
  };

  inline void
  createTrunkHandP0ToInfosetIdx(const shared_ptr<EFGHand> &nodeHandp0, const shared_ptr<AOH> &infoset, int id) {
    auto &hand2AOHp0 = Hand2AOHsP0_[id];
    const auto maybeHandp0 = hand2AOHp0.find(nodeHandp0);
    if (maybeHandp0 == hand2AOHp0.end()) {
      hand2AOHp0.emplace(nodeHandp0, vector<shared_ptr<AOH>>{infoset});
    } else {
      vector<shared_ptr<AOH>> &handaohp0 = hand2AOHp0.at(nodeHandp0);
      if (std::find(handaohp0.begin(), handaohp0.end(), infoset) == handaohp0.end()) {
        handaohp0.emplace_back(infoset);
      }
    }
  };

  inline void
  createTrunkHandP1ToAugInfosetIdx(const shared_ptr<EFGHand> &nodeHandp1, const shared_ptr<AOH> &auginfoset, int id) {
    auto &hand2AOHp1 = Hand2AOHsP1_[id];
    const auto maybeHandp1 = hand2AOHp1.find(nodeHandp1);
    if (maybeHandp1 == hand2AOHp1.end()) {
      hand2AOHp1.emplace(nodeHandp1, vector<shared_ptr<AOH>>{auginfoset});
    } else {
      vector<shared_ptr<AOH>> &handaohp1 = hand2AOHp1.at(nodeHandp1);
      if (std::find(handaohp1.begin(), handaohp1.end(), auginfoset) == handaohp1.end()) {
        handaohp1.emplace_back(auginfoset);
      }
    }
  };

  inline void
  createTrunkPubStateHandAOHP0Idx(const shared_ptr<PublicState> &pubState, const shared_ptr<EFGHand> &nodeHandp0,
                                  const shared_ptr<AOH> &infoset, int id) {
    auto &publicState2Hand2AOHp0 = publicState2Hands2AOHP0_[id];
    const auto maybePubState2H2AOHP0 = publicState2Hand2AOHp0.find(pubState);
    if (maybePubState2H2AOHP0 == publicState2Hand2AOHp0.end()) {
      publicState2Hand2AOHp0.emplace(pubState, unordered_map<shared_ptr<EFGHand>, shared_ptr<AOH>>({{nodeHandp0,
                                                                                                     infoset}}));
    } else {
      unordered_map<shared_ptr<EFGHand>, shared_ptr<AOH>> &pubstatefoundp0 = publicState2Hand2AOHp0.at(pubState);
      if (vectorNotFoundInMap<EFGHand, AOH>(pubstatefoundp0, nodeHandp0)) {
        pubstatefoundp0.emplace(nodeHandp0, infoset);
      }
    }
  }

  inline void
  createTrunkPubStateHandAOHP1Idx(const shared_ptr<PublicState> &pubState, const shared_ptr<EFGHand> &nodeHandp1,
                                  const shared_ptr<AOH> &auginfoset, int id) {
    auto &publicState2Hand2AOHp1 = publicState2Hands2AOHP1_[id];
    const auto maybePubState2H2AOHP1 = publicState2Hand2AOHp1.find(pubState);
    if (maybePubState2H2AOHP1 == publicState2Hand2AOHp1.end()) {
      publicState2Hand2AOHp1.emplace(pubState, unordered_map<shared_ptr<EFGHand>, shared_ptr<AOH>>({{nodeHandp1,
                                                                                                     auginfoset}}));
    } else {
      auto &pubstatefoundp1 = publicState2Hand2AOHp1.at(pubState);
      if (vectorNotFoundInMap<EFGHand, AOH>(pubstatefoundp1, nodeHandp1)) {
        pubstatefoundp1.emplace(nodeHandp1, auginfoset);
      }
    }
  }

  inline void createDLCFRMapping(const shared_ptr<EFGNode> &node) {
    if (node->type_ == TerminalNode || node->type_ == ChanceNode)return;

    for (int i = 0; i < trunk_depths_.size(); ++i) {
      int trunk = trunk_depths_[i];

      if (!NodePosition(node, i, InTrunkBorder))return;
      shared_ptr<EFGHand> nodeHandp0;
      shared_ptr<EFGHand> nodeHandp1;

      const auto &pubState = getPublicStateFor(node);
      const auto infoset = node->getAOHAugInfSet(Player(0));
      const auto auginfoset = node->getAOHAugInfSet(Player(1));

      if (typeid(domain_).name() == typeid(domains::GenericPokerDomain).name()) {
//                nodeHandp0 = make_shared<EFGHand>(node->getPrivateHand(Player(0)));
//                nodeHandp1 = make_shared<EFGHand>(node->getPrivateHand(Player(1)));
      } else {

        const auto &fognode = CastEFGtoFOG(node);

        nodeHandp0 = make_shared<EFGHand>(fognode->getHand(Player(0)));
        nodeHandp1 = make_shared<EFGHand>(fognode->getHand(Player(1)));
      }

      createTrunkNodeIndex(node, i);
      createInfoSetEFGDepth(node);
      createInfoSetRound(node);
      createTrunkNodetoHandIndex(node, nodeHandp0, nodeHandp1, i);
      createTrunkInfosetIndex(infoset, i);
      createTrunkAugInfosetIndex(auginfoset, i);
      createTrunkInfosetToNodeIndex(infoset, node, i);
      createTrunkAugInfosetToNodeIndex(auginfoset, node, i);
      createTrunkNodeToInfosetIndex(node, infoset, i);
      createTrunkNodeToAugInfosetIndex(node, auginfoset, i);
      createTrunkPubStateIndex(pubState, i);
      createTrunkPossibleActionSeqP0(nodeHandp0, i);
      createTrunkPossibleActionSeqP1(nodeHandp1, i);
      createTrunkPubStateToHandP0Idx(pubState, nodeHandp0, i);
      createTrunkPubStateToHandP1Idx(pubState, nodeHandp1, i);
      createTrunkHandP0ToInfosetIdx(nodeHandp0, infoset, i);
      createTrunkHandP1ToAugInfosetIdx(nodeHandp1, auginfoset, i);
      createTrunkPubStateHandAOHP0Idx(pubState, nodeHandp0, infoset, i);
      createTrunkPubStateHandAOHP1Idx(pubState, nodeHandp1, auginfoset, i);
    }
  }
};

enum GeneratedDataType {
  RandomData, CFRDData, CFRNNData
};

class CFR_DL {
 public:
  inline explicit CFR_DL(const int &current_trunk_id,
                         const vector<vector<string>> &network_paths,
                         const Domain &domain,
                         CFRSettings &settings,
                         DLCFRData &cache, bool isCFV = true) :
      cache_(cache), cfrsettings_(settings) {
    network_paths_ = network_paths;
    current_trunk_ = current_trunk_id;
    domainMaxDepth_ = domain.getMaxStateDepth() * 2;
    predicted_values_ = vector<double>();
    isMappingBuilt_ = false;
    domain_type_ = typeid(domain).name();
    transform_ = TransformCFRDataToNN();
    num_possible_actions_sequences_p0_ = 0;
    num_possible_actions_sequences_p1_ = 0;
    num_publicstates_at_trunk_depth_ = 0;
    public_features_ = vector<vector<double>>();
    information_set_mapping_ = nullptr;
    augmented_information_set_mapping_ = nullptr;
    num_infosets_at_trunk_depth_ = 0;
    num_auginfosets_at_trunk_depth_ = 0;
    predicted_values_ = vector<double>();
    tfnn_ = vector<TFNeuralNetwork *>(cache_.trunk_depths_.size(), nullptr);
    isLoadedNN_ = false;
    UnfixedTrunkFixedBottom_ = false;
    isCFV_ = isCFV;
    domainMaxUtility_ = domain.getMaxUtility();

  };

  inline explicit CFR_DL(const int &current_trunk_id,
                         Domain &domain,
                         DLCFRData &cache,
                         CFRSettings &settings)
      : cache_(cache), cfrsettings_(settings) {
    current_trunk_ = current_trunk_id;
    domainMaxDepth_ = domain.getMaxStateDepth() * 2;
    isMappingBuilt_ = false;
    domain_type_ = typeid(domain).name();
    num_possible_actions_sequences_p0_ = 0;
    num_possible_actions_sequences_p1_ = 0;
    num_publicstates_at_trunk_depth_ = 0;
    public_features_ = vector<vector<double>>();
    information_set_mapping_ = nullptr;
    augmented_information_set_mapping_ = nullptr;
    num_infosets_at_trunk_depth_ = 0;
    num_auginfosets_at_trunk_depth_ = 0;
    predicted_values_ = vector<double>();
    tfnn_ = vector<TFNeuralNetwork *>();
    isLoadedNN_ = false;
    UnfixedTrunkFixedBottom_ = false;
    domainMaxUtility_ = domain.getMaxUtility();
    isCFV_ = false;
    transform_ = TransformCFRDataToNN();
    isLoadedNN_ = false;
  };

  inline DLCFRData &getCache() {
    return cache_;
  }

  void createMappingData();

  const vector<vector<double>> getRMTrunkRangesAllP0P1();

  void load_nn();

  const vector<vector<double>>
  cfr_to_nn_input(const vector<double> &, const vector<double> &);

  void nn_to_cfr_input(const vector<double> &range_acting_player,
                       const vector<double> &range_opponent_player,
                       const vector<vector<double>> &nn_output_matrix, Player updatingPlayer);

  void runCFRDIteration(int numIterations, Domain &domain, Player updatingPlayer);

  void runCFRD(int numIterations, Domain &domain);

  void runCFRDSumInfosetsIteration(int numIterations, Domain &domain, Player updatingPlayer);

  void runCFRDSumInfosets(int numIterations, Domain &domain);

  void runNoisyCFRDSumInfosetsIteration(int numIterations, Domain &domain, Player updatingPlayer,
                                        double noise_prop_total, double noise_prop_sign, double noise_mean,
                                        double noise_std);

  void runNoisyCFRDSumInfosets(int numIterations, Domain &domain, double noise_prop_total = 0.9,
                               double noise_prop_sign = 0.5, double noise_mean = 0.0, double noise_std = 0.01);

  void runSystematicNoiseCFRDSumInfosetsIteration(int numIterations, Domain &domain, Player updatingPlayer);

  void runSystematicNoiseCFRDSumInfosets(int numIterations, Domain &domain);

  void runCFRDNN(int numIterations, Domain &domain, const string &path, bool verbose = false);

  void runDepthLimitedCFRIterations(int numIterations);

  void runCFRNNIteration(Player updatingPlayer);

  double runDepthLimitedCFRIteration(const shared_ptr<EFGNode> &node,
                                     const array<double, 3> reachProbs,
                                     const Player updatingPl);

  void printTrunkRegrets() const;

  void printBottomAVGStrats(const DLCFRData &cache, const int trunk_depth) const;

  void generate_public_features();

  void generate_maps();

  void UnfixTrunkFixBottom();

  const vector<double> CFRDcollectExPV(CFRData &cache, Player player);

  const double getNodePredAtIndex(const shared_ptr<EFGNode> &node) const;

  vector<unsigned int> ***return_infoset_mapping() const;

  vector<unsigned int> ***return_auginfoset_mapping() const;

  void CollectNodeData();

  void UpdateInfosetRegrets(Player updatingPlayer);

  void RandomizeTrunkStrategies(int seed, double nonzero);

  void printMappings();

  vector<double> getAllRMRangesForNode(const shared_ptr<EFGNode> &node);

  vector<long> getPublicAugInfIndices(const shared_ptr<EFGNode> &node);

  vector<double> getNodalPublicFeaturesAllPlayers(const shared_ptr<EFGNode> &node);

  vector<double> ExpVtoCFVInfoset(const Player player);

  void printCumulativeRegrets(Player player) const;

  vector<double> computeCFVInfosets(Player player);

  vector<double>
  computeNoisyCFVInfosets(Player player, double noise_prop_total, double noise_prop_sign, double noise_mean,
                          double noise_std);

  double HuberCFRDNN(vector<double> &cfrdvalues, vector<double> &nnvalues, double clip_delta = 1.0);

  double
  linfCFRDNN(vector<double> &cfrdvalues, vector<double> &nnvalues, Player player, double linf_tolerance = 1.,
             bool verbose = false);

  pair<double, double> ZeroRatioRanges(vector<double> rangep0, vector<double> rangep1);

  void FixResetTrunkStrats(DLCFRData &cache);

  void ResetNodalFeatures();

  void GenerateSeed(int numIterations,
                    string path,
                    GeneratedDataType TypeOfData,
                    Domain &domain,
                    int Iteration);

  void
  GenerateSeeds(int numSeeds,
                int numIterations,
                const string path,
                GeneratedDataType TypeOfData,
                Domain &domain);

  void print_public_features();

  const vector<double> collectSubgameValues(DLCFRData &cache, Player player);

  unsigned long num_possible_actions_sequences_p0_;
  unsigned long num_possible_actions_sequences_p1_;
  unsigned long num_publicstates_at_trunk_depth_;
  DLCFRData &cache_;
  vector<unsigned int> ***information_set_mapping_;
  vector<unsigned int> ***augmented_information_set_mapping_;
  vector<pair<shared_ptr<EFGNode>, vector<double>>> nodal_features_;

  vector<double> debugvector_;

  vector<vector<double>> sparseness_error_;

  vector<shared_ptr<EFGNode>> nodevector_;

  vector<double> predicted_values_;

  unordered_map<shared_ptr<AOH>, vector<vector<double>>> InfosetactionRegrets_;

  CFRSettings &cfrsettings_;

 protected:
  int current_trunk_;
  vector<vector<string>> network_paths_;
  vector<vector<double>> public_features_;
  int domainMaxDepth_;
  unsigned long num_infosets_at_trunk_depth_;
  unsigned long num_auginfosets_at_trunk_depth_;
  bool isMappingBuilt_;
  bool isLoadedNN_;
  const char *domain_type_;
  TransformCFRDataToNN transform_;
  vector<TFNeuralNetwork *> tfnn_;
  bool UnfixedTrunkFixedBottom_;
  bool isCFV_;
  double domainMaxUtility_;
};

};

#endif //GTLIB2_CFR_DL_H
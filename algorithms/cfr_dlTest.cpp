/*
    Copyright 2019 Faculty of Electrical Engineering at CTU in Prague

    This file is part of Game Theoretic Library.

    Game Theoretic Library is free software: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License
    as published by the Free Software Foundation, either version 3
    of the License, or (at your option) any later version.

    Game Theoretic Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Game Theoretic Library.

    If not, see <http://www.gnu.org/licenses/>.
*/

#include "base/base.h"

#include "domains/goofSpiel.h"
#include "domains/darkchess.h"
#include "domains/chess_utils.h"
#include "algorithms/cfr_dl.h"
#include "algorithms/cfr_br.h"
#include "utils/tfNN.h"
#include "gtest/gtest.h"

using namespace GTLib2::domains::chess;
namespace GTLib2::algorithms {
// TODO add test for multiple networks
TEST(CFRDL, darkchess) {
  domains::DarkchessDomain domain(6, BOARD::MINIMAL4x3);

  vector<double>
      expCFVp0{0, 0, 0, 0, 0.299985, 0, 0, 0.0763564, 0, 0, 0, 0, 0, 0, 0, 0, 0, -0.00246779, 0, 0, 0, 0, 0, 0, 0, 0};
  vector<double> expCFVp1
      {0, 0, 0, 0.0002421, 0.0123409, 0.000416784, -0.0304735, 0.0221445, -0.00192121, 0, 0, 0, 0, 0, 0, 0, 0,
       -0.00431544, -0.0442114, 0, 0, 0, 0, 0, 0, 0};
  double err = 0.000001;

  vector<int> trunk_depth = {2};
  int num_iteration = 3;
  CFRSettings cfrSettings;
  cfrSettings.regretMatching = RegretMatchingPlus;
  cfrSettings.cfrUpdating = InfosetsUpdating;
  cfrSettings.accumulatorWeighting = LinearAccWeighting;
  DLCFRData dlcfrData(domain, trunk_depth);
  string dings = "../tests/cfrdl_test_nn/nn4x3";
  string metadings = dings + ".meta";
  vector<string> nn_setup{metadings, dings, "dense_1_input:0", "dense_4/BiasAdd:0"};
  CFR_DL cfr_nn(0, {nn_setup}, domain,
                cfrSettings,
                dlcfrData, true);
  cfr_nn.runDepthLimitedCFRIterations(num_iteration);
  auto sumcfvcfrdp0 = cfr_nn.computeCFVInfosets(Player(0));
  auto sumcfvcfrdp1 = cfr_nn.computeCFVInfosets(Player(1));

  EXPECT_EQ(sumcfvcfrdp1.size(), expCFVp1.size());
  EXPECT_EQ(sumcfvcfrdp0.size(), expCFVp0.size());

  for (int i = 0; i < expCFVp0.size(); ++i) {
    EXPECT_LE(sumcfvcfrdp0[i] - expCFVp0[i], err);
    EXPECT_LE(sumcfvcfrdp1[1] - expCFVp1[1], err);
  }

}

TEST(CFRDL, goofspiel) {
  // run on RELEASE, otherwise could get different results.
  domains::GoofSpielDomain domain
      ({/*.variant=*/domains::IncompleteObservations, /*.numCards=*/3, /*.fixChanceCards=*/true, /*.chanceCards=*/
                     {3, 2, 1}});

  vector<double> expCFVp0{0, 0, 0.516667, 0, 0, 1.97, 0, 0, 0};
  vector<double> expCFVp1{0, 0, 0, -0.847453, 0, 0.127988, 0, -2.10382, 0};
  double err = 0.0000001;

  vector<int> trunk_depth = {2};
  int num_iteration = 5;
  CFRSettings cfrSettings;
  cfrSettings.regretMatching = RegretMatchingPlus;
  cfrSettings.cfrUpdating = InfosetsUpdating;
  cfrSettings.accumulatorWeighting = LinearAccWeighting;
  DLCFRData dlcfrData(domain, trunk_depth);

  CFR_DL cfrdl(0, domain, dlcfrData, cfrSettings);
  cfrdl.runCFRD(num_iteration, domain);
  auto sumcfvcfrdp0 = cfrdl.computeCFVInfosets(Player(0));
  auto sumcfvcfrdp1 = cfrdl.computeCFVInfosets(Player(1));

  EXPECT_EQ(sumcfvcfrdp1.size(), expCFVp1.size());
  EXPECT_EQ(sumcfvcfrdp0.size(), expCFVp0.size());

  for (int i = 0; i < expCFVp0.size(); ++i) {
    EXPECT_LE(sumcfvcfrdp0[i] - expCFVp0[i], err);
    EXPECT_LE(sumcfvcfrdp1[1] - expCFVp1[1], err);
  }
}

TEST(CFRDL, NNCoverges) {
  domains::DarkchessDomain domain(6, BOARD::MINIMAL4x3);

  vector<int> trunk_depth = {2};
  int num_iteration = 100;
  CFRSettings cfrSettings;
  cfrSettings.regretMatching = RegretMatchingPlus;
  cfrSettings.cfrUpdating = InfosetsUpdating;
  cfrSettings.accumulatorWeighting = LinearAccWeighting;
  DLCFRData dlcfrData(domain, trunk_depth);
  string dings = "../tests/cfrdl_test_nn/nn4x3";

  string metadings = dings + ".meta";
  vector<string> nn_setup{metadings, dings, "dense_1_input:0", "dense_4/BiasAdd:0"};
  CFR_DL cfr_nn(0, {nn_setup}, domain,
                cfrSettings,
                dlcfrData, true);
  cfr_nn.runDepthLimitedCFRIterations(num_iteration);
  CFRBR cfrbr(domain, 0);
  auto expl = cfrbr.calcExploitability(num_iteration, dlcfrData);
  EXPECT_LE(expl, 0.01);
}
}
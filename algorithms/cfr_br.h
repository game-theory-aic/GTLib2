/*
    Copyright 2019 Faculty of Electrical Engineering at CTU in Prague

    This file is part of Game Theoretic Library.

    Game Theoretic Library is free software: you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public License
    as published by the Free Software Foundation, either version 3
    of the License, or (at your option) any later version.

    Game Theoretic Library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with Game Theoretic Library.

    If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GTLIB2_CFR_BR_H
#define GTLIB2_CFR_BR_H

#include "algorithms/cfr_dl.h"

namespace GTLib2::algorithms {

class CFRBR {
 public:

  inline explicit CFRBR(Domain &domain, int current_trunk_id) {
    current_trunk_ = current_trunk_id;
    domain_type_ = typeid(domain).name();
    domainMaxDepth_ = domain.getMaxStateDepth();
    cfrsettings_ = CFRSettings();
    cfrsettings_.regretMatching = RegretMatchingPlus;
    cfrsettings_.accumulatorWeighting = LinearAccWeighting;
    cfrsettings_.cfrUpdating = InfosetsUpdating;
  };

  void AssignTrunkUniformStrategy(DLCFRData &cache_);

//  double calcExploitabilityUnifStrat(const Domain &domain);

  void InfoSetUpdateRegretsPlus(DLCFRData &cache_, Player updatingPlayer);

  double runCFRBRIteration(DLCFRData &cache_, const shared_ptr<EFGNode> &node,
                           array<double, 3> reachProbs,
                           Player updatingPl, Player FixedPlayer);

  void runCFRBRIterations(DLCFRData &cache_, int numIterations,
                          Player FixedPlayer);

  void FixTrunkInfosetsFor(Player FixedPlayer,
                           DLCFRData &cache_);

  void FixAllBottomInfosets(DLCFRData &cache_);

  double calcBRValue(int numIterations, Player bestResponder, const DLCFRData cache_);

  double calcExploitability(int numIterations,
                            DLCFRData cache_);

  vector<double> computeBRVofAllPubStates(const Domain &domain_, int numIterations, const int &trunk_depth_,
                                          Player bestResponder,
                                          CFRSettings &settings_, const DLCFRData cache_);

  double calcValueofPublicState(DLCFRData &cache, Player BestResponder,
                                const shared_ptr<PublicState> &pubState, int trunk_depth_);

  vector<double>
  getWeightedExpValueofNode(DLCFRData &cache, const unordered_set<shared_ptr<EFGNode>> &nodes,
                            int trunk_depth_, Player player);

  const char *domain_type_;

  int domainMaxDepth_;

  int current_trunk_;

  CFRSettings cfrsettings_;

};// class CFRBR

}//namespace GTLib2::algorithms


#endif //GTLIB2_CFR_BR_H
